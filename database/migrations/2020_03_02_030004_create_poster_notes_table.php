<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePosterNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('poster_notes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('outher_id')->nullable();
            $table->string('title');
            $table->text('description')->nullable();
            $table->longText('body')->nullable();
            $table->string('img')->nullable();
            $table->text('tags')->nullable();
            $table->string('programms')->nullable();
            $table->integer('category_id')->nullable();
            $table->string('slug')->nullable();
            $table->integer('isActive')->default(1);
            $table->string('seoTitle')->nullable();
            $table->text('seoDesc')->nullable();
            $table->text('seoKeywords')->nullable();
            $table->string('author_name')->nullable()->default(null);
            $table->integer('views')->default(0);
            $table->integer('likes')->default(0);
            $table->integer('dislikes')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('poster_notes');
    }
}
