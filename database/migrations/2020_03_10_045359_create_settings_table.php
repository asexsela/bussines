<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->string('logo_header')->nullable();
            $table->string('logo_footer')->nullable();
            $table->text('mails')->nullable();
            $table->text('phones')->nullable();
            $table->text('address')->nullable();
            $table->string('vk')->nullable();
            $table->string('fb')->nullable();
            $table->string('inst')->nullable();
            $table->string('yuotube')->nullable();
            $table->longText('policy')->nullable();
            $table->longText('personal_data')->nullable();
            $table->longText('scripts')->nullable();
            $table->string('theme')->default('basic');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
