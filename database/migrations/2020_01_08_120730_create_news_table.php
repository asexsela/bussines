<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('outher_id');
            $table->string('title');
            $table->text('description')->nullable();
            $table->longText('body');
            $table->string('img')->nullable();
            $table->text('tags')->nullable();
            $table->integer('category_id')->nullable();
            $table->string('slug');
            $table->integer('isActive')->default(1);
            $table->integer('themeIsDay')->default(0);
            $table->text('tagsInCloud')->nullable();
            $table->integer('rating')->default(0);
            $table->string('seoTitle');
            $table->text('seoDesc')->nullable();
            $table->text('seoKeywords')->nullable();
            $table->integer('author_id')->nullable()->default(null);
            $table->string('author_name')->nullable()->default(null);
            $table->integer('views')->default(0);
            $table->integer('likes')->default(0);
            $table->integer('dislikes')->default(0);
            $table->integer('comment_id')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}
