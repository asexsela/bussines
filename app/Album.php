<?php

namespace App;

use App\Image;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Album extends Model
{

    protected $guarded = ['file', 'files', '_token', 'body', 'images'];

    public function images() {
        return $this->hasMany(Image::class, 'album_id', 'outher_id');
    }

    public function previews() {
        return $this->hasMany(Image::class, 'album_id', 'outher_id')->limit(2);
    }

    public function setCustomDateAttribute($value)
    {
        $this->attributes['custom_date'] = Carbon::parse($value);
    }
}
