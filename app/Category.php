<?php

namespace App;

use App\Heading;
use App\HeadingNews;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    protected $guarded = ['file', '_token', 'body', 'tags'];

    
    public function headings() {
        return $this->hasMany(Heading::class)->where('active', 1);
    }
    public function news() {
        return $this->hasMany(HeadingNews::class);
    }
    public function heading() {
        return $this->beLongsTo(Heading::class);
    }
}
