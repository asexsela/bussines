<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PosterCategory extends Model
{
    protected $guarded = ['file', '_token', 'body', 'tags'];

    public function posters() {
        return $this->hasMany(Poster::class, 'category_id')->orderBy('created_at', 'asc');
    }
    
}
