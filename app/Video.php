<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\VideoViews;
use App\VideoLikes;
use App\VideoDislikes;

class Video extends Model
{
    protected $guarded = ['file', '_token'];

    public function views() {
        return $this->hasMany(VideoViews::class);
    }

    public function likes() {
        return $this->hasMany(VideoLikes::class);
    }

    public function dislikes() {
        return $this->hasMany(VideoDislikes::class);
    }
}
