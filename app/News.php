<?php

namespace App;

use App\Views;
use App\Likes;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class News extends Model
{
    protected $guarded = ['file', '_token', 'editor'];

    public function views() {
        return $this->hasMany(Views::class);
    }

    public function likes() {
        return $this->hasMany(Likes::class);
    }

    public function dislikes() {
        return $this->hasMany(Dislikes::class);
    }

    public function isType() {
        return 'news-page/';
    }

    public function setPublishedAtAttribute($value)
    {
        $this->attributes['published_at'] = Carbon::parse($value);
    }
}
