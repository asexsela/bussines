<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Number extends Model
{
    protected $fillable = [
        'title', 
        'seoTitle', 
        'seoDesc', 
        'seoKeyWords', 
        'archive_id', 
        'file', 
        'old_file',
        'img',
        'old_img',
    ];
}
