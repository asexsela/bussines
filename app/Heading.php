<?php

namespace App;

use App\HeadingNews;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Heading extends Model
{
    protected $guarded = ['file', '_token', 'body', 'tags'];
    
    public function news() {
        return $this->hasMany(HeadingNews::class)->orderBy('created_at', 'desc')->where('published_at', '<=', Carbon::now());
    }

    public function sub_categories() {
        return $this->hasMany(Category::class);
    }

}
