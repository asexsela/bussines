<?php

namespace App;

use App\Author;
use Illuminate\Database\Eloquent\Model;

class BlogNews extends Model
{
    protected $guarded = ['file', '_token', 'editor'];
    
    public function author() {
        return $this->belongsTo(Author::class, 'author_id', 'outher_id');
    }

    public function views() {
        return $this->hasMany(BlogViews::class, 'news_id');
    }

    public function likes() {
        return $this->hasMany(BlogLikes::class, 'news_id');
    }

    public function dislikes() {
        return $this->hasMany(BlogDislikes::class, 'news_id');
    }

    public function isType() {
        return '/author/article/';
    }
}
