<?php

namespace App\Providers;

use App\Archive;
use App\Partner;
use App\News;
use App\Setting;
use App\Banner;
use Jenssegers\Date\Date;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        // \Illuminate\Support\Facades\URL::forceScheme('https');

        Date::setlocale(config('app.locale'));


        view()->share('numbers_componets', Archive::orderBy('title', 'desc')->get());
        view()->share('parnters_slider', Partner::get()->random(6));
        view()->share('page_outher_news', News::get()->random(3));
        view()->share('banners', Banner::all());
        view()->share('count_banner', Banner::all()->count());
        view()->share('setting', Setting::find(1));



    }
}
