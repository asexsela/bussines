<?php

use App\Menu;

function menu($id, $file = false) {

    if(!$file) {
        $file = 'components.menus.nav';
    }

    if($model = Menu::find($id)) {
        $data = [
            'name' => $model->name,
            'items' => $model->items
        ];

        return view($file, compact('data'));
    }

    return 'Меню не найдено';
}