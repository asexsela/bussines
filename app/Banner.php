<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    protected $fillable = ['title', 'url', 'img_big', 'img_big_old', 'img_small', 'img_small_old'];

    public function views() {
        return $this->hasMany(BannerViews::class);
    }

    public function clicks() {
        return $this->hasMany(BannerClicks::class);
    }
}
