<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Letter extends Model
{
    protected $guarded = ['file', '_token', 'body', 'tags'];
    
}
