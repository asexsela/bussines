<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MenuItem extends Model
{
    protected $fillable = ['name', 'position', 'url', 'menu_id', 'img'];

    public function menu() {
        return $this->belongsTo(Menu::class);
    }
}
