<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $fillable = ['name', 'body', 'active', 'img', 'outher_id', 'created_at'];
}
