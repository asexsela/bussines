<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendMail extends Mailable
{
    use Queueable, SerializesModels;

    public $email;
    public $name;
    public $mess;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->name = $data->name;
        $this->email = $data->email;
        $this->mess = $data->body;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('БИЗНЕС и ВЛАСТЬ | Новое сообщение')
                    ->view('mails.sendmail')
                    ->with([
                        'name' => $this->name,
                        'email' => $this->email,
                        'mess' => $this->mess,
                    ]);
    }
}
