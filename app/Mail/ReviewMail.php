<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ReviewMail extends Mailable
{
    use Queueable, SerializesModels;

    public $name;
    public $mess;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->name = $data['name'];
        $this->mess = $data['mess'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('БИЗНЕС и ВЛАСТЬ | Новый отзыв')
                    ->view('mails.reviewmail')
                    ->with([
                        'name' => $this->name,
                        'mess' => $this->mess,
                    ]);
    }
}
