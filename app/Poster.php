<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Poster extends Model
{
    protected $guarded = ['file', '_token', 'editor'];

    public function notes() {
        return $this->hasMany(PosterProgramms::class, 'poster_id')->orderBy('created_at', 'asc');
    }
    public function programms() {
        return $this->hasMany(PosterNotes::class, 'poster_id')->orderBy('created_at', 'asc');
    }
}
