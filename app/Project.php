<?php

namespace App;

use App\ProjectNews;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    public function news()  {
        return $this->hasMany(ProjectNews::class)->orderBy('created_at', 'desc');
    }
}
