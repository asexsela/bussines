<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Jenssegers\Date\Date;

class VoitingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'date_end' => Date::parse($this->date_end)->format('j F Y '),
            'date_start' => Date::parse($this->created_at)->format('j F Y '),
            'date_check' => Date::parse($this->date_end)->timestamp,
            'items' => $this->items,
            'all' => $this->allVoiting->count(),
            'status' => $this->status,
            'isIndex' => $this->isIndex,
            'date_now' => Date::parse(now())->timestamp,
        ];
    }
}
