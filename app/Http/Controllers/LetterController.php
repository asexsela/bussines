<?php

namespace App\Http\Controllers;

use App\Letter;
use Illuminate\Http\Request;

class LetterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $letters = Letter::all();
        return view('pages.thanks_mails', compact('letters'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function adminIndex()
    {
        $letters = Letter::all();
        return view('pages.admin.thanks_mails', compact('letters'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $images = json_decode($request->images);

        foreach ($images as $img) {
            $letter = new Letter;
            $letter->img = '/images/'.$img;
            $letter->save();
        }

        return response()->json([
            'success' => 1,
            'type' => 'update'
        ]);
  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Letter  $letter
     * @return \Illuminate\Http\Response
     */
    public function destroy(Letter $letter)
    {

        $letter->delete();
        
        return response()->json([
            'success' => 1
        ]);
    }
}
