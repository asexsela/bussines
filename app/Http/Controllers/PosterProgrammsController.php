<?php

namespace App\Http\Controllers;

use App\PosterProgramms;
use App\Poster;
use Illuminate\Http\Request;

class PosterProgrammsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = PosterProgramms::orderBy('created_at', 'desc')->paginate(10);
        return view('pages.admin.poster_programms', compact('news'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = Poster::all();
        return view('pages.admin.create.poster_programms', compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!isset($request->isActive) && $request->isActive != '1') {
            $request->request->add(['isActive' => 0]);
        }
        if(!$request->slug) {
            $request->merge(['slug' => str_slug($request->title)]);
        }
        $data = PosterProgramms::create($request->all());

        if($data) {
            return response()->json([
                'success' => 1,
                'type' => 'create'
            ]);
        }
        return response()->json([
            'error' => 1
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PosterProgramms  $posterProgramms
     * @return \Illuminate\Http\Response
     */
    public function show(PosterProgramms $note)
    {
        $outher = $note->poster->notes->random(3);
        return view('pages.poster_programms', compact('note', 'outher'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PosterProgramms  $posterProgramms
     * @return \Illuminate\Http\Response
     */
    public function edit(PosterProgramms $note)
    {
        $news = $note;
        $category = Poster::all();
        return view('pages.admin.edit.poster_programms', compact('news', 'category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PosterProgramms  $posterProgramms
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PosterProgramms $note)
    {
        if(!isset($request->isActive) && $request->isActive != '1') {
            $request->request->add(['isActive' => 0]);
        }
        if(!$request->slug) {
            $request->merge(['slug' => str_slug($request->title)]);
        }
        $data = $note->update($request->all());

        if($data) {
            return response()->json([
                'success' => 1,
                'type' => 'update'
            ]);
        }
        return response()->json([
            'error' => 1
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PosterProgramms  $posterProgramms
     * @return \Illuminate\Http\Response
     */
    public function destroy(PosterProgramms $note)
    {
        $note->delete();

        return response()->json([
            'success' => 1
        ]);
    }


    public function adminSearch(Request $request) {
        $string = $request->string;
        $news = PosterProgramms::where('title', 'like', '%'.$string.'%')->orderBy('created_at', 'desc')->limit(100)->get();
        return response()->json($news);
    }
}
