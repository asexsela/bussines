<?php

namespace App\Http\Controllers;

use App\BlogNews;
use App\Author;
use App\Category;
use Carbon\Carbon;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
class BlogNewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = BlogNews::orderBy('created_at', 'desc')->paginate(10);
        return view('pages.admin.blogNews', compact('news'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = Category::all();
        $authors = Author::all();
        return view('pages.admin.create.blogNews', compact('category', 'authors'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!isset($request->isActive) && $request->isActive != '1') {
            $request->request->add(['isActive' => 0]);
        }
        if(!isset($request->themeIsDay) && $request->themeIsDay != '1') {
            $request->request->add(['themeIsDay' => 0]);
        }

        if(!$request->slug) {
            $request->merge(['slug' => str_slug($request->title)]);
        }

        if(!$request->published_at) {
            $request->request->add(['published_at' => Carbon::now()]);
        } else {
            $request->request->add(['published_at' => Carbon::parse($request->published_at)]);
        }
        
        
        $data = BlogNews::create($request->all());


        if($data) {
            return response()->json([
                'success' => 1,
                'type' => 'create'
            ]);
        }
        return response()->json([
            'error' => 1
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BlogNews  $blogNews
     * @return \Illuminate\Http\Response
     */
    public function show(BlogNews $news)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BlogNews  $blogNews
     * @return \Illuminate\Http\Response
     */
    public function edit(BlogNews $news)
    {
        $category = Category::all();
        $authors = Author::all();
        return view('pages.admin.edit.blogNews', compact('category', 'authors', 'news'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BlogNews  $blogNews
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BlogNews $news)
    {
        if(!isset($request->isActive) && $request->isActive != '1') {
            $request->request->add(['isActive' => 0]);
        }
        if(!isset($request->themeIsDay) && $request->themeIsDay != '1') {
            $request->request->add(['themeIsDay' => 0]);
        }
        if(!$request->slug) {
            $request->merge(['slug' => str_slug($request->title)]);
        }

        $data = $news->update($request->all());


        if($data) {
            return response()->json([
                'success' => 1,
                'type' => 'update'
            ]);
        }
        return response()->json([
            'error' => 1
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BlogNews  $blogNews
     * @return \Illuminate\Http\Response
     */
    public function destroy(BlogNews $news)
    {
        $old_img = $news->old_img;
        $views = $news->views;
        $likes = $news->likes;
        $dislikes = $news->dislikes;

        foreach($views as $view) {
            $view->delete();
        }

        foreach($likes as $like) {
            $like->delete();
        }

        foreach($dislikes as $dislike) {
            $dislike->delete();
        }

        $news->delete();
        
        return response()->json([
            'success' => public_path($old_img)
        ]);
    }

    public function adminSearch(Request $request) {
        $string = $request->string;
        $news = BlogNews::where('title', 'like', '%'.$string.'%')->orderBy('created_at', 'desc')->limit(100)->get();
        return response()->json($news);
    }
}
