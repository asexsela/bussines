<?php

namespace App\Http\Controllers;

use App\Menu;
use App\MenuItem;
use Illuminate\Http\Request;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menus = Menu::all();
        return view('pages.admin.menu', compact('menus'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.admin.create.menu');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $id = Menu::create($request->all())->id;

        foreach (json_decode($request->items) as $item) {
            MenuItem::create([
                'position' => $item->position,
                'name' => $item->name,
                'url' => $item->url,
                'menu_id' => $id
            ]);
        }
 
        if($id) {
            return response()->json([
                'success' => 1,
                'type' => 'create'
            ]);
        }
        return response()->json([
            'error' => 1
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function show(Menu $menu)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function edit(Menu $menu)
    {
        return view('pages.admin.edit.menu', compact('menu'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Menu $menu)
    {

        $data = $menu->update($request->all());

        foreach (json_decode($request->items) as $item) {
            $item_menu = MenuItem::find($item->id);
            if($item_menu) {
                $item_menu->update([
                    'position' => $item->position,
                    'name' => $item->name,
                    'url' => $item->url,
                    'img' => $item->img,
                ]);
            } else {
                MenuItem::create([
                    'position' => $item->position,
                    'name' => $item->name,
                    'url' => $item->url,
                    'menu_id' => $menu->id,
                    'img' => $menu->img
                ]);
            }


        }

 
        if($data) {
            return response()->json([
                'success' => 1,
                'type' => 'update'
            ]);
        }
        return response()->json([
            'error' => 1
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function destroy(Menu $menu)
    {
        
        foreach ($menu->items as $item) {
            $item->delete();
        }
        
        $menu->delete();

        return response()->json([
            'success' => 1
        ]);
    }
}
