<?php

namespace App\Http\Controllers;

use App\News;
use App\HeadingNews;
use App\Author;
use App\BlogNews;
use App\Voiting;
use App\Video;

use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index() {
        
        $date = Carbon::now()->subWeek();

        // $all_news = News::where('created_at', '>=', $date)->get();
        // $top_news = $all_news->sortByDesc(function ($news, $key) {
        //     return count($news->views);
        // })->slice(0, 5);
        $random_news = News::get()->random(1);

        $news_is_day = [
            'themeIsDay' => HeadingNews::orderby('published_at', 'desc')->where('published_at', '<=', Carbon::now())->where('themeIsDay', 1)->first() ?? $random_news[0],
            'news_last' => News::orderby('published_at', 'desc')->where('published_at', '<=', Carbon::now())->where('themeIsDay', '!=', 1)->where('isPartner', '!=', 1)->first(),
            'partners' => News::orderby('published_at', 'desc')->where('published_at', '<=', Carbon::now())->where('themeIsDay', '!=', 1)->where('isPartner', 1)->first() ?? $random_news[0]
        ];
        $top_news = News::orderBy('published_at', 'desc')
                        ->where('published_at', '<=', Carbon::now())
                        ->whereNotIn('id', [$news_is_day['news_last']->id, $news_is_day['partners']->id])
                        ->take(5)
                        ->get();

        // dd($random_news);
        // $top_news = News::orderBy('rating', 'desc')->limit(4)->get();
        $last_video = Video::orderby('created_at', 'desc')->first();
        $heading_news = HeadingNews::orderBy('published_at', 'desc')->where('published_at', '<=', Carbon::now())->whereNotIn('id', [$news_is_day['themeIsDay']->id])->limit(5)->get();
        $new_posts = BlogNews::orderBy('created_at', 'desc')->limit(4)->get();
        $best_posts = BlogNews::get()->random(4);
        $best_authors = Author::get()->random(4);
        $voiting = Voiting::where('isIndex', 1)->first();

        // dd($last_video);

        return view('pages.index', compact(
            'top_news', 
            'news_is_day',
            'heading_news',
            'new_posts',
            'best_posts',
            'best_authors',
            'voiting',
            'last_video'
        ));
    }
}
