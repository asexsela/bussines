<?php

namespace App\Http\Controllers;

use App\Poster;
use App\PosterCategory;
use Illuminate\Http\Request;

class PosterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($type = false)
    {
        if(!$type) {
            $category = PosterCategory::orderBy('title', 'desc')->first();
        } else {
            $category = PosterCategory::where('title', $type)->first();
        }
        // dd($category);  
        if(!$category) {
            return abort(404);
        }

        $categories = PosterCategory::orderBy('title', 'desc')->get();

        $posters = $category->posters;
        return view('pages.posters', compact('posters', 'categories', 'category'));
    }

    public function adminIndex() {

        $news = Poster::orderBy('created_at', 'desc')->paginate(10);
        return view('pages.admin.posters', compact('news'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = PosterCategory::all();
        
        return view('pages.admin.create.posters', compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!isset($request->isActive) && $request->isActive != '1') {
            $request->request->add(['isActive' => 0]);
        }
        if(!$request->slug) {
            $request->merge(['slug' => str_slug($request->title)]);
        }
        $data = Poster::create($request->all());

        if($data) {
            return response()->json([
                'success' => 1,
                'type' => 'create'
            ]);
        }
        return response()->json([
            'error' => 1
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Poster  $poster
     * @return \Illuminate\Http\Response
     */
    public function show(Poster $poster)
    {   
        $posters = Poster::orderBy('created_at', 'desc')
                            ->whereNotIn('id', [$poster->id])
                            ->where('category_id', $poster->category_id)
                            ->get();
        $count = $posters->count() > 4 ? 4 : $posters->count();
        $outher = $posters->random($count);
        $notes = $poster->notes;
        $programms = $poster->programms;
        return view('pages.poster', compact('poster', 'outher', 'notes', 'programms'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Poster  $poster
     * @return \Illuminate\Http\Response
     */
    public function edit(Poster $poster)
    {
        $news = $poster;
        $category = PosterCategory::all();
        return view('pages.admin.edit.posters', compact('news', 'category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Poster  $poster
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Poster $poster)
    {

        // return $request->all();
        if(!isset($request->isActive) && $request->isActive != '1') {
            $request->request->add(['isActive' => 0]);
        }
        if(!$request->slug) {
            $request->merge(['slug' => str_slug($request->title)]);
        }
        $data = $poster->update($request->all());

        if($data) {
            return response()->json([
                'success' => 1,
                'type' => 'update'
            ]);
        }
        return response()->json([
            'error' => 1
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Poster  $poster
     * @return \Illuminate\Http\Response
     */
    public function destroy(Poster $poster)
    {

        $poster->delete();

        return response()->json([
            'success' => 1
        ]);
    }

    public function adminSearch(Request $request) {
        $string = $request->string;
        $news = Poster::where('title', 'like', '%'.$string.'%')->orderBy('created_at', 'desc')->limit(100)->get();
        return response()->json($news);
    }
}
