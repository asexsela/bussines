<?php

namespace App\Http\Controllers;

use App\Page;
use App\Image;
use App\MenuItem;
use Carbon\Carbon;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages = Page::orderBy('created_at', 'desc')->paginate(10);
        return view('pages.admin.page', compact('pages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.admin.create.page');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
                 
        if(!$request->slug) {
            $request->merge(['slug' => str_slug($request->title)]);
        }

        $validation = Validator::make($request->all(), [
            'title' => 'required',
            'slug' => 'unique:pages',
        ]);
        
        if($validation->passes())  {
            
            $id = Page::create($request->all())->id;

            if($id) {
                return response()->json([
                    'success' => 1,
                    'type' => 'create',
                ]);
            }

        } else {
            return response()->json([
                'error' => 2
            ]);
        }

        return response()->json([
            'error' => 1
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function show(Page $page)
    {
        $item_menu = MenuItem::where('name', $page->title)->first();
        
        if($item_menu ) {
            $menu = $item_menu->menu->items;
        } else {
            $menu = false;
        }

        return view('pages.page', compact('page', 'menu'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function edit(Page $page)
    {
        return view('pages.admin.edit.page', compact('page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Page $page)
    {
        if(!$request->slug) {
            $request->merge(['slug' => str_slug($request->title)]);
        }

        // return $request->all();

        $validation = Validator::make($request->all(), [
            'title' => 'required',
        ]);
        
        if($validation->passes())  {
            
            $page->update($request->all());

            if($page) {
                return response()->json([
                    'success' => 1,
                    'type' => 'update',
                ]);
            }

        } else {
            return response()->json([
                'error' => 1
            ]);
        }

        return response()->json([
            'error' => 1
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function destroy(Page $page)
    {
        $page->delete();
        
        return response()->json([
            'success' => 1
        ]);
    }

    public function adminSearch(Request $request) {
        $string = $request->string;
        $news = Page::where('title', 'like', '%'.$string.'%')->orderBy('created_at', 'desc')->limit(100)->get();
        return response()->json($news);
    }

}
