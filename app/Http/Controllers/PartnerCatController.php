<?php

namespace App\Http\Controllers;

use App\PartnerCat;
use App\Partner;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PartnerCatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($type = false)
    {

        if(!$type) {
            $type = PartnerCat::get()->first();
        } else {
            $type = PartnerCat::where('title', $type)->first();
        }

        $categories = PartnerCat::all();
        $partners = $type->partners()->paginate(8);

        return view('pages.partners', compact('categories', 'type', 'partners'));
    }

    public function adminIndex() {
        $partners = PartnerCat::orderBy('created_at', 'desc')->paginate(8);
        return view('pages.admin.partner_category', compact('partners'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = PartnerCat::all();
        return view('pages.admin.create.partner_category', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'title' => 'required'
        ]);


        if($validation->passes())  {

            $id = PartnerCat::create($request->all())->id;

            $new_cat = PartnerCat::find($id);
            $new_cat->outher_id = $id;
            $new_cat->save();

            return response()->json([
                'success' => 1,
                'type' => 'create'
            ]);

        } 

        return response()->json([
            'error' => 1,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PartnerCat  $partnerCat
     * @return \Illuminate\Http\Response
     */
    public function show(PartnerCat $partnerCat)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PartnerCat  $partnerCat
     * @return \Illuminate\Http\Response
     */
    public function edit(PartnerCat $category)
    {
        return view('pages.admin.edit.partner_category', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PartnerCat  $partnerCat
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PartnerCat $category)
    {
        $validation = Validator::make($request->all(), [
            'title' => 'required'
        ]);


        if($validation->passes())  {

            $id = $category->update($request->all());

            return response()->json([
                'success' => 1,
                'type' => 'update'
            ]);

        } 

        return response()->json([
            'error' => 1,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PartnerCat  $partnerCat
     * @return \Illuminate\Http\Response
     */
    public function destroy(PartnerCat $category)
    {
        $partners = Partner::where('partnercat_id', $category->outher_id)->get();

        if($partners->count() > 0) {
            foreach ($partners as $item) {
                if(File::exists(public_path($item->img))) {
                    File::delete(public_path($item->img));
                }
                $item->delete();
            }
        }

        $category->delete();

        return response()->json([
            'success' => 1
        ]);
    }

    public function adminSearch(Request $request) {
        $string = $request->string;
        $news = PartnerCat::where('title', 'like', '%'.$string.'%')->orderBy('created_at', 'desc')->limit(100)->get();
        return response()->json($news);
    }
}
