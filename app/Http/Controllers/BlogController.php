<?php

namespace App\Http\Controllers;

use App\Author;
use App\BlogNews;
use App\BlogViews;
use App\BlogLikes;
use App\BlogDislikes;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function index() {

        $authors = Author::where('active', 1)->get()->random(6);
        $news = BlogNews::orderBy('created_at', 'desc')->paginate(15);
        
        return view('pages.blogs', compact('authors', 'news'));
    }

    public function views(Request $request) {

        $uid = $request->uid;
        $news_id = $request->id;

        // return $request->all();

        $views = BlogViews::where('uid', $uid)->where('news_id', $news_id)->first();

        if(!$views) {

            $item = new BlogViews;
            $item->news_id = $news_id;
            $item->uid = $uid;
            $item->save();

            return 1;
        }
        return 2;
    }

    public function likes(Request $request) {

        $uid = $request->uid;
        $news_id = $request->id;

        // return $request->all();

        $likes = BlogLikes::where('uid', $uid)->where('news_id', $news_id)->first();
        $dislikes = BlogDislikes::where('uid', $uid)->where('news_id', $news_id)->first();

        if($likes) {

            $likes->delete();

            return response()->json([
                'data' => 'unlike'
            ]);


        } else {

            if(!$dislikes) { 

                $item = new BlogLikes;
                $item->news_id = $news_id;
                $item->uid = $uid;
                $item->save();
    
                return response()->json([
                    'data' => 'like'
                ]);
    
            }

        }

        return 2;

    }

    public function dislikes(Request $request) {

        $uid = $request->uid;
        $news_id = $request->id;

        $likes = BlogLikes::where('uid', $uid)->where('news_id', $news_id)->first();
        $dislikes = BlogDislikes::where('uid', $uid)->where('news_id', $news_id)->first();

        if($dislikes) {

            $dislikes->delete();

            return response()->json([
                'data' => 'undislike'
            ]);


        } else {

            if(!$likes) {
 
                $item = new BlogDislikes;
                $item->news_id = $news_id;
                $item->uid = $uid;
                $item->save();
    
                return response()->json([
                    'data' => 'dislike'
                ]);
    
            }

        }
        return 2;

    }
}
