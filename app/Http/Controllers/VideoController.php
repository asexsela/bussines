<?php

namespace App\Http\Controllers;

use App\Video;
use App\VideoViews;
use App\VideoLikes;
use App\VideoDislikes;
use Carbon\Carbon;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class VideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $videos = Video::orderBy('created_at', 'desc')->paginate(22);
        return view('pages.videos', compact('videos'));
    }

    public function adminIndex() {

        $videos = Video::orderBy('created_at', 'desc')->paginate(10);
        return view('pages.admin.video', compact('videos'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.admin.create.video');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!isset($request->isActive) && $request->isActive != '1') {
            $request->request->add(['isActive' => 0]);
        }

        if(!$request->slug) {
            $request->merge(['slug' => str_slug($request->title)]);
        }

        $data = Video::create($request->all());


        if($data) {
            return response()->json([
                'success' => 1,
                'type' => 'create'
            ]);
        }
        return response()->json([
            'error' => 1
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function show(Video $video)
    {
        $outhers = Video::get()->random(12);
        return view('pages.video', compact('video', 'outhers'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function edit(Video $video)
    {
        return view('pages.admin.edit.video', compact('video'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Video $video)
    {
        if(!isset($request->isActive) && $request->isActive != '1') {
            $request->request->add(['isActive' => 0]);
        }
        if(!$request->slug) {
            $request->merge(['slug' => str_slug($request->title)]);
        }

        $data = $video->update($request->all());


        if($data) {
            return response()->json([
                'success' => 1,
                'type' => 'update'
            ]);
        }
        return response()->json([
            'error' => 1
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function destroy(Video $video)
    {
        $views = $video->views;
        $likes = $video->likes;
        $dislikes = $video->dislikes;

        foreach($views as $view) {
            $view->delete();
        }

        foreach($likes as $like) {
            $like->delete();
        }

        foreach($dislikes as $dislike) {
            $dislike->delete();
        }


        $video->delete();
        
        return response()->json([
            'success' => 1
        ]);
    }

    public function views(Request $request) {

        $uid = $request->uid;
        $video_id = $request->id;

        // return $request->all();

        $views = VideoViews::where('uid', $uid)->where('video_id', $video_id)->first();

        if(!$views) {

            $item = new VideoViews;
            $item->video_id = $video_id;
            $item->uid = $uid;
            $item->save();

            return 1;
        }
        return 2;
    }

    public function likes(Request $request) {

        $uid = $request->uid;
        $video_id = $request->id;

        // return $request->all();

        $likes = VideoLikes::where('uid', $uid)->where('video_id', $video_id)->first();
        $dislikes = VideoDislikes::where('uid', $uid)->where('video_id', $video_id)->first();

        if($likes) {

            $likes->delete();

            return response()->json([
                'data' => 'unlike'
            ]);


        } else {

            if(!$dislikes) { 

                $item = new VideoLikes;
                $item->video_id = $video_id;
                $item->uid = $uid;
                $item->save();
    
                return response()->json([
                    'data' => 'like'
                ]);
    
            }

        }

        return 2;

    }

    public function dislikes(Request $request) {

        $uid = $request->uid;
        $video_id = $request->id;

        $likes = VideoLikes::where('uid', $uid)->where('video_id', $video_id)->first();
        $dislikes = VideoDislikes::where('uid', $uid)->where('video_id', $video_id)->first();

        if($dislikes) {

            $dislikes->delete();

            return response()->json([
                'data' => 'undislike'
            ]);


        } else {

            if(!$likes) {
 
                $item = new VideoDislikes;
                $item->video_id = $video_id;
                $item->uid = $uid;
                $item->save();
    
                return response()->json([
                    'data' => 'dislike'
                ]);
    
            }

        }
        return 2;

    }

    public function adminSearch(Request $request) {
        $string = $request->string;
        $news = Video::where('title', 'like', '%'.$string.'%')->orderBy('created_at', 'desc')->limit(100)->get();
        return response()->json($news);
    }
}
