<?php

namespace App\Http\Controllers;

use App\Author;
use App\BlogNews;
use Carbon\Carbon;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class AuthorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $authors = Author::orderBy('created_at', 'desc')->where('active', 1)->paginate(10);
        $best_posts = BlogNews::get()->random(4);
        $best_authors = Author::get()->where('active', 1)->random(4);
        return view('pages.authors', compact('authors', 'best_posts', 'best_authors'));
    }

    public function adminIndex()
    {
        $authors = Author::orderBy('created_at', 'desc')->paginate(10);
        return view('pages.admin.author', compact('authors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.admin.create.author');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!isset($request->active) && $request->active != '1') {
            $request->request->add(['active' => 0]);
        }
        
        $id = Author::create($request->all())->id;

        $new_author= Author::find($id);
        $new_author->outher_id = $id;
        $new_author->save();

        if($id) {
            return response()->json([
                'success' => 1,
                'type' => 'create'
            ]);
        }
        return response()->json([
            'error' => 1
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Author  $author
     * @return \Illuminate\Http\Response
     */
    public function show(Author $author)
    {
        $news = $author->news()->paginate(10);
        $outher_authors = Author::get()->random(3);
        return view('pages.author_blogs', compact('author', 'news', 'outher_authors'));
    }


    public function article(BlogNews $article)
    {
        $author = $article->author;
        $outher_news = BlogNews::get()->random(4);
        return view('pages.article', compact('author', 'article', 'outher_news'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Author  $author
     * @return \Illuminate\Http\Response
     */
    public function edit(Author $author)
    {
        return view('pages.admin.edit.author', compact('author'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Author  $author
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Author $author)
    {
        if(!isset($request->active) && $request->active != '1') {
            $request->request->add(['active' => 0]);
        }
        $data = $author->update($request->all());

        if($data) {
            return response()->json([
                'success' => 1,
                'type' => 'update'
            ]);
        }
        return response()->json([
            'error' => 1
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Author  $author
     * @return \Illuminate\Http\Response
     */
    public function destroy(Author $author)
    {

        $news = BlogNews::where('author_id', $author->outher_id)->get();

        if($news->count() > 0) {
            foreach ($news as $item) {
                $item->delete();
            }
        }

        $author->delete();
        
        return response()->json([
            'success' => 1
        ]);
    }


    public function adminSearch(Request $request) {
        $string = $request->string;
        $news = Author::where('title', 'like', '%'.$string.'%')->orderBy('created_at', 'desc')->limit(100)->get();
        return response()->json($news);
    }
}
