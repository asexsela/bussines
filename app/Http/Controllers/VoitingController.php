<?php

namespace App\Http\Controllers;

use App\Voiting;
use App\VoitingItem;
use App\VoitingCheck;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

use App\Http\Resources\VoitingResource;

class VoitingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.voting');
    }

    public function indexApi($uid)
    {
        return response()->json([
            'data' => VoitingResource::collection(Voiting::orderBy('created_at', 'desc')->get()),
            'check' => VoitingCheck::where('uid', $uid)->get()
        ]);
    }

    public function adminIndex()
    {
        $voiting = Voiting::paginate(10);
        return view('pages.admin.voiting', compact('voiting'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.admin.create.voiting');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request->all();

        if(!isset($request->isIndex) && $request->isIndex != '1') {
            $request->request->add(['isIndex' => 0]);
        }

        if($request->isIndex == 1) {
            $arr = Voiting::all();

            foreach ($arr as $a) {
                $a->isIndex = 0;
                $a->save();
            }
        }

        $validation = Validator::make($request->all(), [
            'title' => 'required'
        ]);


        if($validation->passes())  {

            $id = Voiting::create([
                'title' => $request->title,
                'date_end' => Carbon::parse($request->date_end),
                'isIndex' => $request->isIndex,
            ])->id;

            $req_items = $request->items;

            foreach ($req_items as $key => $value) {

                VoitingItem::create([
                    'title' => $value,
                    'count' => 0,
                    'voiting_id' => $id,
                ]);

            }

            return response()->json([
                'success' => 1,
                'type' => 'create'
            ]);

        } 

       
        return response()->json([
            'error' => 1,
        ]);

        


        // return $items;

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Voiting  $voiting
     * @return \Illuminate\Http\Response
     */
    public function show(Voiting $voiting)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Voiting  $voiting
     * @return \Illuminate\Http\Response
     */
    public function edit(Voiting $voiting)
    {
        return view('pages.admin.edit.voiting', compact('voiting'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Voiting  $voiting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Voiting $voiting)
    {

        if(!isset($request->isIndex) && $request->isIndex != '1') {
            $request->merge(['isIndex' => 0]);
        }
        // return $request->all();
        
        if($request->isIndex == '1') {
            $arr = Voiting::all();

            foreach ($arr as $a) {
                if($voiting->id !== $a->id) {
                    $a->isIndex = 0;
                    $a->save();
                }
            }
        }

        // return $request->all();

        $validation = Validator::make($request->all(), [
            'title' => 'required'
        ]);


        if($validation->passes())  {

            $voiting->update([
                'title' => $request->title,
                'date_end' => Carbon::parse($request->date_end),
                'isIndex' => $request->isIndex,
            ]);


            $req_items = $request->items;

            foreach ($req_items as $key => $value) {
                
                $check = VoitingItem::where('title', $value)
                                    ->where('voiting_id', $voiting->id)
                                    ->first();
                if(!$check) {
                    VoitingItem::create([
                        'title' => $value,
                        'count' => 0,
                        'voiting_id' => $voiting->id,
                    ]);
                }

            }

            return response()->json([
                'success' => 1,
                'type' => 'update'
            ]);

        } 

       
        return response()->json([
            'error' => 1,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Voiting  $voiting
     * @return \Illuminate\Http\Response
     */
    public function destroyItem(VoitingItem $item)
    {
        $item->delete();

        return response()->json([
            'success' => 1
        ]);
    }

    public function destroy(Voiting $voiting)
    {
        $items = VoitingItem::where('voiting_id', $voiting->id)->get();

        if($items->count() > 0) {
            foreach ($items as $item) {
                $item->delete();
            }
        }

        $voiting->delete();

        return response()->json([
            'success' => 1
        ]);
    }

    public function toVote(Request $request) {

        $check = VoitingCheck::create($request->all());
        
        $item = VoitingItem::find($request->item_id);
        $item->count++;
        $item->save();

        return response()->json([
            'success' => 1
        ]);
    }

    public function adminSearch(Request $request) {
        $string = $request->string;
        $news = Voiting::where('title', 'like', '%'.$string.'%')->orderBy('created_at', 'desc')->limit(100)->get();
        return response()->json($news);
    }
}
