<?php

namespace App\Http\Controllers;

use App\Banner;
use App\BannerViews;
use App\BannerClicks;

use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banners = Banner::orderBy('created_at', 'desc')->paginate(10);
        return view('pages.admin.banner', compact('banners'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.admin.create.banner');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $data = Banner::create($request->all());

        if($data) {
            return response()->json([
                'success' => 1,
                'type' => 'create'
            ]);
        }
        return response()->json([
            'error' => 1
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function show(Banner $banner)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function edit(Banner $banner)
    {
        return view('pages.admin.edit.banner', compact('banner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Banner $banner)
    {


        $data = $banner->update($request->all());


        if($data) {
            return response()->json([
                'success' => 1,
                'type' => 'update'
            ]);
        }
        return response()->json([
            'error' => 1
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function destroy(Banner $banner)
    {

        $banner->delete();
        
        return response()->json([
            'success' => 1
        ]);
    }

    public function views(Request $request) {

        $uid = $request->uid;
        $type = $request->type;
        $arrs = $request->arr;

        foreach ($arrs as $id) {
            $view = BannerViews::where('uid', $uid)
                                ->where('banner_id', $id)
                                ->where('type', $type)
                                ->first();
            if(!$view) {
                $new_view = new BannerViews;
                $new_view->uid = $uid;
                $new_view->banner_id = $id;
                $new_view->type = $type;
                $new_view->save();
            }
        }

    }

    public function clicks(Request $request) {

        // return $request->all();

        $uid = $request->uid;
        $id = $request->arr;
        $type = $request->type;

        $click = BannerClicks::where('uid', $uid)
                              ->where('banner_id', $id)
                              ->where('type', $type)
                              ->first();

        if(!$click) {
            $new_view = new BannerClicks;
            $new_view->uid = $uid;
            $new_view->banner_id = $id;
            $new_view->type = $type;
            $new_view->save();
        }
    }

    public function adminSearch(Request $request) {
        $string = $request->string;
        $news = Banner::where('title', 'like', '%'.$string.'%')->orderBy('created_at', 'desc')->limit(100)->get();
        return response()->json($news);
    }
}
