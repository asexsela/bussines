<?php

namespace App\Http\Controllers;

use App\PosterNotes;
use App\Poster;
use Illuminate\Http\Request;

class PosterNotesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = PosterNotes::orderBy('created_at', 'desc')->paginate(10);
        return view('pages.admin.poster_notes', compact('news'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = Poster::all();
        return view('pages.admin.create.poster_notes', compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!isset($request->isActive) && $request->isActive != '1') {
            $request->request->add(['isActive' => 0]);
        }
        if(!$request->slug) {
            $request->merge(['slug' => str_slug($request->title)]);
        }
        $data = PosterNotes::create($request->all());

        if($data) {
            return response()->json([
                'success' => 1,
                'type' => 'create'
            ]);
        }
        return response()->json([
            'error' => 1
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PosterNotes  $posterNotes
     * @return \Illuminate\Http\Response
     */
    public function show(PosterNotes $note)
    {
        $count = ($note->poster->programms->count() < 3) ? $note->poster->programms->count() : 3;
        $outher = $note->poster->programms->random($count);
        return view('pages.poster_notes', compact('note', 'outher'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PosterNotes  $posterNotes
     * @return \Illuminate\Http\Response
     */
    public function edit(PosterNotes $note)
    {
        $news = $note;
        $category = Poster::all();
        return view('pages.admin.edit.poster_notes', compact('news', 'category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PosterNotes  $posterNotes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PosterNotes $note)
    {
        if(!isset($request->isActive) && $request->isActive != '1') {
            $request->request->add(['isActive' => 0]);
        }
        if(!$request->slug) {
            $request->merge(['slug' => str_slug($request->title)]);
        }
        $data = $note->update($request->all());

        if($data) {
            return response()->json([
                'success' => 1,
                'type' => 'update'
            ]);
        }
        return response()->json([
            'error' => 1
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PosterNotes  $posterNotes
     * @return \Illuminate\Http\Response
     */
    public function destroy(PosterNotes $note)
    {
        $note->delete();

        return response()->json([
            'success' => 1
        ]);
    }
}
