<?php

namespace App\Http\Controllers;

use App\MenuItem;
use Illuminate\Http\Request;

class MenuItemController extends Controller
{

    public function destroy(MenuItem $item)
    {
        $item->delete();

        return response()->json([
            'success' => 1
        ]);
    }
}
