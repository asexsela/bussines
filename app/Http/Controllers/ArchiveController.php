<?php

namespace App\Http\Controllers;

use App\Archive;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ArchiveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($type = false)
    {
        if(!$type) {
            $archive = Archive::orderBy('title', 'desc')->first();
        } else {
            $archive = Archive::where('title', $type)->first();
        }
        
        $archives = Archive::orderBy('title', 'desc')->get();

        if(!$archive) {
            return abort(404);
        }

        $numbers = $archive->numbers;

        return view('pages.arhives_news', compact('archives', 'archive', 'numbers'));

    }

    public function adminIndex() {

        $archives = Archive::orderBy('created_at', 'desc')->paginate(10);

        return view('pages.admin.archive', compact('archives'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.admin.create.archive');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'title' => 'required'
        ]);


        if($validation->passes())  {
            $new = Archive::create([
                'title' => $request->title,
            ])->id;

            $outher_id = Archive::find($new);
            $outher_id->outher_id = $new;
            $outher_id->save();

            return response()->json([
                'success' => 1,
                'type' => 'create'
            ]);

        } 

        return response()->json([
            'error' => 1,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Archive  $archive
     * @return \Illuminate\Http\Response
     */
    public function show(Archive $archive)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Archive  $archive
     * @return \Illuminate\Http\Response
     */
    public function edit(Archive $archive)
    {
        return view('pages.admin.edit.archive', compact('archive'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Archive  $archive
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Archive $archive)
    {

        $validation = Validator::make($request->all(), [
            'title' => 'required'
        ]);


        if($validation->passes())  {
            $archive->update([
                'title' => $request->title,
            ]);

            return response()->json([
                'success' => 1,
                'type' => 'create'
            ]);

        } 

        return response()->json([
            'error' => 1,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Archive  $archive
     * @return \Illuminate\Http\Response
     */
    public function destroy(Archive $archive)
    {
        $archive->delete();

        return response()->json([
            'success' => 1
        ]);
    }

    public function adminSearch(Request $request) {
        $string = $request->string;
        $news = Archive::where('title', 'like', '%'.$string.'%')->orderBy('created_at', 'desc')->limit(100)->get();
        return response()->json($news);
    }
}
