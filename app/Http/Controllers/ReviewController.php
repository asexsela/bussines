<?php

namespace App\Http\Controllers;

use App\Review;
use Illuminate\Http\Request;

class ReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reviews = Review::orderBy('created_at', 'desc')->where('active', 1)->paginate(10);
        return view('pages.reviews', compact('reviews'));
    }

    public function adminIndex() {
        $reviews = Review::orderBy('created_at', 'desc')->paginate(10);
        return view('pages.admin.review', compact('reviews'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.admin.create.review');
    }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!isset($request->active) && $request->active != '1') {
            $request->request->add(['active' => 0]);
        }

        // return $request->all();

        if($request->has('admin')) {

            $data = Review::create($request->all());

            if($data) {
                return response()->json([
                    'success' => 1,
                    'type' => 'create'
                ]);
            }
            return response()->json([
                'error' => 1
            ]);

        } else {

            $name = $request->firstName.' '.$request->secondName;
            $msg = $request->body;
            $active = $request->active;

            SendController::reviewmail([
                'name' => $name,
                'mess' => $msg
            ]);
    
            $data = Review::create([
                'name' => $name,
                'body' => $msg,
                'active' => $active
            ]);
    
            if($data) {
                return redirect()->back()->with('message', 'Спасибо за отзыв, скоро он появится на сайте!');
            }
    
            return redirect()->back()->with('error', 'Вы не можете оставлять отзывы!');
        }


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function edit(Review $review)
    {
        return view('pages.admin.edit.review', compact('review'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Review  $review
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Review $review, $type)
    {

        if(!isset($request->active) && $request->active != '1') {
            $request->request->add(['active' => 0]);
        }

        if($type === 'published') {

            $review->active = 1;
            $review->save();

        } else if($type === 'admin') {

            $data = $review->update($request->all());

            if($data) {
                return response()->json([
                    'success' => 1,
                    'type' => 'update'
                ]);
            }
            return response()->json([
                'error' => 1
            ]);

        } else {

            $review->active = 0;
            $review->save();

        }

        return response()->json([
            'success' => 1,
            'type' => 'update'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Review  $review
     * @return \Illuminate\Http\Response
     */
    public function destroy(Review $review)
    {
        $review->delete();
        
        return response()->json([
            'success' => 1,
        ]);

    }

    public function adminSearch(Request $request) {
        $string = $request->string;
        $news = Review::where('name', 'like', '%'.$string.'%')->orderBy('created_at', 'desc')->limit(100)->get();
        return response()->json($news);
    }
}
