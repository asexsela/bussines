<?php

namespace App\Http\Controllers;

use App\Album;
use App\Image;
use Carbon\Carbon;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class AlbumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $albums = Album::orderBy('created_at', 'desc')->paginate(5);
        return view('pages.photos', compact('albums'));
    }

    public function adminIndex()
    {
        $albums = Album::orderBy('created_at', 'desc')->paginate(10);
        return view('pages.admin.album', compact('albums'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.admin.create.album');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request->all();

        if(!isset($request->isActive) && $request->isActive != '1') {
            $request->request->add(['isActive' => 0]);
        }

                
        if(!$request->slug) {
            $request->merge(['slug' => str_slug($request->title)]);
        }

        $validation = Validator::make($request->all(), [
            'title' => 'required',
        ]);
        

        if($validation->passes())  {
            
            $id = Album::create($request->all())->id;
            
            $new_album = Album::find($id);
            $new_album->outher_id = $id;
            $new_album->save();

            if($request->images) {
                $gallery = $this->addGallery(json_decode($request->images), $id);
            }

            
            if($id) {
                return response()->json([
                    'success' => 1,
                    'type' => 'create',
                ]);
            }

        } else {
            return response()->json([
                'error' => 2
            ]);
        }

        return response()->json([
            'error' => 1
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Album  $album
     * @return \Illuminate\Http\Response
     */
    public function show(Album $album)
    {
        $images = $album->images()->paginate(11);
        return view('pages.photo', compact('album', 'images'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Album  $album
     * @return \Illuminate\Http\Response
     */
    public function edit(Album $album)
    {
        $gallery = $album->images;
        return view('pages.admin.edit.album', compact('gallery', 'album'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Album  $album
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Album $album)
    {
        if(!isset($request->isActive) && $request->isActive != '1') {
            $request->request->add(['isActive' => 0]);
        }
                
        if(!$request->slug) {
            $request->merge(['slug' => str_slug($request->title)]);
        }

        $validation = Validator::make($request->all(), [
            'title' => 'required',
        ]);
        
        if($validation->passes())  {
            
            
            $album->update($request->all());

            if ($request->images) {
                $gallery = $this->addGallery(json_decode($request->images), $album->id);
            }

            if($album) {
                return response()->json([
                    'success' => 1,
                    'type' => 'update',
                    'data' => $album->id,
                ]);
            }

        } else {
            return response()->json([
                'error' => 2
            ]);
        }

        return response()->json([
            'error' => 1
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Album  $album
     * @return \Illuminate\Http\Response
     */
    public function destroy(Album $album)
    {
        $album->delete();
        return response()->json([
            'success' => 1
        ]);
    }

    public function addGallery($images, $id) {

        foreach ($images as $image) {
            $img = new Image;
            $img->src = '/images/'.$image;
            $img->album_id = $id;
            $img->save();
        }

        return true;
    }

    public function deleteImg(Image $img) {        
        $img->delete();
        return response()->json([
            'success' => 1
        ]);
    }

    public function adminSearch(Request $request) {
        $string = $request->string;
        $news = Album::where('title', 'like', '%'.$string.'%')->orderBy('created_at', 'desc')->limit(100)->get();
        return response()->json($news);
    }
}
