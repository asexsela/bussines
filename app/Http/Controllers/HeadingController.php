<?php

namespace App\Http\Controllers;

use App\Heading;
use App\Parse;
use App\News;
use App\Album;
use App\Image;
use App\Video;
use App\Author;
use App\Archive;
use App\BlogNews;
use App\Number;
use Carbon\Carbon;
use App\HeadingNews;
use App\PartnerCat;
use App\Partner;
use App\Category;
use App\Letter;
use App\Project;
use App\Poster;
use App\Review;
use App\PosterNotes;
use App\PosterProgramms;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use Orchestra\Parser\Xml\Facade as XmlParser;

class HeadingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        $headings = Heading::where('active', 1)->get();

        return view('pages.headings', compact('categories', 'headings'));    
    }

    public function adminIndex() {

        $headings = Heading::orderBy('created_at', 'desc')->paginate(10);
        return view('pages.admin.heading', compact('headings'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('pages.admin.create.heading', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!isset($request->active) && $request->active != '1') {
            $request->request->add(['active' => 0]);
        }

        $rules = [
            // 'file' => 'image|mimes:jpeg,png,jpg,gif',
            'name' => 'required'
        ];

        $messages = [
            'name.required' => 'Заполните это поле',
            // 'file.mimes' => 'Разрешенные форматы изображений jpeg,png,jpg,gif',
            // 'file.image' => 'Загрузите изображение',
        ];

        $validation = Validator::make($request->all(), $rules, $messages);


        if($validation->passes())  {
            if(!$request->slug) {
                $request->merge(['slug' => str_slug($request->name)]);
            }


            Heading::create($request->all());

            return response()->json([
                'success' => 1,
                'type' => 'create'
            ]);

        } 

        return response()->json([
            'error' => 1,
            'data' => $validation->errors()
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Heading  $heading
     * @return \Illuminate\Http\Response
     */
    public function show($slug, $type = 'all', $d = null)
    {   

        $heading = Heading::where('slug', $slug)->first();

        if($type === 'all') {

            $news = $heading->news()
                            ->paginate(17);
            return view('pages.heading', compact('heading', 'news', 'type'));

        } else if($type === 'week') {

            $date = Carbon::now()->subWeek();

            $news = $heading->news()
                            ->where('created_at', '>=', $date)
                            ->paginate(17);

            return view('pages.heading', compact('heading', 'news', 'type'));

        } else if($type === 'month') {

            $date = Carbon::now()->subMonth();

            $news = $heading->news()
                            ->where('created_at', '>=', $date)
                            ->paginate(17);

            return view('pages.heading', compact('heading', 'news', 'type'));

        } else if($type === 'date') {

            $Data = explode(",", $d);

            $news = $heading->news()
                        ->whereDate('created_at', '>=', $Data[0])
                        ->whereDate('created_at', '<=', $Data[1])
                        ->paginate(17);
                        
            return view('pages.heading', compact('heading', 'news', 'type'));
        }

        return abort(404);



    }

    public function showCategory($slug, $type = 'all', $d = null)
    {   
        $heading = Category::where('slug', $slug)->first();
        if($type === 'all') {

            $news = $heading->news()
                            ->paginate(16);
            return view('pages.heading', compact('heading', 'news', 'type'));

        } else if($type === 'week') {

            $date = Carbon::now()->subWeek();

            $news = $heading->news()
                            ->where('created_at', '>=', $date)
                            ->paginate(16);

            return view('pages.heading', compact('heading', 'news', 'type'));

        } else if($type === 'month') {

            $date = Carbon::now()->subMonth();

            $news = $heading->news()
                            ->where('created_at', '>=', $date)
                            ->paginate(16);

            return view('pages.heading', compact('heading', 'news', 'type'));

        } else if($type === 'date') {

            $Data = explode(",", $d);

            $news = $heading->news()
                        ->whereDate('created_at', '>=', $Data[0])
                        ->whereDate('created_at', '<=', $Data[1])
                        ->paginate(16);
            
            return view('pages.heading', compact('heading', 'news', 'type'));
        }

        return abort(404);


    }

    public function showNews($slug, $id)
    {   
        $heading = Heading::where('slug', $slug)->first();
        $news = $heading->news()->find($id);
        $date = Carbon::now()->subMonth();
        $count = $heading->news()->where('created_at', '>=', $date)->get()->count() < 3 ? $heading->news()->where('created_at', '>=', $date)->get()->count() : 3;
        $outher = $heading->news()->where('created_at', '>=', $date)->get()->random($count);

        return view('pages.heading_page', compact('news', 'heading', 'outher'));
    }

    public function showNewsCategory($slug, $id)
    {   
        $heading = Category::where('slug', $slug)->first();
        $news = $heading->news()->find($id);
        $count = 3;
        if($heading->news()->get()->count() < $count) {
            $count = $heading->news()->get()->count();
        }
        $outher = $heading->news()->get()->random($count);

        return view('pages.heading_page', compact('news', 'heading', 'outher'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Heading  $heading
     * @return \Illuminate\Http\Response
     */
    public function edit(Heading $heading)
    {
        $categories = Category::all();
        return view('pages.admin.edit.heading', compact('heading', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Heading  $heading
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Heading $heading)
    {
        if(!isset($request->active) && $request->active != '1') {
            $request->request->add(['active' => 0]);
        }

        $rules = [
            // 'file' => 'image|mimes:jpeg,png,jpg,gif',
            'name' => 'required'
        ];

        $messages = [
            'name.required' => 'Заполните это поле',
            // 'file.mimes' => 'Разрешенные форматы изображений jpeg,png,jpg,gif',
            // 'file.image' => 'Загрузите изображение',
        ];

        $validation = Validator::make($request->all(), $rules, $messages);


        if($validation->passes())  {
            if(!$request->slug) {
                $request->merge(['slug' => str_slug($request->name)]);
            }


            $heading->update($request->all());

            return response()->json([
                'success' => 1
            ]);

        } 

        return response()->json([
            'error' => 1,
            'data' => $validation->errors()
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Heading  $heading
     * @return \Illuminate\Http\Response
     */
    public function destroy(Heading $heading)
    {
        $heading->delete();

        return response()->json([
            'success' => 1
        ]);
    }

    public function adminSearch(Request $request) {
        $string = $request->string;
        $news = Heading::where('name', 'like', '%'.$string.'%')->orderBy('created_at', 'desc')->limit(100)->get();
        return response()->json($news);
    }

}
