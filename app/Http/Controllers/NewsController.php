<?php

namespace App\Http\Controllers;

use App\News;
use App\Views;
use App\Likes;
use App\Dislikes;
use App\Heading;
use App\Category;
use App\Author;
use Carbon\Carbon;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($type = 'all', $d = null)
    {
        $partner_count = News::where('isPartner', 1)->where('published_at', '<=', Carbon::now())->get()->count();
        if($partner_count < 3) {
            $partner_news = News::where('isPartner', 1)->where('published_at', '<=', Carbon::now())->orderBy('created_at', 'desc')->take($partner_count)->get();
        } else {
            $partner_news = News::where('isPartner', 1)->where('published_at', '<=', Carbon::now())->orderBy('created_at', 'desc')->take(3)->get();
        }

        if($type === 'all') {

            $news = News::orderBy('created_at', 'desc')->where('published_at', '<=', Carbon::now())->paginate(15);
            
            return view('pages.news', compact('news', 'type', 'partner_news'));

        } else if($type === 'week') {

            $date = Carbon::now()->subWeek();
            $news = News::where('created_at', '>=', $date)
                        ->where('published_at', '<=', Carbon::now())
                        ->orderBy('created_at', 'desc')
                        ->paginate(15);
            
            return view('pages.news', compact('news', 'type', 'partner_news'));

        } else if($type === 'month') {

            $date = Carbon::now()->subMonth();
            $news = News::where('created_at', '>=', $date)
                        ->where('published_at', '<=', Carbon::now())
                        ->orderBy('created_at', 'desc')
                        ->paginate(15);
            
            return view('pages.news', compact('news', 'type', 'partner_news'));

        } else if($type === 'date') {
            
            $Data = explode(",", $d);
            
            $news = News::whereDate('created_at', '>=', $Data[0])
                        ->whereDate('created_at', '<=', $Data[1])
                        ->where('published_at', '<=', Carbon::now())
                        ->orderBy('created_at', 'desc')
                        ->paginate(15);
            
            
            return view('pages.news', compact('news', 'type', 'partner_news'));
        }

        return abort(404);
    }

    public function adminIndex() {

        $news = News::orderBy('created_at', 'desc')->paginate(10);
        return view('pages.admin.news', compact('news'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = Category::all();
        $authors = Author::all();
        return view('pages.admin.create.news', compact('category', 'authors'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!isset($request->isActive) && $request->isActive != '1') {
            $request->request->add(['isActive' => 0]);
        }
        if(!isset($request->themeIsDay) && $request->themeIsDay != '1') {
            $request->request->add(['themeIsDay' => 0]);
        }
        if(!isset($request->isPartner) && $request->isPartner != '1') {
            $request->request->add(['isPartner' => 0]);
        }
        if(!$request->slug) {
            $request->merge(['slug' => str_slug($request->title)]);
        }

        if(!$request->published_at) {
            $request->merge(['published_at' => Carbon::now()]);
        } else {
            $request->merge(['published_at' => Carbon::parse($request->published_at)]);
        }

        $data = News::create($request->all());

        if($data) {
            return response()->json([
                'success' => 1,
                'type' => 'create'
            ]);
        }
        return response()->json([
            'error' => 1
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function show(News $news)
    {
        $date = Carbon::now()->subMonth();
        $data = News::where('created_at', '>=', $date)->where('published_at', '<=', Carbon::now())->get();
        $count = $data->count() > 3 ? 3 : $data->count();
        $outher = $data->random($count);
        return view('pages.new', compact('news', 'outher'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function edit(News $news)
    {
        $category = Category::all();
        $authors = Author::all();
        return view('pages.admin.edit.news', compact('news', 'authors', 'category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, News $news)
    {
        if(!isset($request->isActive) && $request->isActive != '1') {
            $request->request->add(['isActive' => 0]);
        }
        if(!isset($request->themeIsDay) && $request->themeIsDay != '1') {
            $request->request->add(['themeIsDay' => 0]);
        }
        if(!isset($request->isPartner) && $request->isPartner != '1') {
            $request->request->add(['isPartner' => 0]);
        }
        if(!$request->slug) {
            $request->merge(['slug' => str_slug($request->title)]);
        }
        $data = $news->update($request->all());

        if($data) {
            return response()->json([
                'success' => 1,
                'type' => 'update'
            ]);
        }
        return response()->json([
            'error' => 1
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function destroy(News $news)
    {
        $views = $news->views;
        $likes = $news->likes;
        $dislikes = $news->dislikes;

        foreach($views as $view) {
            $view->delete();
        }

        foreach($likes as $like) {
            $like->delete();
        }

        foreach($dislikes as $dislike) {
            $dislike->delete();
        }
        
        $news->delete();
        
        return response()->json([
            'success' => 1
        ]);
    }

    public function views(Request $request) {

        $uid = $request->uid;
        $news_id = $request->id;

        // return $request->all();

        $views = Views::where('uid', $uid)->where('news_id', $news_id)->first();

        if(!$views) {

            $item = new Views;
            $item->news_id = $news_id;
            $item->uid = $uid;
            $item->save();

            return 1;
        }
        return 2;
    }

    public function likes(Request $request) {

        $uid = $request->uid;
        $news_id = $request->id;

        // return $request->all();

        $likes = Likes::where('uid', $uid)->where('news_id', $news_id)->first();
        $dislikes = Dislikes::where('uid', $uid)->where('news_id', $news_id)->first();

        if($likes) {

            $likes->delete();

            return response()->json([
                'data' => 'unlike'
            ]);


        } else {

            if(!$dislikes) { 

                $item = new Likes;
                $item->news_id = $news_id;
                $item->uid = $uid;
                $item->save();
    
                return response()->json([
                    'data' => 'like'
                ]);
    
            }

        }

        return 2;

    }

    public function dislikes(Request $request) {

        $uid = $request->uid;
        $news_id = $request->id;

        $likes = Likes::where('uid', $uid)->where('news_id', $news_id)->first();
        $dislikes = Dislikes::where('uid', $uid)->where('news_id', $news_id)->first();

        if($dislikes) {

            $dislikes->delete();

            return response()->json([
                'data' => 'undislike'
            ]);


        } else {

            if(!$likes) {
 
                $item = new Dislikes;
                $item->news_id = $news_id;
                $item->uid = $uid;
                $item->save();
    
                return response()->json([
                    'data' => 'dislike'
                ]);
    
            }

        }
        return 2;

    }

    public function adminSearch(Request $request) {
        $string = $request->string;
        $news = News::where('title', 'like', '%'.$string.'%')->orderBy('created_at', 'desc')->limit(100)->get();
        return response()->json($news);
    }

    public function sitemap()
    {
        $news = News::where('isActive', 1)->get();
        return response()->view('sitemap', compact('news'))->header('Content-Type', 'text/xml');
    }
}
