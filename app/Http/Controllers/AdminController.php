<?php

namespace App\Http\Controllers;

use App\Author;
use App\News;
use App\Heading;
use App\Image;
use App\Video;
use App\Number;
use App\Review;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index() {
        $authors = Author::all()->count();
        $news = News::all()->count();
        $heading = Heading::all()->count();
        $image = Image::all()->count();
        $video = Video::all()->count();
        $number = Number::all()->count();
        $reviews = Review::orderBy('created_at', 'desc')->where('active', 0)->get();

        return view('pages.admin.dashboard', compact(
            'authors',
            'news',
            'heading',
            'image',
            'video',
            'number',
            'reviews'
        ));
    }

    public function filemanager() {
        return view('pages.admin.filemanager');
    }


}
