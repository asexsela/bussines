<?php

namespace App\Http\Controllers;

use App\HeadingNews;
use App\HeadingViews;
use App\HeadingLikes;
use App\HeadingDislikes;
use App\Category;
use App\Heading;
use Carbon\Carbon;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class HeadingNewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = HeadingNews::orderBy('created_at', 'desc')->paginate(10);
        return view('pages.admin.headingNews', compact('news'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = Category::all();
        $headings = Heading::all();
        return view('pages.admin.create.headingNews', compact('category', 'headings'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!isset($request->isActive) && $request->isActive != '1') {
            $request->request->add(['isActive' => 0]);
        }
        if(!isset($request->themeIsDay) && $request->themeIsDay != '1') {
            $request->request->add(['themeIsDay' => 0]);
        }
        if(!$request->slug) {
            $request->merge(['slug' => str_slug($request->title)]);
        }
        if(!$request->published_at) {
            $request->merge(['published_at' => Carbon::now()]);
        } else {
            $request->merge(['published_at' => Carbon::parse($request->published_at)]);
        }

        
        $data = HeadingNews::create($request->all());


        if($data) {
            return response()->json([
                'success' => 1,
                'type' => 'create'
            ]);
        }
        return response()->json([
            'error' => 1
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\HeadingNews  $headingNews
     * @return \Illuminate\Http\Response
     */
    public function show(HeadingNews $news)
    {
       //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\HeadingNews  $headingNews
     * @return \Illuminate\Http\Response
     */
    public function edit(HeadingNews $news)
    {
        $category = Category::all();
        $headings = Heading::all();
        return view('pages.admin.edit.headingNews', compact('category', 'headings', 'news'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\HeadingNews  $headingNews
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, HeadingNews $news)
    {
        if(!isset($request->isActive) && $request->isActive != '1') {
            $request->request->add(['isActive' => 0]);
        }
        if(!isset($request->themeIsDay) && $request->themeIsDay != '1') {
            $request->request->add(['themeIsDay' => 0]);
        }
        if(!$request->slug) {
            $request->merge(['slug' => str_slug($request->title)]);
        }

        $data = $news->update($request->all());


        if($data) {
            return response()->json([
                'success' => 1,
                'type' => 'update'
            ]);
        }
        return response()->json([
            'error' => 1
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\HeadingNews  $headingNews
     * @return \Illuminate\Http\Response
     */
    public function destroy(HeadingNews $news)
    {
        $views = $news->views;
        $likes = $news->likes;
        $dislikes = $news->dislikes;

        foreach($views as $view) {
            $view->delete();
        }

        foreach($likes as $like) {
            $like->delete();
        }

        foreach($dislikes as $dislike) {
            $dislike->delete();
        }

        $news->delete();
        
        return response()->json([
            'success' => 1
        ]);
    }

    public function views(Request $request) {

        $uid = $request->uid;
        $news_id = $request->id;

        // return $request->all();

        $views = HeadingViews::where('uid', $uid)->where('news_id', $news_id)->first();

        if(!$views) {

            $item = new HeadingViews;
            $item->news_id = $news_id;
            $item->uid = $uid;
            $item->save();

            return 1;
        }
        return 2;
    }

    public function likes(Request $request) {

        $uid = $request->uid;
        $news_id = $request->id;

        // return $request->all();

        $likes = HeadingLikes::where('uid', $uid)->where('news_id', $news_id)->first();
        $dislikes = HeadingDislikes::where('uid', $uid)->where('news_id', $news_id)->first();

        if($likes) {

            $likes->delete();

            return response()->json([
                'data' => 'unlike'
            ]);


        } else {

            if(!$dislikes) { 

                $item = new HeadingLikes;
                $item->news_id = $news_id;
                $item->uid = $uid;
                $item->save();
    
                return response()->json([
                    'data' => 'like'
                ]);
    
            }

        }

        return 2;

    }

    public function dislikes(Request $request) {

        $uid = $request->uid;
        $news_id = $request->id;

        $likes = HeadingLikes::where('uid', $uid)->where('news_id', $news_id)->first();
        $dislikes = HeadingDislikes::where('uid', $uid)->where('news_id', $news_id)->first();

        if($dislikes) {

            $dislikes->delete();

            return response()->json([
                'data' => 'undislike'
            ]);


        } else {

            if(!$likes) {
 
                $item = new HeadingDislikes;
                $item->news_id = $news_id;
                $item->uid = $uid;
                $item->save();
    
                return response()->json([
                    'data' => 'dislike'
                ]);
    
            }

        }
        return 2;

    }

    public function adminSearch(Request $request) {
        $string = $request->string;
        $news = HeadingNews::where('title', 'like', '%'.$string.'%')->orderBy('created_at', 'desc')->limit(100)->get();
        return response()->json($news);
    }

}
