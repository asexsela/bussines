<?php

namespace App\Http\Controllers;

use App\Category;
use App\Heading;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::orderBy('created_at', 'desc')->paginate(10);
        return view('pages.admin.category', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        $headings = Heading::all();
        return view('pages.admin.create.category', compact('categories', 'headings'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {      
    
        $validation = Validator::make($request->all(), [
            'title' => 'required'
        ]);


        if($validation->passes())  {
            if(!$request->slug) {
                $request->merge(['slug' => str_slug($request->title)]);
            }

            Category::create($request->all());

            return response()->json([
                'success' => 1,
                'type' => 'create'
            ]);

        } 

        return response()->json([
            'error' => 1,
        ]);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        $categories = Category::all();
        $headings = Heading::all();
        return view('pages.admin.edit.category', compact('category', 'categories', 'headings'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        $rules = [
            // 'file' => 'image|mimes:jpeg,png,jpg,gif',
            'title' => 'required'
        ];

        $messages = [
            'title.required' => 'Заполните это поле',
            // 'file.mimes' => 'Разрешенные форматы изображений jpeg,png,jpg,gif',
            // 'file.image' => 'Загрузите изображение',
        ];

        $validation = Validator::make($request->all(), $rules, $messages);


        if($validation->passes())  {
            if(!$request->slug) {
                $request->merge(['slug' => str_slug($request->title)]);
            }

            $category->update($request->all());

            return response()->json([
                'success' => 1
            ]);

        } 

        return response()->json([
            'error' => 1,
            'data' => $validation->errors()
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        $category->delete();

        return response()->json([
            'success' => 1
        ]);
    }

    public function adminSearch(Request $request) {
        $string = $request->string;
        $news = Category::where('title', 'like', '%'.$string.'%')->orderBy('created_at', 'desc')->limit(100)->get();
        return response()->json($news);
    }
}
