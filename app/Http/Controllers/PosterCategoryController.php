<?php

namespace App\Http\Controllers;

use App\PosterCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PosterCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = PosterCategory::orderBy('created_at', 'desc')->paginate(10);
        return view('pages.admin.poster_category', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.admin.create.poster_category');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'title' => 'required'
        ]);


        if($validation->passes())  {
            PosterCategory::create($request->all());
            return response()->json([
                'success' => 1,
                'type' => 'create'
            ]);
        } 

        return response()->json([
            'error' => 1,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PosterCategory  $posterCategory
     * @return \Illuminate\Http\Response
     */
    public function show(PosterCategory $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PosterCategory  $posterCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(PosterCategory $category)
    {
        return view('pages.admin.edit.poster_category', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PosterCategory  $posterCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PosterCategory $category)
    {
        $rules = [
            'title' => 'required'
        ];

        $messages = [
            'title.required' => 'Заполните это поле',
        ];

        $validation = Validator::make($request->all(), $rules, $messages);

        if($validation->passes())  {
            $category->update($request->all());
            return response()->json([
                'success' => 1
            ]);
        } 

        return response()->json([
            'error' => 1,
            'data' => $validation->errors()
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PosterCategory  $posterCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(PosterCategory $category)
    {
        $category->delete();

        return response()->json([
            'success' => 1
        ]);
    }

    public function adminSearch(Request $request) {
        $string = $request->string;
        $news = PosterCategory::where('title', 'like', '%'.$string.'%')->orderBy('created_at', 'desc')->limit(100)->get();
        return response()->json($news);
    }
}
