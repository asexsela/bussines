<?php

namespace App\Http\Controllers;

use App\News;
use App\BlogNews;
use App\HeadingNews;
use App\Heading;

use Jenssegers\Date\Date;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Carbon\Carbon;
use App\Http\Resources\SearchNewsResource;
use Illuminate\Support\Str;

class SearchController extends Controller
{

    public function index($search = false) {
        $type = 'search';
        $tag = false;
        if($search) {    
            $search = trim($search, ' ');

            $news = News::where('title', 'like', '%' . $search . '%')
                        ->orWhere('body', 'like', '%' . $search . '%')
                        ->where('created_at', '<=', Carbon::now())
                        ->get();

            $news_heading = HeadingNews::where('title', 'like', '%' . $search . '%')
                                        ->orWhere('body', 'like', '%' . $search . '%')
                                        ->where('created_at', '<=', Carbon::now())
                                        ->get();

            $all_news = Arr::collapse([$news, $news_heading]);
            $count = count($all_news);

            $result = [
                'news' => collect($all_news)->sortByDesc('created_at')->forPage(1, 10),
            ];

            return view('pages.search', compact('result', 'search', 'type', 'tag', 'count'));
        }
        
        return view('pages.search', compact('search', 'type', 'tag'));
        
    }

    function more_data(Request $request){
        if($request->ajax()){

            // return $request->all();
            $take = 10;
            $page = $request->page + 1;
            $search = $request->search;
            $tag = $request->tag;
            $type = $request->type;
            if($type == 'search') {

                $news = News::where('title', 'like', '%' . $search . '%')
                            ->orWhere('body', 'like', '%' . $search . '%')
                            ->where('created_at', '<=', Carbon::now())
                            ->get();
    
                $news_heading = HeadingNews::where('title', 'like', '%' . $search . '%')
                                            ->orWhere('body', 'like', '%' . $search . '%')
                                            ->where('created_at', '<=', Carbon::now())
                                            ->get();
            } else {
                $news = News::where('tags', 'like', '%'. $tag .'%')
                    ->where('created_at', '<=', Carbon::now())
                    ->get();
        
                $news_heading = HeadingNews::where('tags', 'like', '%'. $tag .'%')
                                    ->where('created_at', '<=', Carbon::now())
                                    ->get();
            }

            $all_news = Arr::collapse([$news, $news_heading]);

            $result = $this->create_html(
                            collect($all_news)->sortByDesc('created_at')->forPage($page, $take),
                            $type,
                            $tag
                        );

            return response()->json($result);
        }else{
            return response()->json('Доступа нет');
        }
    }

    public function create_html($data, $type, $tag) {
        $html = "";
        foreach ($data as $value) {
            $html .= "<div class='item item_search'>";
                $html .= "<div class='date'>" . Date::parse($value->created_at)->ago() . "</div>";
                $html .= "<div class='body'>";
                    if ($type == 'search')  {
                        if ($value->heading) {
                            $html .= "<span class='label'>" . $value->heading->name . "</span>";
                        } else {
                            $html .= "<span class='label'>Новость</span>";
                        }
                    } else {
                        $html .= "<span class='label'>#" . $tag . "</span>";
                    }
                    $html .= "<a href='" . ($value->isType() === 'heading-page/' ? url($value->isType().$value->heading->slug.'/'.$value->id) : url($value->isType().$value->id)) . "'>" . $value->title . "</a>";
                $html .= "</div>";
            $html .= "</div>";
        }

        return $html;
    }

    public function searchHash($tag) {
        $type = 'tag';
        $news = News::where('tags', 'like', '%'. $tag .'%')
                    ->where('created_at', '<=', Carbon::now())
                    ->get();
        
        $news_heading = HeadingNews::where('tags', 'like', '%'. $tag .'%')
                                    ->where('created_at', '<=', Carbon::now())
                                    ->get();
        
        $all_news = Arr::collapse([$news,$news_heading]);
        $count = count($all_news);
        $result = [
            'news' => collect($all_news)->sortByDesc('created_at')->forPage(1, 10),
        ];
        // dd($result);
        $search = '';
        return view('pages.search', compact('result', 'tag', 'search', 'type', 'count'));
    }

    public function mobSearch($data = false) {

        if(!$data) {    

            $news = News::orderBy('created_at', 'desc')
                        ->where('created_at', '<=', Carbon::now())
                        ->limit(5)
                        ->get();

            // $news_blog = BlogNews::orderBy('created_at', 'desc')->where('created_at', '<=', Carbon::now())->limit(5)->get();
            $news_heading = HeadingNews::orderBy('created_at', 'desc')
                                        ->where('created_at', '<=', Carbon::now())
                                        ->limit(5)
                                        ->get();

            $res = Arr::collapse([$news, $news_heading]);

            return response()->json([
                'success' => 1,
                'type' => false,
                'res' =>  SearchNewsResource::collection($res)
            ]);

        } else {

            $news = News::orderBy('created_at', 'desc')
                        ->where('title', 'like', '%'.$data.'%')
                        ->where('created_at', '<=', Carbon::now())
                        ->limit(5)
                        ->get();

            // $news_blog = BlogNews::orderBy('created_at', 'desc')->where('title', 'like', $data.'%')->where('created_at', '<=', Carbon::now())->limit(5)->get();
            $news_heading = HeadingNews::orderBy('created_at', 'desc')
                                        ->where('title', 'like', '%'.$data.'%')
                                        ->orWhere('body', 'like', '%' . $data . '%')
                                        ->where('created_at', '<=', Carbon::now())
                                        ->limit(5)
                                        ->get();

            $res = Arr::collapse([$news, $news_heading]);

            return response()->json([
                'success' => 1,
                'type' => true,
                'res' => SearchNewsResource::collection($res)
            ]);

        }

        return false;

    }
}
