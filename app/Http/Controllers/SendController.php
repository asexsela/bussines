<?php

namespace App\Http\Controllers;

use App\Mail\SendMail;
use App\Mail\ReviewMail;

use App\Setting;

use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;

class SendController extends Controller
{
    public function sendmail(Request $request) {
        // return $request->all();
        $mailto = Setting::find(1)->mailto;
        Mail::to($mailto)
            ->send(new SendMail($request));

        if(count(Mail::failures()) > 0){
            return 0;
        } else {
            return 1;
        }
    }

    static function reviewmail($data) {
        $mailto = Setting::find(1)->mailto;
        Mail::to($mailto)
            ->send(new ReviewMail($data));

        if(count(Mail::failures()) > 0){
            return 0;
        } else {
            return 1;
        }
    }
}
