<?php

namespace App;

use App\Number;
use Illuminate\Database\Eloquent\Model;

class Archive extends Model
{
    protected $fillable = ['title'];
    
    public function numbers() {
        return $this->hasMany(Number::class, 'archive_id', 'outher_id')->orderBy('created_at', 'asc');
    }
}
