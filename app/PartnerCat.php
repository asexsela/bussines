<?php

namespace App;

use App\Partner;

use Illuminate\Database\Eloquent\Model;

class PartnerCat extends Model
{

    protected $guarded = ['file', '_token', 'body', 'tags'];
    
    public function partners() {
        return $this->hasMany(Partner::class, 'partnercat_id', 'outher_id')->orderBy('created_at', 'desc');
    }
}
