<?php

namespace App;

use App\BlogNews;
use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    protected $guarded = ['file', '_token', 'body', 'tags'];
    
    public function news() {
        return $this->hasMany(BlogNews::class, 'author_id', 'outher_id')->orderBy('created_at', 'desc');
    }
}
