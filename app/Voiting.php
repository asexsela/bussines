<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Voiting extends Model
{
    protected $guarded = ['file', '_token', 'body', 'tags'];

    public function items() {
        return $this->hasMany(VoitingItem::class);
    }

    public function allVoiting() {
        return $this->hasMany(VoitingCheck::class);
    }

}
