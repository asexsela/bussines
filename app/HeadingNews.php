<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class HeadingNews extends Model
{
    protected $table = 'heading_news';

    protected $guarded = ['file', '_token', 'editor'];

    public function heading() {
        return $this->belongsTo(Heading::class);
    }

    public function views() {
        return $this->hasMany(HeadingViews::class, 'news_id');
    }

    public function likes() {
        return $this->hasMany(HeadingLikes::class, 'news_id');
    }

    public function dislikes() {
        return $this->hasMany(HeadingDislikes::class, 'news_id');
    }

    public function isType() {
        return 'heading-page/';
    }

    public function setPublishedAtAttribute($value)
    {
        $this->attributes['published_at'] = Carbon::parse($value);
    }
}
