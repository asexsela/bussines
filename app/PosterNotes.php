<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PosterNotes extends Model
{
    protected $guarded = ['file', '_token', 'editor'];

    public function poster() {
        return $this->belongsTo(Poster::class);
    }
}
