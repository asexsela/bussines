<?php

use Illuminate\Http\Request;
use Carbon\Carbon;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('voitings/{uid}', 'VoitingController@indexApi');
Route::post('to_vote/', 'VoitingController@toVote');
Route::post('/search-mob/{data?}', 'SearchController@mobSearch');

Route::post('/rates', function(Request $request) {
    $url = 'https://www.cbr-xml-daily.ru/daily_json.js';
    $info = file_get_contents($url);
    $info = json_decode($info, true);
    return response()->json($info);
});
