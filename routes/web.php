<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('/send-mail', 'SendController@sendmail');


Route::get('/', 'IndexController@index')->name('index');


Route::get('/sitemap.xml', 'NewsController@sitemap');

Route::post('more_search', 'SearchController@more_data')->name('more_search');


Route::get('/page/{page}', 'PageController@show')->name('page.page');

Route::get('/search/{search?}', 'SearchController@index')->name('page.search');
Route::get('/search-tag/{search?}', 'SearchController@searchHash')->name('page.search.tag');

Route::get('/news/{type?}/{date?}', 'NewsController@index')->name('page.news');
Route::get('/news-page/{news}', 'NewsController@show')->name('page.new');
Route::post('/news/views', 'NewsController@views');
Route::post('/news/likes', 'NewsController@likes');
Route::post('/news/dislikes', 'NewsController@dislikes');

Route::get('/headings', 'HeadingController@index')->name('page.headings');
Route::get('/heading/category/{slug}/{type?}/{date?}', 'HeadingController@showCategory')->name('page.heading.category');
Route::get('/heading-page/category/{slug}/{id}', 'HeadingController@showNewsCategory')->name('page.heading_page.category');
Route::get('/heading/{slug}/{type?}/{date?}', 'HeadingController@show')->name('page.heading');
Route::get('/heading-page/{slug}/{id}', 'HeadingController@showNews')->name('page.heading_page');
Route::post('/heading/views', 'HeadingNewsController@views');
Route::post('/heading/likes', 'HeadingNewsController@likes');
Route::post('/heading/dislikes', 'HeadingNewsController@dislikes');

Route::get('/photos', 'AlbumController@index')->name('page.photos');
Route::get('/photos/{album}', 'AlbumController@show')->name('page.photo');

Route::get('/videos', 'VideoController@index')->name('page.videos');
Route::get('/video/{video}', 'VideoController@show')->name('page.video');
Route::post('/video/views', 'VideoController@views');
Route::post('/video/likes', 'VideoController@likes');
Route::post('/video/dislikes', 'VideoController@dislikes');

Route::get('/voting', 'VoitingController@index')->name('page.voting');

// Route::get('/projects', 'ProjectController@index')->name('page.projects');
// Route::get('/project/{project}', 'ProjectController@show')->name('page.project');
// Route::get('/project/{project}/{project_news}', 'ProjectController@showProject')->name('page.project_page');

Route::get('/partners/{type?}', 'PartnerCatController@index')->name('page.partners');

Route::get('/posters/{type?}', 'PosterController@index')->name('page.posters');
Route::get('/poster/{poster}', 'PosterController@show')->name('page.poster.page');
Route::get('/poster/programm/{note}', 'PosterNotesController@show')->name('page.poster.programm');
Route::get('/poster/note/{note}', 'PosterProgrammsController@show')->name('page.poster.note');
// Route::post('/posters/views', 'PosterController@views');
// Route::post('/posters/likes', 'PosterController@likes');
// Route::post('/posters/dislikes', 'PosterController@dislikes');

// Route::get('/poster', function () {
//     return view('pages.poster');
// })->name('page.poster');

Route::get('/arhive-numbers/{type?}', 'ArchiveController@index')->name('page.arhives_news');

Route::get('/thanks_mails', 'LetterController@index')->name('page.thanks_mails');

Route::get('/reviews', 'ReviewController@index')->name('page.reviews');


Route::get('/authors', 'AuthorController@index')->name('page.authors');
Route::get('/author/blog/{author}', 'AuthorController@show')->name('page.author_blogs');
Route::get('/author/article/{article}', 'AuthorController@article')->name('page.article');

Route::get('/blogs', 'BlogController@index')->name('page.blogs');
Route::post('/blog/views', 'BlogController@views');
Route::post('/blog/likes', 'BlogController@likes');
Route::post('/blog/dislikes', 'BlogController@dislikes');

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
Route::post('/review/add', 'ReviewController@store')->name('review.save');
// Route::post('/review/add', 'ReviewController@store')->name('review.save');

Route::post('/banner/views', 'BannerController@views')->name('page.banner.view');
Route::post('/banner/clicks', 'BannerController@clicks')->name('page.banner.click');

Route::prefix('admin')->middleware('auth')->group(function () {

    Route::get('/', 'AdminController@index')->name('dashboard');

    Route::get('/category', 'CategoryController@index')->name('category');
    Route::get('/category/add', 'CategoryController@create')->name('category.create');
    Route::post('/category/add', 'CategoryController@store')->name('category.save');
    Route::get('/category/{category}', 'CategoryController@edit')->name('category.edit');
    Route::post('/category/edit/{category}', 'CategoryController@update')->name('category.update');
    Route::delete('/category/delete/{category}', 'CategoryController@destroy')->name('category.destroy');
    Route::post('/category/search', 'CategoryController@adminSearch')->name('category.search');

    Route::get('/heading', 'HeadingController@adminIndex')->name('heading');
    Route::get('/heading/add', 'HeadingController@create')->name('heading.create');
    Route::post('/heading/add', 'HeadingController@store')->name('heading.save');
    Route::get('/heading/{heading}', 'HeadingController@edit')->name('heading.edit');
    Route::post('/heading/edit/{heading}', 'HeadingController@update')->name('heading.update');
    Route::delete('/heading/delete/{heading}', 'HeadingController@destroy')->name('heading.destroy');
    Route::post('/heading/search', 'HeadingController@adminSearch')->name('heading.search');

    Route::get('/news', 'NewsController@adminIndex')->name('news');
    Route::get('/news/add', 'NewsController@create')->name('news.create');
    Route::post('/news/add', 'NewsController@store')->name('news.save');
    Route::get('/news/{news}', 'NewsController@edit')->name('news.edit');
    Route::post('/news/edit/{news}', 'NewsController@update')->name('news.update');
    Route::delete('/news/delete/{news}', 'NewsController@destroy')->name('news.destroy');
    Route::post('/news/uploadImage', 'NewsController@uploadImage')->name('news.upload');
    Route::post('/news/search', 'NewsController@adminSearch')->name('news.search');

    Route::get('/heading-news', 'HeadingNewsController@index')->name('heading.news');
    Route::get('/heading-news/add', 'HeadingNewsController@create')->name('heading.news.create');
    Route::post('/heading-news/add', 'HeadingNewsController@store')->name('heading.news.save');
    Route::get('/heading-news/{news}', 'HeadingNewsController@edit')->name('heading.news.edit');
    Route::post('/heading-news/edit/{news}', 'HeadingNewsController@update')->name('heading.news.update');
    Route::delete('/heading-news/delete/{news}', 'HeadingNewsController@destroy')->name('heading.news.destroy');
    Route::post('/heading-news/uploadImage', 'HeadingNewsController@uploadImage')->name('heading.news.upload');
    Route::post('/heading-news/search', 'HeadingNewsController@adminSearch')->name('heading.news.search');

    Route::get('/blog-news', 'BlogNewsController@index')->name('blog.news');
    Route::get('/blog-news/add', 'BlogNewsController@create')->name('blog.news.create');
    Route::post('/blog-news/add', 'BlogNewsController@store')->name('blog.news.save');
    Route::get('/blog-news/{news}', 'BlogNewsController@edit')->name('blog.news.edit');
    Route::post('/blog-news/edit/{news}', 'BlogNewsController@update')->name('blog.news.update');
    Route::delete('/blog-news/delete/{news}', 'BlogNewsController@destroy')->name('blog.news.destroy');
    Route::post('/blog-news/uploadImage', 'BlogNewsController@uploadImage')->name('blog.news.upload');
    Route::post('/blog-news/search', 'BlogNewsController@adminSearch')->name('blog.news.search');

    Route::get('/album', 'AlbumController@adminIndex')->name('album');
    Route::get('/album/add', 'AlbumController@create')->name('album.create');
    Route::post('/album/add', 'AlbumController@store')->name('album.save');
    Route::get('/album/{album}', 'AlbumController@edit')->name('album.edit');
    Route::post('/album/edit/{album}', 'AlbumController@update')->name('album.update');
    Route::delete('/album/delete/{album}', 'AlbumController@destroy')->name('album.destroy');
    Route::post('/album/deleteImg/{img}', 'AlbumController@deleteImg')->name('album.deleteImg');
    Route::post('/album_search', 'AlbumController@adminSearch')->name('album.search');
    
    Route::get('/video', 'VideoController@adminIndex')->name('video');
    Route::get('/video/add', 'VideoController@create')->name('video.create');
    Route::post('/video/add', 'VideoController@store')->name('video.save');
    Route::get('/video/{video}', 'VideoController@edit')->name('video.edit');
    Route::post('/video/edit/{video}', 'VideoController@update')->name('video.update');
    Route::post('/video/remove/{video}', 'VideoController@removeImg')->name('video.remove.item');
    Route::delete('/video/delete/{video}', 'VideoController@destroy')->name('video.destroy');
    Route::post('/video/uploadImage', 'VideoController@uploadImage')->name('video.upload');
    Route::post('/video_search', 'VideoController@adminSearch')->name('video.search');

    Route::get('/author', 'AuthorController@adminIndex')->name('author');
    Route::get('/author/add', 'AuthorController@create')->name('author.create');
    Route::post('/author/add', 'AuthorController@store')->name('author.save');
    Route::get('/author/{author}', 'AuthorController@edit')->name('author.edit');
    Route::post('/author/edit/{author}', 'AuthorController@update')->name('author.update');
    Route::delete('/author/delete/{author}', 'AuthorController@destroy')->name('author.destroy');
    Route::post('/author_search', 'AuthorController@adminSearch')->name('author.search');

    Route::get('/parnter-category', 'PartnerCatController@adminIndex')->name('parnter.category');
    Route::get('/parnter-category/add', 'PartnerCatController@create')->name('parnter.category.create');
    Route::post('/parnter-category/add', 'PartnerCatController@store')->name('parnter.category.save');
    Route::get('/parnter-category/{category}', 'PartnerCatController@edit')->name('parnter.category.edit');
    Route::post('/parnter-category/edit/{category}', 'PartnerCatController@update')->name('parnter.category.update');
    Route::delete('/parnter-category/delete/{category}', 'PartnerCatController@destroy')->name('parnter.category.destroy');
    Route::post('/parnter-category/search', 'PartnerCatController@adminSearch')->name('parnter.category.search');
    
    Route::get('/partner', 'PartnerController@index')->name('partner');
    Route::get('/partner/add', 'PartnerController@create')->name('partner.create');
    Route::post('/partner/add', 'PartnerController@store')->name('partner.save');
    Route::get('/partner/{partner}', 'PartnerController@edit')->name('partner.edit');
    Route::post('/partner/edit/{partner}', 'PartnerController@update')->name('partner.update');
    Route::delete('/partner/delete/{partner}', 'PartnerController@destroy')->name('partner.destroy');
    Route::post('/partner_search', 'PartnerController@adminSearch')->name('partner.search');

    Route::get('/voiting', 'VoitingController@adminIndex')->name('voiting');
    Route::get('/voiting/add', 'VoitingController@create')->name('voiting.create');
    Route::post('/voiting/add', 'VoitingController@store')->name('voiting.save');
    Route::get('/voiting/{voiting}', 'VoitingController@edit')->name('voiting.edit');
    Route::post('/voiting/edit/{voiting}', 'VoitingController@update')->name('voiting.update');
    Route::delete('/voiting/delete/{voiting}', 'VoitingController@destroy')->name('voiting.destroy');
    Route::delete('/voiting/item//delete/{item}', 'VoitingController@destroyItem')->name('voiting.item.destroy');
    Route::post('/voiting_search', 'VoitingController@adminSearch')->name('voiting.search');

    Route::get('/archive', 'ArchiveController@adminIndex')->name('archive');
    Route::get('/archive/add', 'ArchiveController@create')->name('archive.create');
    Route::post('/archive/add', 'ArchiveController@store')->name('archive.save');
    Route::get('/archive/{archive}', 'ArchiveController@edit')->name('archive.edit');
    Route::post('/archive/edit/{archive}', 'ArchiveController@update')->name('archive.update');
    Route::delete('/archive/delete/{archive}', 'ArchiveController@destroy')->name('archive.destroy');
    Route::post('/archive/search', 'ArchiveController@adminSearch')->name('archive.search');

    Route::get('/number', 'NumberController@index')->name('number');
    Route::get('/number/add', 'NumberController@create')->name('number.create');
    Route::post('/number/add', 'NumberController@store')->name('number.save');
    Route::get('/number/{number}', 'NumberController@edit')->name('number.edit');
    Route::post('/number/edit/{number}', 'NumberController@update')->name('number.update');
    Route::delete('/number/delete/{number}', 'NumberController@destroy')->name('number.destroy');
    Route::post('/number/search', 'NumberController@adminSearch')->name('number.search');

    Route::get('/review', 'ReviewController@adminIndex')->name('review');
    Route::get('/review/add', 'ReviewController@create')->name('review.create');
    Route::get('/review/{review}', 'ReviewController@edit')->name('review.edit');
    Route::post('/review/edit/{review}/{type}', 'ReviewController@update')->name('review.update');
    Route::delete('/review/delete/{review}', 'ReviewController@destroy')->name('review.destroy');
    Route::post('/review_search', 'ReviewController@adminSearch')->name('review.search');

    Route::get('/page', 'PageController@index')->name('page');
    Route::get('/page/add', 'PageController@create')->name('page.create');
    Route::post('/page/add', 'PageController@store')->name('page.save');
    Route::get('/page/{page}', 'PageController@edit')->name('page.edit');
    Route::post('/page/edit/{page}', 'PageController@update')->name('page.update');
    Route::delete('/page/delete/{page}', 'PageController@destroy')->name('page.destroy');
    Route::post('/page/search', 'PageController@adminSearch')->name('page.search.admin');

    Route::get('/banner', 'BannerController@index')->name('banner');
    Route::get('/banner/add', 'BannerController@create')->name('banner.create');
    Route::post('/banner/add', 'BannerController@store')->name('banner.save');
    Route::get('/banner/{banner}', 'BannerController@edit')->name('banner.edit');
    Route::post('/banner/edit/{banner}', 'BannerController@update')->name('banner.update');
    Route::delete('/banner/delete/{banner}', 'BannerController@destroy')->name('banner.destroy');
    Route::post('/banner_search', 'BannerController@adminSearch')->name('banner.search');

    Route::get('/poster_category', 'PosterCategoryController@index')->name('poster_category');
    Route::get('/poster_category/add', 'PosterCategoryController@create')->name('poster_category.create');
    Route::post('/poster_category/add', 'PosterCategoryController@store')->name('poster_category.save');
    Route::get('/poster_category/{category}', 'PosterCategoryController@edit')->name('poster_category.edit');
    Route::post('/poster_category/edit/{category}', 'PosterCategoryController@update')->name('poster_category.update');
    Route::delete('/poster_category/delete/{category}', 'PosterCategoryController@destroy')->name('poster_category.destroy');
    Route::post('/poster_category_search', 'PosterCategoryController@adminSearch')->name('poster_category.search');

    Route::get('/posters', 'PosterController@adminIndex')->name('posters');
    Route::get('/posters/add', 'PosterController@create')->name('posters.create');
    Route::post('/posters/add', 'PosterController@store')->name('posters.save');
    Route::get('/posters/{poster}', 'PosterController@edit')->name('posters.edit');
    Route::post('/posters/edit/{poster}', 'PosterController@update')->name('posters.update');
    Route::delete('/posters/delete/{poster}', 'PosterController@destroy')->name('posters.destroy');
    Route::post('/posters_search', 'PosterController@adminSearch')->name('posters.search');

    // Route::get('/programms', 'PosterNotesController@index')->name('notes');
    // Route::get('/programms/add', 'PosterNotesController@create')->name('notes.create');
    // Route::post('/programms/add', 'PosterNotesController@store')->name('notes.save');
    // Route::get('/programms/{note}', 'PosterNotesController@edit')->name('notes.edit');
    // Route::post('/programms/edit/{note}', 'PosterNotesController@update')->name('notes.update');
    // Route::delete('/programms/delete/{note}', 'PosterNotesController@destroy')->name('notes.destroy');

    Route::get('/notes', 'PosterProgrammsController@index')->name('programms');
    Route::get('/notes/add', 'PosterProgrammsController@create')->name('programms.create');
    Route::post('/notes/add', 'PosterProgrammsController@store')->name('programms.save');
    Route::get('/notes/{note}', 'PosterProgrammsController@edit')->name('programms.edit');
    Route::post('/notes/edit/{note}', 'PosterProgrammsController@update')->name('programms.update');
    Route::delete('/notes/delete/{note}', 'PosterProgrammsController@destroy')->name('programms.destroy');
    Route::post('/notes_search', 'PosterProgrammsController@adminSearch')->name('programms.search');

    Route::get('/setting/menu', 'MenuController@index')->name('menu');
    Route::get('/setting/menu/add', 'MenuController@create')->name('menu.create');
    Route::post('/setting/menu/add', 'MenuController@store')->name('menu.save');
    Route::get('/setting/menu/{menu}', 'MenuController@edit')->name('menu.edit');
    Route::post('/setting/menu/edit/{menu}', 'MenuController@update')->name('menu.update');
    Route::delete('/setting/menu/delete/{menu}', 'MenuController@destroy')->name('menu.destroy');
    Route::delete('/setting/menu/item/delete/{item}', 'MenuItemController@destroy')->name('menu.item.destroy');


    Route::get('/setting/filemanager', 'AdminController@filemanager')->name('filemanager');
    Route::get('/setting/base', 'SettingController@index')->name('base');
    Route::post('/setting/save', 'SettingController@update')->name('base.save');


    Route::get('/thanks_mails', 'LetterController@adminIndex')->name('thanks_mails');
    Route::post('/thanks_mails/add', 'LetterController@store')->name('thanks_mails.save');
    Route::post('/thanks_mails/delete/{letter}', 'LetterController@destroy')->name('thanks_mails.destroy'); 


});