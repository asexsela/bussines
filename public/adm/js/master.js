$(function() {

    setTimeout(function() {
        $('#admin-login').addClass('show');
    }, 2000)


    $('.sidenav').sidenav();
    $('.collapsible').collapsible();
    $('.fixed-action-btn').floatingActionButton();
    $('select').formSelect();
    $('.tabs').tabs();
    // $('.materialboxed').materialbox();

    $('.materialboxed').on('load', function(){
        $('.materialboxed').materialbox();
    });
    // $('.chips').chips({
    //     placeholder: 'Теги',
    //     secondaryPlaceholder: '+Тег',
    //     onChipAdd: function(e, chip){
    //         $("#chips_inputcontainer").append("<input type='hidden' name='chip[new][]' value='" + chip.innerHTML.substr(0, chip.innerHTML.indexOf("<i")) +  "'>");
    //     },
    // });
    $('.datepicker').datepicker({
        firstDay: 1,
        format: 'dd.mm.yyyy',
        defaultDate: Date.now(),
        i18n: {
            cancel: 'Отмена',
            clear: 'Очистить',
            done: 'Сохранить',
            months: [
                'Январь',
                'Февраль',
                'Март',
                'Апрель',
                'Май',
                'Июнь',
                'Июль',
                'Август',
                'Сентябь',
                'Октябрь',
                'Ноябрь',
                'Декабрь'
            ],
            monthsShort: [
                'Янв',
                'Фев',
                'Мар',
                'Апр',
                'Май',
                'Июн',
                'Июл',
                'Авг',
                'Сен',
                'Окт',
                'Ноб',
                'Дек'
            ],
            weekdays: [
                'Воскресенье',
                'Понедельник',
                'Вторник',
                'Среда',
                'Четверг',
                'Пятница',
                'Суббота'
            ],
            weekdaysShort: [
                'Вск',
                'Пн',
                'Вт',
                'Ср',
                'Чт',
                'Пт',
                'Сб'
            ],
            weekdaysAbbrev: ['В','П','В','С','Ч','П','С']
        }
    });
    $('.tooltipped').tooltip();
    $('.modal').modal();

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
    
            reader.onload = function (e) {
                $('#image').attr('src', e.target.result);
            };
    
            reader.readAsDataURL(input.files[0]);

            $('#image').css('display', 'block');

        }
    }

    function readURLFile(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
    
            reader.onload = function (e) {
                $('#image-file').attr('src', e.target.result);
            };
    
            reader.readAsDataURL(input.files[0]);

            $('#image-file').css('display', 'block');

        }
    }

    function readURLs(input) {

        var files = input.files;
        for (var i = 0, f; f = files[i]; i++) {
            if (!f.type.match('image.*')) {
                let audio = 'http://www.orangefreesounds.com/wp-content/uploads/2015/08/Error.mp3',
                    play = new Audio(audio);
                    play.volume = 0.2;
                    play.play();
                M.toast({html: 'Только изображения!', classes: 'error'});

                return;
            }
            var reader = new FileReader();

            reader.onload = (function (theFile) {
                return function (e, index) {
                    $('#gallery').append('<div class="img"><img src="' + e.target.result + '"></div>');
                };
            })(f);

            reader.readAsDataURL(f);
        }

    }
    
    $("#field").change(function(){
        readURL(this);
        
    });

    $("#file").change(function(){
        readURLFile(this);
    });

    $("#upload-images").change(function(){
        readURLs(this);
    });

    let src = $('.materialboxed');

    if(src.attr('src') === '') {
        src.css('display', 'none');
    }

    SmoothScroll({ stepSize: 20 });

})


if(document.querySelector('#editor1')) {    
    ClassicEditor
        .create(document.querySelector('#editor1'), {
            language: 'ru',
            
        })
}

// VK.Widgets.Group("vk_groups", {
//     mode: 4, 
//     wide: 1, 
//     no_cover: 1, 
//     height: "400",
//     color1: '#08554b',
//     color2: 'FFFFFF', 
//     color3: 'C0C0C0'
// }, 143365133);

 
$(".switch").find("input[type=checkbox]").on("change",function() {
    var status = $(this).prop('checked');
    
    if(status) {
        $(this).attr('value', '1');
    } else {
        $(this).attr('value', '0');
    }
});

$(function() {

    $('#save-button').click(function(e) {
        e.preventDefault();
        let data = new FormData($('#form-save')[0]),
            url = $('#form-save').attr('action'),
            chips = [],
            body = $('#editor').html();

            $('.chips').find('.chip').each(function() {
                chips.push($(this).text().replace('close', ""));
            })

        data.append('tags', chips);

        $.ajax({
            type: 'post',
            url: url,
            data: data,
            dataType:'JSON',
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function() {
                $('.progress').show();
            },
            success: function(data) {
                console.log(data)
                $('.progress').hide();
                if(data.success) {
                    let audio = 'https://notificationsounds.com/soundfiles/d86ea612dec96096c5e0fcc8dd42ab6d/file-sounds-1144-me-too.mp3',
                        play = new Audio(audio);
                        play.volume = 0.2;
                        play.play();
                    M.toast({html: 'Успешно!', classes: 'success'});
                    if(data.type === 'create') {
                        setTimeout(() => {
                            location.href = $('#back_parent').attr('href');
                        }, 300);
                    } else {
                        setTimeout(() => {
                            location.reload()
                        }, 300);
                    }
                }
                if(data.error === 1) {
                    let audio = 'http://www.orangefreesounds.com/wp-content/uploads/2015/08/Error.mp3',
                        play = new Audio(audio);
                        play.volume = 0.2;
                        play.play();
                    M.toast({html: 'Заполните все поля со звездочкой!', classes: 'error'});
                } else if(data.error === 2) {
                    let audio = 'http://www.orangefreesounds.com/wp-content/uploads/2015/08/Error.mp3',
                        play = new Audio(audio);
                        play.volume = 0.2;
                        play.play();
                    M.toast({html: 'Только картинки', classes: 'error'});
                }
            },
            error: function(err) {
                let audio = 'http://www.orangefreesounds.com/wp-content/uploads/2015/08/Error.mp3',
                    play = new Audio(audio);
                    play.volume = 0.2;
                    play.play();
                $('.progress').hide();
                M.toast({html: 'Ошибка сервера!', classes: 'error'});
            }
        })
    
    })

    $('#save-button-setting').click(function(e) {
        e.preventDefault();
        let data = new FormData($('#form-save-setting')[0]),
            url = $('#form-save-setting').attr('action');
            data.append('header_metrick', $('#header-metrick').text())
            data.append('footer_metrick', $('#footer-metrick').text())
        // console.log( $('#header-metrick').html().text())
        $.ajax({
            type: 'post',
            url: url,
            data: data,
            dataType:'JSON',
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function() {
                $('.progress').show();
            },
            success: function(data) {
                console.log(data)
                $('.progress').hide();
                if(data.success) {
                    let audio = 'https://notificationsounds.com/soundfiles/d86ea612dec96096c5e0fcc8dd42ab6d/file-sounds-1144-me-too.mp3',
                        play = new Audio(audio);
                        play.volume = 0.2;
                        play.play();
                    M.toast({html: 'Успешно!', classes: 'success'});
                    if(data.type === 'create') {
                        setTimeout(() => {
                            location.href = $('#back_parent').attr('href');
                        }, 300);
                    } else {
                        setTimeout(() => {
                            location.reload()
                        }, 300);
                    }
                }
                if(data.error === 1) {
                    let audio = 'http://www.orangefreesounds.com/wp-content/uploads/2015/08/Error.mp3',
                        play = new Audio(audio);
                        play.volume = 0.2;
                        play.play();
                    M.toast({html: 'Заполните все поля со звездочкой!', classes: 'error'});
                } else if(data.error === 2) {
                    let audio = 'http://www.orangefreesounds.com/wp-content/uploads/2015/08/Error.mp3',
                        play = new Audio(audio);
                        play.volume = 0.2;
                        play.play();
                    M.toast({html: 'Только картинки', classes: 'error'});
                }
            },
            error: function(err) {
                let audio = 'http://www.orangefreesounds.com/wp-content/uploads/2015/08/Error.mp3',
                    play = new Audio(audio);
                    play.volume = 0.2;
                    play.play();
                $('.progress').hide();
                M.toast({html: 'Ошибка сервера!', classes: 'error'});
            }
        })
    
    })

    $('#save-button-menu').click(function(e) {
        e.preventDefault();
        let data = new FormData($('#form-save-menu')[0]),
            url = $('#form-save-menu').attr('action'),
            items = [];

            $('.item-menu').each(function(i, e) {
                items.push({
                    id: $(this).attr('data-id'),
                    position: i+1,
                    name: $(this).find('.item-name').val(),
                    url: $(this).find('.item-url').val(),
                    img: $(this).find('.item-img').val(),
                });
              
            });

        // data.append('body', body);
        data.append('items', JSON.stringify(items));

        $.ajax({
            type: 'post',
            url: url,
            data: data,
            dataType:'JSON',
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function() {
                $('.progress').show();
            },
            success: function(data) {
                console.log(data)
                $('.progress').hide();
                if(data.success) {
                    let audio = 'https://notificationsounds.com/soundfiles/d86ea612dec96096c5e0fcc8dd42ab6d/file-sounds-1144-me-too.mp3',
                        play = new Audio(audio);
                        play.volume = 0.2;
                        play.play();
                    M.toast({html: 'Успешно!', classes: 'success'});
                    if(data.type === 'create') {
                        setTimeout(() => {
                            location.href = $('#back_parent').attr('href');
                        }, 300);
                    } else {
                        setTimeout(() => {
                            location.reload()
                        }, 300);
                    }
                }
                if(data.error === 1) {
                    let audio = 'http://www.orangefreesounds.com/wp-content/uploads/2015/08/Error.mp3',
                        play = new Audio(audio);
                        play.volume = 0.2;
                        play.play();
                    M.toast({html: 'Заполните все поля со звездочкой!', classes: 'error'});
                } else if(data.error === 2) {
                    let audio = 'http://www.orangefreesounds.com/wp-content/uploads/2015/08/Error.mp3',
                        play = new Audio(audio);
                        play.volume = 0.2;
                        play.play();
                    M.toast({html: 'Только картинки', classes: 'error'});
                }
            },
            error: function(err) {
                let audio = 'http://www.orangefreesounds.com/wp-content/uploads/2015/08/Error.mp3',
                    play = new Audio(audio);
                    play.volume = 0.2;
                    play.play();
                $('.progress').hide();
                M.toast({html: 'Ошибка сервера!', classes: 'error'});
            }
        })
    
    })

    $('.delete').click(function(e) {
        e.preventDefault();
        let url = $(this).attr('href'),
            token = $('[name="csrf-token"]').attr('content');
        $.ajax({
            type: 'delete',
            url: url,
            headers: {'X-CSRF-TOKEN': token},
            dataType:'JSON',
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function() {
                $('.progress').show();
            },
            success: function(data) {
                $('.progress').hide();
                // console.log(data)
                if(data.success) {
                    let audio = 'https://notificationsounds.com/soundfiles/d86ea612dec96096c5e0fcc8dd42ab6d/file-sounds-1144-me-too.mp3',
                        play = new Audio(audio);
                        play.volume = 0.2;
                        play.play();
                    M.toast({html: 'Успешно!', classes: 'success'});
                    setTimeout(() => {
                        location.reload()
                    }, 1000);
                }
            },
            error: function(err) {
                let audio = 'http://www.orangefreesounds.com/wp-content/uploads/2015/08/Error.mp3',
                    play = new Audio(audio);
                    play.volume = 0.2;
                    play.play();
                $('.progress').hide();
                
                M.toast({html: 'Ошибка сервера!', classes: 'error'});
            }
        })
    
    })

    $('.remove-item').click(function(e) {
        e.preventDefault();
        let url = $(this).data('url'),
            token = $('[name="csrf-token"]').attr('content');
        $.ajax({
            type: 'delete',
            url: url,
            headers: {'X-CSRF-TOKEN': token},
            dataType:'JSON',
            contentType: false,
            cache: false,
            processData: false,
            success: function(data) {
                console.log(data)
                if(data.success) {
                    let audio = 'https://notificationsounds.com/soundfiles/d86ea612dec96096c5e0fcc8dd42ab6d/file-sounds-1144-me-too.mp3',
                        play = new Audio(audio);
                        play.volume = 0.2;
                        play.play();
                    M.toast({html: 'Успешно!', classes: 'success'});
                }
            },
            error: function(err) {
                let audio = 'http://www.orangefreesounds.com/wp-content/uploads/2015/08/Error.mp3',
                    play = new Audio(audio);
                    play.volume = 0.2;
                    play.play();
                $('.progress').hide();
                
                M.toast({html: 'Ошибка сервера!', classes: 'error'});
            }
        })
    
    })

    $('.remove').click(function(e) {
        e.preventDefault();
       
        let url = $(this).data('url'),
            _this = $(this),
            token = $('[name="csrf-token"]').attr('content');
        $.ajax({
            type: 'post',
            url: url,
            headers: {'X-CSRF-TOKEN': token},
            dataType:'JSON',
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function() {
                _this.siblings('.loader').show().css('display', 'flex');
            },
            success: function(data) {
                console.log(data)
                if(data.success) {
                    let audio = 'https://notificationsounds.com/soundfiles/d86ea612dec96096c5e0fcc8dd42ab6d/file-sounds-1144-me-too.mp3',
                    play = new Audio(audio);
                    play.volume = 0.2;
                    play.play();
                    M.toast({html: 'Успешно!', classes: 'success'});
                    setTimeout(() => {
                        _this.siblings('.loader').hide();
                        location.reload()
                    }, 300);
                } else {
                    let audio = 'http://www.orangefreesounds.com/wp-content/uploads/2015/08/Error.mp3',
                        play = new Audio(audio);
                        play.volume = 0.2;
                        play.play();
                    _this.siblings('.loader').hide();
                    M.toast({html: 'Ошибка удаления!', classes: 'error'});
                }
            },
            error: function(err) {
                let audio = 'http://www.orangefreesounds.com/wp-content/uploads/2015/08/Error.mp3',
                    play = new Audio(audio);
                    play.volume = 0.2;
                    play.play();
                    _this.siblings('.loader').hide();
                
                M.toast({html: 'Ошибка сервера!', classes: 'error'});
            }
        })
    
    })


    $('body').on('click', '.remove-item', function(e) {
        e.preventDefault();
        let count = $('[name="items[]"]').length;
        if(count > 1) {
            $(this).parent().remove()

            let url = $(this).data('url'),
                token = $('[name="csrf-token"]').attr('content');
            $.ajax({
                type: 'delete',
                url: url,
                headers: {'X-CSRF-TOKEN': token},
                dataType:'JSON',
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function() {
                    $('.progress').show();
                },
                success: function(data) {
                    $('.progress').hide();
                    // console.log(data)
                    if(data.success) {
                        let audio = 'https://notificationsounds.com/soundfiles/d86ea612dec96096c5e0fcc8dd42ab6d/file-sounds-1144-me-too.mp3',
                            play = new Audio(audio);
                            play.volume = 0.2;
                            play.play();
                        M.toast({html: 'Успешно!', classes: 'success'});
                        setTimeout(() => {
                            location.reload()
                        }, 1000);
                    }
                },
                error: function(err) {
                    let audio = 'http://www.orangefreesounds.com/wp-content/uploads/2015/08/Error.mp3',
                        play = new Audio(audio);
                        play.volume = 0.2;
                        play.play();
                    $('.progress').hide();
                    
                    M.toast({html: 'Ошибка сервера!', classes: 'error'});
                }
            })
        }



    })

    $('.update').click(function(e) {

        e.preventDefault();

        let token = $('[name="csrf-token"]').attr('content'),
            url = $(this).data('href');

        $.ajax({
            type: 'post',
            url: url,
            headers: {'X-CSRF-TOKEN': token},
            success: function(data) {
                if(data.success) {
                    let audio = 'https://notificationsounds.com/soundfiles/d86ea612dec96096c5e0fcc8dd42ab6d/file-sounds-1144-me-too.mp3',
                        play = new Audio(audio);
                        play.volume = 0.2;
                        play.play();
                    M.toast({html: 'Успешно!', classes: 'success'});
                    setTimeout(() => {
                        location.reload()
                    }, 1000);
                }

            },
            error: function(err) {
                let audio = 'http://www.orangefreesounds.com/wp-content/uploads/2015/08/Error.mp3',
                        play = new Audio(audio);
                        play.volume = 0.2;
                        play.play();
                    $('.progress').hide();
                    
                    M.toast({html: 'Ошибка сервера!', classes: 'error'});
            }
        })
    })


    $('.add-item').click(function() {
        $('#voiting_items .content').parent().append(`
            <div class="input-field">
                <input type="text" name="items[]" value="" placeholder="Заголовок">
                <span class="remove-item"><i class="material-icons">delete</i></span>
            </div>
        `)
        
    })

    $('.add-item-menu').click(function() {
        let count = $('#sortable').find('.item-menu').length + 1;
        $('#sortable').append(`
            <div id="${count}" data-id="" class="item-menu">
                <div class="title">
                    <div class="img">
                            <a href="/filemanager/dialog.php?type=2&field_id=img${count}&relative_url=1" class="change iframe-btn">+</a>
                            <input type="hidden" class="item-img" id="img${count}" value="">
                            <img src="/images/photos.png" alt="">
                        </div>
                    <p>Новая ссылка</p>
                    <span class="arrow">&#8250;</span>
                    <span class="remove-item">&times;</span>
                </div>
                <div class="content">
                    <div class="row">
                        <div class="col s12 l6">
                            <div class="input-field">
                                <input class="item-name" type="text" required>
                                <label>Название*</label>
                            </div>
                        </div>
                        <div class="col s12 l6">
                            <div class="input-field">
                                <input class="item-url" type="text" required>
                                <label>URL*</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        `)
        
        
    })

    $( "#sortable" ).sortable({
        revert: 100,
        placeholder: "item-menu-placeholder",
        update:function(event, ui) {
            // event.preventDefault();
            // $('.item-menu-placeholder').css('height', $(this).height())
            console.log($(this).sortable( "toArray" ))
            // $.ajax({
            //    type:'POST',
            //    url:'{{ route('productImageSort') }}',
            //    data: $(this).sortable('serialize'),
            //    success:function(data){
            //       alert(data.success);
            //    },
            //     error : function() {
            //       alert('error...');
            //     }
            // });
        }
    });
    
    $( "#sortable" ).disableSelection();


    $('body').on('click', '.arrow', function() {
        let el = $(this).parents('.item-menu');
            $(el).toggleClass('open')
    })

    $('body').on('click', '.remove-item', function() {
        $(this).parents('.item-menu').remove();
    })


})


if(document.querySelector('#editor')) {

    tinymce.init({
        selector: '#editor',
        plugins: 'print preview paste importcss searchreplace autolink autosave save directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern noneditable help charmap quickbars emoticons',
        imagetools_cors_hosts: ['picsum.photos'],
        menubar: 'edit view insert format tools table',
        toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist | forecolor backcolor removeformat | pagebreak | charmap emoticons | fullscreen  preview print | insertfile image media template link anchor codesample | ltr rtl',
        toolbar_sticky: true,
        image_advtab: true,
        image_caption: true,
        quickbars_selection_toolbar: 'bold italic | quicklink h2 h3 blockquote quickimage quicktable',
        noneditable_noneditable_class: "mceNonEditable",
        toolbar_drawer: 'sliding',
        contextmenu: "link image imagetools table",
        external_filemanager_path:"/filemanager/",
        filemanager_title:"Файловый менеджер" ,
        external_plugins: { "filemanager" : "/filemanager/plugin.min.js"},
        language: 'ru',
        height: 500,
        setup: function (editor) {
            editor.on('change', function () {
                editor.save();
            });
        }
    });
    
}
function responsive_filemanager_callback(field_id){

    if(field_id !== 'thanks_mails'){
        var url= '/images/' + $('#' + field_id).val();
        $('#' + field_id).val(url).trigger('change');
        if(field_id == 'field') {
            $('#image').attr('src', url);
        } else if(field_id == 'field2') {
            $('#image-file').attr('src', url);
        }
        $.fancybox.close();
    } else {
        var url= $('#' + field_id).val();
        $('#' + field_id).val(url).trigger('change');
        $.fancybox.close();
    }
}
$(function() {
    $('.iframe-btn').fancybox({
        'type'	: 'iframe',
        'autoScale'   : false
    });
})

let img = $('.img'),
    width = $(document).innerWidth();

if(width >= 500) {
    img.each(function(i) {
        if($(img[i]).data('style')) {
            $(img[i]).find('img').css($(img[i]).data('style'))
        }
    })
}

$(window).on('load', function() {
    $('.loader').fadeOut();
})


$(function() {

    

})