@extends('layouts.base')

@section('seo')
    <title>Порект | Вологодские новости</title>
@endsection

@section('content')

    <section class="headline">
        <div class="container bc">
            <div class="breadcrumbs">
                <a href="#">Главная</a>
                <a href="{{ route('page.projects') }}">Проекты</a>
                <span>{{ $project->title }}</span>
            </div>
            <h1></h1>
        </div>
    </section>
    
    <section class="project pt-4">
        <div class="container">
            <div class="row">
                
                <div class="col-9">
                    <div class="search">
                        <p><img src="{{ asset('images/clock.svg') }}" alt="">Период с </p> 
                        <input type="text">
                        <p> по </p>
                        <input type="text">
                        <a href="#">Найти</a>
                    </div>
                    
                    <div class="box_project">

                        @foreach ($project->news as $item)
                            
                            <div class="item">
                                <p>{{ Date::parse($item->created_at)->format('j F Y | H:i ') }}</p>
                                <div>
                                    @if ($item->img)
                                        <img src="{{ asset($item->img) }}" alt="">
                                    @else
                                        <img src="{{ asset('/images/photos.png') }}" alt="">
                                    @endif
                                    <div class="body">
                                        <h3>{{ $item->title }}</h3>
                                        <p>{{ $item->label }}</p>
                                        <div class="desc">
                                            <p>{{ $item->description }}</p>
                                        </div>
                                        <div class="footer_news">
                                            <div class="control">
                                                <a href="#">
                                                    <img src="{{ asset('images/eye.svg') }}" alt="">
                                                    <span>{{ $item->views }}</span>
                                                </a>
                                                <a href="#">
                                                    <img src="{{ asset('images/like.svg') }}" alt="">
                                                    <span>{{ $item->likes }}</span>
                                                </a>
                                                <a href="#">
                                                    <img src="{{ asset('images/dislike.svg') }}" alt="">
                                                    <span>{{ $item->dislikes }}</span>
                                                </a>
                                            </div>
                                            <a href="{{ route('page.project_page', ['project' => $project->id, 'project_news' => $item->id]) }}" class="more_news">Подробнее</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        @endforeach

                    </div>
                    

                </div>
                <div class="col-3">
                    @if ($project->news->count() > 0)    
                        <div class="box-ad">
                            <img src="{{ asset('images/banners_min.jpg') }}" alt="">
                            <img src="{{ asset('images/banners_min.jpg') }}" alt="">
                            <img src="{{ asset('images/banners_min.jpg') }}" alt="">
                            <img src="{{ asset('images/banners_min.jpg') }}" alt="">
                            <img src="{{ asset('images/banners_min.jpg') }}" alt="">
                        </div>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    @if ($project->news->count() <= 0)
                        <p class="empty">Проектов не найдено</p>
                    @endif
                </div>
            </div>
        </div>
    </section>

    <section class="pagination">
        <div class="container">
            {{-- pagination --}}
        </div>
    </section>
    <div class="space"></div>

@endsection