@extends('layouts.base')

@section('seo')
    <title>{{ $album->title }} | Вологодские новости</title>
    {{-- <title>{{ json_decode($setting->seo)->home->title }}</title>
    <meta name="description" content="{{ json_decode($setting->seo)->home->description }}" />
    <meta name="keywords" content="{{ json_decode($setting->seo)->home->keywords }}">
    <meta property="og:title" content="{{ json_decode($setting->seo)->home->title }}" />
    <meta property="og:description" content="{{ json_decode($setting->seo)->home->description }}" /> --}}
@endsection

@section('content')
<div class="space"></div>
    <section class="headline">
        <div class="container bc">
            <div class="breadcrumbs">
                <a href="/">Главная</a>
                <a href="{{ route('page.photos') }}">Фото</a>
                <span>{{ $album->title }}</span>
            </div>
            <div class="toback">
                <a href="{{ route('page.photos') }}">< Вернуться</a>
            </div>
        </div>
    </section>
    <section class="headline">
        <div class="container">
            <h1 style="padding-top: 20px;">{{ $album->title }}</h1>
        </div>
    </section>

    <section class="photo">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-10 offset-lg-1">
                    <div class="box_gallery">
                        @foreach ($images as $img)
                            <a data-fancybox="gallery" href="{{ asset($img->src) }}">
                                <img src="{{ asset($img->src) }}" alt="">
                            </a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="space"></div>

    <section class="pagination">
        <div class="container">
            {{ $images->links() }}
        </div>
    </section>

    <div class="space"></div>

@endsection