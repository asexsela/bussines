@extends('layouts.base')

@section('seo')
    <title>Блоги | Вологодские новости</title>

@endsection

@section('content')
    <section class="sec1">
        <div class="container">
            @php
                $banner = $banners->whereNotNull('img_big')->random(1)[0];
            @endphp
            <a href="{{ $banner->url }}" target="blank" banner-big data-id="{{ $banner->id }}">
                <img src="{{ asset($banner->img_big) }}" alt="">
            </a>
        </div>
    </section>
    <section class="headline">
        <div class="container">
            <h1>Блоги</h1>
        </div>
    </section>
    
    <section class="blogs">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-9">
                    <div class="row">
                        <div class="col-4 d-none d-lg-block">
                            <div class="box_authors">
                                <div class="head">
                                    <p><img src="{{ asset('images/authors.svg') }}" alt="">Авторы</p>
                                </div>
                                <div class="authors_list">
                                    @foreach ($authors as $item)    
                                        <div class="author">
                                            <a href="{{ route('page.author_blogs', ['author' => $item->id]) }}">
                                                <img src="{{ asset($item->img) }}" alt="">
                                                <span>{{ $item->title }}</span>
                                            </a>
                                        </div>
                                    @endforeach
                                </div>
                                <a href="{{ route('page.authors') }}" class="more_news">Посмотреть всех авторов</a>
                            </div>
                        </div>
                        <div class="col-12 col-lg-8">
                            <div class="box_blogs">
                                @foreach ($news as $key => $item)

                                    @if ($key >= 2)
                                        @continue
                                    @endif
                                    {{-- {{ dd($item) }} --}}
                                    <div class="news">
                                        <a href="{{ route('page.article', ['article' => $item->id]) }}">
                                            @if ($item->img)
                                            <div class="img big" data-bigstyle="{{ $item->big_style }}"  data-smallstyle="{{ $item->small_style }}">
                                                    <span class="label">{{ $item->author->title }}</span>
                                                    <img class="news_image" src="{{ asset($item->img) }}" alt="">
                                                </div>
                                            @else
                                                <div class="img">
                                                    <span class="label">{{ $item->author->title }}</span>
                                                    <img class="news_image" src="{{ asset('/images/photos.png') }}" alt="">
                                                </div>
                                            @endif
                                           
                                            <h3 class="news-title">{{ $item->title }}</h3>
                                            <p class="news-desc">{{ $item->description }}</p>
                                        </a>
                                        <div class="footer_news">
                                            <span class="data">{{ Date::parse($item->created_at)->format('j F Y, H:i ') }}</span>
                                            <div class="control">
                                                <a href="#">
                                                    <img src="{{ asset('images/eye.svg') }}" alt="">
                                                    <span>{{ $item->views->count() }}</span>
                                                </a>
                                                <a href="#!" class="like" data-id="{{ $item->id }}" data-type="blog">
                                                    <img src="{{ asset('images/like.svg') }}" alt="">
                                                    <span>{{ $item->likes->count() }}</span>
                                                </a>
                                                <a href="#" class="dislike" data-id="{{ $item->id }}" data-type="blog">
                                                    <img src="{{ asset('images/dislike.svg') }}" alt="">
                                                    <span>{{ $item->dislikes->count() }}</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    
                                @endforeach

                            </div>
                        </div>
                    </div>
                </div>
                <section class="sec4 mob">
                    <div class="container">
                        @php
                            $banner = $banners->whereNotNull('img_small')->random(1)[0];
                        @endphp
                        <a href="{{ $banner->url }}" target="blank" banner-small data-id="{{ $banner->id }}">
                            <img src="{{ asset($banner->img_small) }}" alt="">
                        </a>
                    </div>
                </section>
                <div class="col-3 d-none d-lg-block">
                    <div class="box-ad">
                        @php
                            $count = ($count_banner < 3) ? $count_banner : 3;
                        @endphp
                        @foreach ($banners->whereNotNull('img_small')->random($count) as $item)
                            <a href="{{ $item->url }}" target="blank" banner-small data-id="{{ $item->id }}">
                                <img src="{{ asset($item->img_small) }}" alt="">
                            </a>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="author_mob">
                <div class="box_authors">
                    <div class="head">
                        <p>Авторы</p>
                    </div>
                    <div class="authors_list">
                        @foreach ($authors as $item)    
                            <div class="author">
                                <a href="{{ route('page.author_blogs', ['author' => $item->id]) }}">
                                    <img src="{{ asset($item->img) }}" alt="">
                                    <span>{{ $item->title }}</span>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="spaser"></div>
            <div class="row outher_blogs">

                @foreach ($news as $key => $item)
                    @if ($key === 3 || $key === 5 || $key === 7 || $key === 8)    
                        <div class="col-12 col-md-4 col-lg-3 mb-4">
                            <div class="news">
                                <a href="{{ route('page.article', ['article' => $item->id]) }}">
                                    @if ($item->img)
                                        <div class="img small" data-bigstyle="{{ $item->big_style }}"  data-smallstyle="{{ $item->small_style }}">
                                            <span class="label">{{ $item->author->title }}</span>
                                            <img class="news_image" src="{{ asset($item->img) }}" alt="">
                                        </div>
                                    @else
                                        <div class="img">
                                            <span class="label">{{ $item->author->title }}</span>
                                            <img class="news_image" src="{{ asset('/images/photos.png') }}" alt="">
                                        </div>
                                    @endif
                                    <h3 class="news-title">{{ $item->title }}</h3>
                                    <p class="news-desc">{{ $item->description }}</p>
                                </a>
                                <div class="footer_news">
                                    <span class="data">{{ Date::parse($item->created_at)->format('j F Y, H:i ') }}</span>
                                    <div class="control">
                                        <a href="#">
                                            <img src="{{ asset('images/eye.svg') }}" alt="">
                                            <span>{{ $item->views->count() }}</span>
                                        </a>
                                        <a href="#!" class="like" data-id="{{ $item->id }}" data-type="blog">
                                            <img src="{{ asset('images/like.svg') }}" alt="">
                                            <span>{{ $item->likes->count() }}</span>
                                        </a>
                                        <a href="#" class="dislike" data-id="{{ $item->id }}" data-type="blog">
                                            <img src="{{ asset('images/dislike.svg') }}" alt="">
                                            <span>{{ $item->dislikes->count() }}</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @elseif($key === 4 || $key === 6)
                        <div class="col-12 col-md-4 col-lg-6 mb-4">
                            <div class="news">
                                <a href="{{ route('page.article', ['article' => $item->id]) }}">
                                    @if ($item->img)
                                        <div  class="img big" data-bigstyle="{{ $item->big_style }}"  data-smallstyle="{{ $item->small_style }}">
                                            <span class="label">{{ $item->author->title }}</span>
                                            <img class="news_image" src="{{ asset($item->img) }}" alt="">
                                        </div>
                                    @else
                                        <div class="img">
                                            <span class="label">{{ $item->author->title }}</span>
                                            <img class="news_image" src="{{ asset('/images/photos.png') }}" alt="">
                                        </div>
                                    @endif
                                    <h3 class="news-title">{{ $item->title }}</h3>
                                    <p class="news-desc">{{ $item->description }}</p>
                                </a>
                                <div class="footer_news">
                                    <span class="data">{{ Date::parse($item->created_at)->format('j F Y, H:i ') }}</span>
                                    <div class="control">
                                        <a href="#">
                                            <img src="{{ asset('images/eye.svg') }}" alt="">
                                            <span>{{ $item->views->count() }}</span>
                                        </a>
                                        <a href="#!" class="like" data-id="{{ $item->id }}" data-type="blog">
                                            <img src="{{ asset('images/like.svg') }}" alt="">
                                            <span>{{ $item->likes->count() }}</span>
                                        </a>
                                        <a href="#" class="dislike" data-id="{{ $item->id }}" data-type="blog">
                                            <img src="{{ asset('images/dislike.svg') }}" alt="">
                                            <span>{{ $item->dislikes->count() }}</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif    
                @endforeach

            </div>
        </div>
    </section>

    <section class="sec4">
        <div class="container">
            @php
                $banner = $banners->whereNotNull('img_big')->random(1)[0];
            @endphp
            <a href="{{ $banner->url }}" target="blank" banner-big data-id="{{ $banner->id }}">
                <img src="{{ asset($banner->img_big) }}" alt="">
            </a>
        </div>
    </section>
    <section class="blogs">
        <div class="container">
            <div class="row outher_blogs">
                @foreach ($news as $key => $item)
                    @if ($key === 9 || $key === 11 || $key === 13 || $key === 14)    
                        <div class="col-12 col-md-4 col-lg-3 mb-4">
                            <div class="news">
                                <a href="{{ route('page.article', ['article' => $item->id]) }}">
                                    @if ($item->img)
                                        <div class="img small" data-bigstyle="{{ $item->big_style }}"  data-smallstyle="{{ $item->small_style }}">
                                            <span class="label">{{ $item->author->title }}</span>
                                            <img class="news_image" src="{{ asset($item->img) }}" alt="">
                                        </div>
                                    @else
                                        <div class="img">
                                            <span class="label">{{ $item->author->title }}</span>
                                            <img class="news_image" src="{{ asset('/images/photos.png') }}" alt="">
                                        </div>
                                    @endif
                                    <h3 class="news-title">{{ $item->title }}</h3>
                                    <p class="news-desc">{{ $item->description }}</p>
                                </a>
                                <div class="footer_news">
                                    <span class="data">{{ Date::parse($item->created_at)->format('j F Y, H:i ') }}</span>
                                    <div class="control">
                                        <a href="#">
                                            <img src="{{ asset('images/eye.svg') }}" alt="">
                                            <span>{{ $item->views->count() }}</span>
                                        </a>
                                        <a href="#!" class="like" data-id="{{ $item->id }}" data-type="blog">
                                            <img src="{{ asset('images/like.svg') }}" alt="">
                                            <span>{{ $item->likes->count() }}</span>
                                        </a>
                                        <a href="#" class="dislike" data-id="{{ $item->id }}" data-type="blog">
                                            <img src="{{ asset('images/dislike.svg') }}" alt="">
                                            <span>{{ $item->dislikes->count() }}</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @elseif($key === 10 || $key === 12)
                        <div class="col-12 col-md-4 col-lg-6 mb-4">
                            <div class="news">
                                <a href="{{ route('page.article', ['article' => $item->id]) }}">
                                    @if ($item->img)
                                        <div class="img big" data-bigstyle="{{ $item->big_style }}"  data-smallstyle="{{ $item->small_style }}">
                                            <span class="label">{{ $item->author->title }}</span>
                                            <img class="news_image" src="{{ asset($item->img) }}" alt="">
                                        </div>
                                    @else
                                        <div class="img">
                                            <span class="label">{{ $item->author->title }}</span>
                                            <img class="news_image" src="{{ asset('/images/photos.png') }}" alt="">
                                        </div>
                                    @endif
                                    <h3 class="news-title">{{ $item->title }}</h3>
                                    <p class="news-desc">{{ $item->description }}</p>
                                </a>
                                <div class="footer_news">
                                    <span class="data">{{ Date::parse($item->created_at)->format('j F Y, H:i ') }}</span>
                                    <div class="control">
                                        <a href="#">
                                            <img src="{{ asset('images/eye.svg') }}" alt="">
                                            <span>{{ $item->views->count() }}</span>
                                        </a>
                                        <a href="#!" class="like" data-id="{{ $item->id }}" data-type="blog">
                                            <img src="{{ asset('images/like.svg') }}" alt="">
                                            <span>{{ $item->likes->count() }}</span>
                                        </a>
                                        <a href="#" class="dislike" data-id="{{ $item->id }}" data-type="blog">
                                            <img src="{{ asset('images/dislike.svg') }}" alt="">
                                            <span>{{ $item->dislikes->count() }}</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif    
                @endforeach
            </div>
        </div>
    </section>
    
    <section class="pagination">
        <div class="container">
            {{ $news->links() }}
        </div>
    </section>


    @include('components.slider_arhives')
    <div class="space"></div>

@endsection