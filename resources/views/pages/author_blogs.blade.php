@extends('layouts.base')

@section('seo')
    @if ($author->seoTitle)
        <title>{{ $author->seoTitle }} | Вологодские новости</title>
        <meta property="og:title" content="{{ $author->seoTitle }} | Вологодские новости" />
    @else
        <title>{{ $author->title }} | Вологодские новости</title>
        <meta property="og:title" content="{{ $author->title }} | Вологодские новости" />
    @endif
    @if ($author->seoDesc)
        <meta name="description" content="{{ $author->seoDesc }}" />
        <meta property="og:description" content="{{ $author->seoDesc }}" />
    @endif
    @if ($author->seoKeywords)
        <meta name="keywords" content="{{ $author->seoKeywords }}">
    @endif
    @if ($author->img)
        <meta property="og:image" content="{{ asset($author->img) }}"/>
        <link rel="image_src" href="{{ asset($author->img) }}" />
    @else
        <meta property="og:image" content="{{ asset('images/site_logo.png') }}"/>
        <link rel="image_src" href="{{ asset('images/site_logo.png') }}" />
    @endif
@endsection

@section('content')
<div class="space"></div>

    <section class="headline">
        <div class="container bc">
            <div class="breadcrumbs">
                <a href="/">Главная</a>
                <a href="{{ route('page.authors') }}">Авторы</a>
                <span>{{ $author->title }}</span>
            </div>
            <div class="toback">
                <a href="{{ route('page.authors') }}">< Вернуться</a>
            </div>
        </div>
    </section>
    <section class="headline">
        <div class="container">
            <h1 style="padding-top: 20px;">{{ $author->title }}</h1>
        </div>
    </section>
    <section class="author_blogs pt-4">
        @if ($news->count() <= 0)    
            <div class="container">
                <p class="empty">Новостей не найдено</p>
            </div>
        @endif
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-8 offset-lg-1">
                   <div class="row">

                        @foreach ($news as $item)    
                            <div class="col-12 col-sm-6">
                                <div class="author_item">
                                    @if ($item->img)
                                        <img class="news_image" src="{{ asset($item->img) }}" alt="">
                                    @else
                                        <img class="news_image" src="{{ asset('/images/photos.png') }}" alt="">
                                    @endif
                                    <div class="body">
                                        <div class="desc">
                                            <h3>{{ $item->title }}</h3>
                                            <p>{{ $item->description }}</p>
                                        </div>
                                        <a href="{{ route('page.article', ['article' => $item->id]) }}" class="more_news">Читать</a>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                   </div>
                </div>
                <div class="col-3 d-none d-lg-block">
                    <div class="box-ad">
                        @php
                            $count = ($count_banner < 6) ? $count_banner : 6;
                        @endphp
                        @foreach ($banners->whereNotNull('img_small')->random($count) as $item)
                            <a href="{{ $item->url }}" target="blank" banner-small data-id="{{ $item->id }}">
                                <img src="{{ asset($item->img_small) }}" alt="">
                            </a>
                        @endforeach
                    </div>
                </div>
                <div class="col-12 mb-5 mt-3">
                    {{ $news->links() }}
                </div>
            </div>
        </div>
    </section>

    <section class="sec4 mob">
        <div class="container">
            @php
                $banner = $banners->whereNotNull('img_small')->random(1)[0];
            @endphp
            <a href="{{ $banner->url }}" target="blank" banner-small data-id="{{ $banner->id }}">
                <img src="{{ asset($banner->img_small) }}" alt="">
            </a>
        </div>
    </section>
    <section class="author_blogs">
        <div class="container">
            <div class="d-none d-md-block">

                <div class="row">
                    <div class="col-12">
                        <div class="head">
                            <p><img src="{{ asset('images/new-post.svg') }}" alt="">Другие авторы</p>
                        </div>
                    </div>
    
                    @foreach ($outher_authors as $item)
    
                        <div class="col-4">
                            <div class="author_item">
                                <img src="{{ asset($item->img) }}" alt="">
                                <div class="body">
                                    <div class="desc">
                                        <h3>{{ $item->title }}</h3>
                                        <p>{{ $item->description }}</p>
                                    </div>
                                    <a href="{{ route('page.author_blogs', ['author' => $item->id]) }}" class="more_news">В блог автора</a>
                                </div>
                            </div>
                        </div>
    
                    @endforeach
    
    
                </div>
            </div>
            <div class="outher_news d-block d-md-none">

                <div class="row">
                    <div class="col-12">
                        <div class="head">
                            <p>Другие авторы</p>
                        </div>
                    </div>
                    <div class="box_news">
                        @foreach ($outher_authors as $item)
    
                            <div class="author_item">
                                <img src="{{ asset($item->img) }}" alt="">
                                <div class="body">
                                    <div class="desc">
                                        <h3>{{ $item->title }}</h3>
                                        <p>{{ $item->description }}</p>
                                    </div>
                                    <a href="{{ route('page.author_blogs', ['author' => $item->id]) }}" class="more_news">В блог автора</a>
                                </div>
                            </div>
                            
                        @endforeach
                    </div>
    
    
                </div>
            </div>
        </div>
    </section>

    

    <div class="space"></div>

@endsection