@extends('layouts.base')

@section('seo')
    <title>{{ $project_news->title }} | Вологодские новости</title>

@endsection

@section('content')

    <section class="headline">
        <div class="container bc">
            <div class="breadcrumbs">
                <a href="/">Главная</a>
                <a href="{{ route('page.projects') }}">Проекты</a>
                <a href="{{ route('page.project', ['project' => $project->id]) }}">{{ $project->title }}</a>
                <span>{{ $project_news->title }}</span>
            </div>
            <h1></h1>
        </div>
    </section>

    <section class="poster new pt-4">
        <div class="container">
            <div class="row">
                <div class="col-9">
                    <div class="header">
                        <p>
                            <span>Автор: {{ $project_news->author_name }}</span>
                            <span>{{ Date::parse($project_news->created_at)->format('j F Y, H:i ') }}</span>
                        </p>                        
                    </div>
                        @if ($project_news->img)
                            <img class="poster_image" src="{{ asset( $project_news->img) }}" alt="">
                        @else
                            <img class="poster_image" src="{{ asset('images/width_news.png') }}" alt="">
                        @endif
                    <h3>{{ $project_news->title }}</h3>
                    <div class="body">{!! $project_news->body !!}</div>
                    <div class="spaser"></div>
                    <div class="footer_poster">
                        <div class="control">
                            <a href="#">
                                <img src="{{ asset('images/eye.svg') }}" alt="">
                                <span>{{ $project_news->views }}</span>
                            </a>
                            <a href="#">
                                <img src="{{ asset('images/like.svg') }}" alt="">
                                <span>{{ $project_news->likes }}</span>
                            </a>
                            <a href="#">
                                <img src="{{ asset('images/dislike.svg') }}" alt="">
                                <span>{{ $project_news->dislikes }}</span>
                            </a>
                            <div class="share">
                                <span>Поделиться</span>
                                <a href="#"><img src="{{ asset('images/vk.svg') }}" alt=""></a>
                                <a href="#"><img src="{{ asset('images/fb.svg') }}" alt=""></a>
                                <a href="#"><img src="{{ asset('images/ok.svg') }}" alt=""></a>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-5">
                        <div class="col-12">
                            <div class="head">
                                <p>
                                    <img src="{{ asset('images/media.svg') }}" alt="">
                                    Другие материалы
                                </p>
                            </div>
                        </div>
                        @foreach ($outher as $news)     
                            <div class="col-4">
                                <div class="news">
                                    <a href="{{ route('page.new', ['news' => $news->id]) }}" title="{{ $news->title }}">
                                        <span class="label">{{ Date::parse($news->created_at)->format('d.m.Y') }}</span>
                                        @if ($news->img)
                                            <img class="news_image" src="{{ asset($news->img) }}" alt="">
                                        @else
                                            <img class="news_image" src="{{ asset('/images/photos.png') }}" alt="">
                                        @endif
                                        <h3 class="news-title">{{ $news->title }}</h3>
                                        <p class="news-desc">{{ $news->description }}</p>
                                    </a>
                                    <div class="footer_news">
                                        <span class="data">{{ Date::parse($news->created_at)->format('j F Y, H:i ') }}</span>
                                        <div class="control">
                                            <a href="#">
                                                <img src="{{ asset('images/eye.svg') }}" alt="">
                                                <span>{{ $news->views }}</span>
                                            </a>
                                            <a href="#">
                                                <img src="{{ asset('images/like.svg') }}" alt="">
                                                <span>{{ $news->likes }}</span>
                                            </a>
                                            <a href="#">
                                                <img src="{{ asset('images/dislike.svg') }}" alt="">
                                                <span>{{ $news->dislikes }}</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    
                </div>
                <div class="col-3">
                    <div class="box-ad">
                        <img src="{{ asset('images/banners_min.jpg') }}" alt="">
                        <img src="{{ asset('images/banners_min.jpg') }}" alt="">
                        <img src="{{ asset('images/banners_min.jpg') }}" alt="">
                        <img src="{{ asset('images/banners_min.jpg') }}" alt="">
                    </div>

                </div>
            </div>
        </div>
    </section>
   
    <div class="space"></div>

@endsection