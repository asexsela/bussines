@extends('layouts.base')

@section('seo')
    <title>Проекты | Вологодские новости</title>
@endsection

@section('content')
    <section class="sec1">
        <div class="container">
            <img src="{{ asset('images/banner.png') }}" alt="">
        </div>
    </section>
    <section class="headline">
        <div class="container">
            <h1>Проекты</h1>
        </div>
    </section>
    
    <section class="projects">
        <div class="container">
            <div class="row">
                <div class="col-9">
                    <div class="row">
                        
                        @foreach ($projects as $item)
                            
                            <div class="col-4">
                                <div class="item_project">
                                    <a href="{{ route('page.project', ['project' => $item->id]) }}">
                                        @if ($item->img_spons)
                                            <img class="news_image" src="{{ asset($item->img_spons) }}" alt="">
                                        @else
                                            <img class="news_image" src="{{ asset('/images/photos.png') }}" alt="">
                                        @endif
                                        <p>{{ $item->title }}</p>
                                    </a>
                                </div>
                            </div>
                            
                        @endforeach

                    </div>
                </div>
                <div class="col-3">
                    <div class="box-ad">
                        <img src="{{ asset('images/banners_min.jpg') }}" alt="">
                        <img src="{{ asset('images/banners_min.jpg') }}" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <section class="pagination">
        <div class="container">
            {{-- pagination --}}
        </div>
    </section>

@endsection