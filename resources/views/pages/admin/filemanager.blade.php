@extends('layouts.admin')

@section('content')

    <div class="had-container">
        <div class="row">
            <div class="col s12">
                <h5>Файловый менеджер</h5>
                <div class="divider"></div>
            </div>
        </div>
    
        <div class="row">
            <div class="col s12">
                <iframe  width="100%" style="height:80vh" frameborder="0" src="/filemanager/dialog.php?type=0"></iframe>
            </div>
        </div>
        
    </div>


@endsection
