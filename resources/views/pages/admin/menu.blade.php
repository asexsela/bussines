@extends('layouts.admin')

@section('content')

    <div class="had-container">
        <div class="row">
            <div class="col s12">
                <h5>Меню</h5>
                <div class="divider"></div>
            </div>
        </div>
        @if ($menus->count() <= 0)
            <div class="row">
                <p style="
                text-align: center;
                font-size: 20px;
                color: red;
            ">Меню не найдены</p>
            </div>
        @else
        <div class="row">
            <div class="col s12">
                <table class="responsive-table highlight" id="table">
                    <thead>
                        <tr>
                            <th>id</th>
                            <th>Наименование</th>
                            <th>Обновление</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach ($menus as $item)
                            
                            <tr>
                                <td>{{ $item->id }}</td>
                                <td>{{ $item->name }}</td>
                                <td>
                                    {{ Date::parse($item->created_at)->format('j F Y, H:i ') }} ({{ Date::parse($item->updated_at)->ago() }})
                                    <div class="control">
                                        <a href="{{ route('menu.edit', ['menu' => $item->id]) }}" class="edit tooltipped" data-position="top" data-tooltip="Редактировать"><i class="material-icons">edit</i></a>
                                        <a href="{{ route('menu.destroy', ['menu' => $item->id]) }}" class="delete tooltipped" data-position="top" data-tooltip="Удалить"><i class="material-icons">delete</i></a>
                                    </div>
                                </td>
                            </tr>

                        @endforeach
                        
                    </tbody>
                </table>
            </div>
  
        </div>
        @endif
        
    </div>
    <div class="fixed-action-btn">
        <a href="{{ route('menu.create') }}" class="btn-floating btn-large green waves-effect pulse tooltipped" data-position="left" data-tooltip="Добавить">
            <i class="large material-icons">add</i>
        </a>
    </div>

@endsection
