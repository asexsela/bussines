@extends('layouts.admin')

@section('content')

    <div class="had-container">
        <div class="row">
            <div class="col s12">
                <h5>Благодарственные письма</h5>
                <div class="divider"></div>
            </div>
        </div>
        
        @if ($letters->count() <= 0)
            <div class="row">
                <p style="
                text-align: center;
                font-size: 20px;
                color: red;
            ">Письма не найдены</p>
            </div>
        @else
        <div class="row">
            <div class="col s12">
                <form action="{{ route('thanks_mails.save') }}" id="form-save">
                    @csrf
                    <div class="file-field input-field">
                        <a href="/filemanager/dialog.php?type=1&field_id=thanks_mails&relative_url=1&multiple=1" class="btn iframe-btn" type="button">Добавить изображения</a>
                        <div class="file-path-wrapper">
                            <input id="thanks_mails" class="" name="images" type="text" value="">
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col s12">
                <div class="body-block pa-2">
                    <div id="gallery" class="thanks_mail">
                        @foreach ($letters as $img)
                            <div class="img">
                                <div class="loader">
                                    <div class="progress loadImg">
                                        <div class="indeterminate"></div>
                                    </div>
                                </div>
                                <span class="remove del" data-url="{{ route('thanks_mails.destroy', ['letter' => $img->id]) }}"><i class="material-icons">delete</i></span>
                                <img src="{{ asset($img->img) }}">
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        @endif
        
    </div>

    <div class="fixed-action-btn">
        <a href="#!" id="save-button" class="btn-floating btn-large green waves-effect pulse tooltipped" data-position="left" data-tooltip="Сохранить">
            <i class="large material-icons">save</i>
        </a>
    </div>

@endsection

@section('scripts')

@endsection