@extends('layouts.admin')

@section('content')


    <div class="had-container">
        <div class="row">
            <div class="col s12">
                <h5>Приборная панель</h5>
                <div class="divider"></div>
            </div>
        </div>
        <div class="row blocks-info">

            <div class="col s6 xl3">

                <div class="info">
                    <p>Авторы</p>
                    <span>{{ $authors }}</span>
                </div>
                
            </div>

            <div class="col s6 xl3">

                <div class="info">
                    <p>Новости</p>
                    <span>{{ $news }}</span>
                </div>

            </div>

            <div class="col s6 xl3">

                <div class="info">
                    <p>Рубрики</p>
                    <span>{{ $heading }}</span>
                </div>

            </div>
            <div class="col s6 xl3">

                <div class="info">
                    <p>Фото</p>
                    <span>{{ $image }}</span>
                </div>

            </div>

            <div class="col s6 xl3">

                <div class="info">
                    <p>Видео</p>
                    <span>{{ $video }}</span>
                </div>

            </div>

            <div class="col s6 xl3">

                <div class="info">
                    <p>Номера</p>
                    <span>{{ $number }}</span>
                </div>

            </div>

            
        </div>
        <div class="row">
            <div class="col s12 m6 xl6">
                <canvas id="allData"></canvas>
            </div>
            <div class="col s12 m6 xl6">
                {{-- <canvas id="productData"></canvas> --}}
                <h5>Новые отзывы</h5>
                <div class="divider"></div>
                <div class="disabled-comment-box">
                    @foreach ($reviews as $item)     
                        <div class="item">
                            <img src="/images/photos.png" alt="">
                            <p>{!! $item->body !!}</p>
                            <div class="info-comment">
                                <a href="#!">{{ $item->name }},</a>
                                <span>{{ Date::parse($item->created_at)->format('j F Y г., H:i ') }} ({{ Date::parse($item->created_at)->ago() }})</span>
                            </div>
                            <div class="control">
                                @if ($item->active)
                                    <a class="update" href="#!" data-href="{{ route('review.update', ['review' => $item->id, 'type' => 'unpublished']) }}" class="edit tooltipped" data-position="top" data-tooltip="Опубликовать"><i class="material-icons">close</i></a>
                                @else
                                    <a class="update" href="#!" data-href="{{ route('review.update', ['review' => $item->id, 'type' => 'published']) }}" class="edit tooltipped" data-position="top" data-tooltip="Опубликовать"><i class="material-icons">publish</i></a>
                                @endif
                                <a href="{{ route('review.destroy', ['review' => $item->id]) }}" class="delete tooltipped" data-position="top" data-tooltip="Удалить"><i class="material-icons">delete</i></a>
                            </div>
                        </div>
                    @endforeach
                    
                </div>
            </div>
        </div>
        <div class="row">
            {{-- <div class="col s12 xl6">
                <h5>Новые комментарии</h5>
                <div class="divider"></div>
                <div class="disabled-comment-box">
                    @foreach ($reviews as $item)     
                        <div class="item">
                            <img src="/images/photos.png" alt="">
                            <p>{{ $item->body }}</p>
                            <div class="info-comment">
                                <a href="#!">{{ $item->name }},</a>
                                <span>{{ Date::parse($item->created_at)->format('j F Y г., H:i ') }} ({{ Date::parse($item->created_at)->ago() }})</span>
                            </div>
                            <div class="control">
                                @if ($item->active)
                                    <a class="update" href="#!" data-href="{{ route('review.update', ['review' => $item->id, 'type' => 'unpublished']) }}" class="edit tooltipped" data-position="top" data-tooltip="Опубликовать"><i class="material-icons">close</i></a>
                                @else
                                    <a class="update" href="#!" data-href="{{ route('review.update', ['review' => $item->id, 'type' => 'published']) }}" class="edit tooltipped" data-position="top" data-tooltip="Опубликовать"><i class="material-icons">publish</i></a>
                                @endif
                                <a href="{{ route('review.destroy', ['review' => $item->id]) }}" class="delete tooltipped" data-position="top" data-tooltip="Удалить"><i class="material-icons">delete</i></a>
                            </div>
                        </div>
                    @endforeach
                    
                </div>
            </div> --}}
            {{-- <div class="col s12 xl6">
                <h5>Новые пользователи</h5>
                <div class="divider"></div>
                <div class="news-users-box">
                    <div class="item">
                        <img src="/adm/images/profile.png" alt="">
                        <p>Иванов Иван Иванович</p>
                        <div class="info-comment">
                            <span>22.05.2020 г.</span>
                        </div>
                        <div class="control">
                            <a href="#!" class="home tooltipped" data-position="left" data-tooltip="Профиль"><i class="material-icons">home</i></a>
                        </div>
                    </div>
                    <div class="item">
                        <img src="/adm/images/profile.png" alt="">
                        <p>Иванов Иван Иванович</p>
                        <div class="info-comment">
                            <span>22.05.2020 г.</span>
                        </div>
                        <div class="control">
                            <a href="#!" class="home tooltipped" data-position="left" data-tooltip="Профиль"><i class="material-icons">home</i></a>
                        </div>
                    </div>
                    <div class="item">
                        <img src="/adm/images/profile.png" alt="">
                        <p>Иванов Иван Иванович</p>
                        <div class="info-comment">
                            <span>22.05.2020 г.</span>
                        </div>
                        <div class="control">
                            <a href="#!" class="home tooltipped" data-position="left" data-tooltip="Профиль"><i class="material-icons">home</i></a>
                        </div>
                    </div>
                    <div class="item">
                        <img src="/adm/images/profile.png" alt="">
                        <p>Иванов Иван Иванович</p>
                        <div class="info-comment">
                            <span>22.05.2020 г.</span>
                        </div>
                        <div class="control">
                            <a href="#!" class="home tooltipped" data-position="left" data-tooltip="Профиль"><i class="material-icons">home</i></a>
                        </div>
                    </div>
                    <div class="item">
                        <img src="/adm/images/profile.png" alt="">
                        <p>Иванов Иван Иванович</p>
                        <div class="info-comment">
                            <span>22.05.2020 г.</span>
                        </div>
                        <div class="control">
                            <a href="#!" class="home tooltipped" data-position="left" data-tooltip="Профиль"><i class="material-icons">home</i></a>
                        </div>
                    </div>
                    <div class="item">
                        <img src="/adm/images/profile.png" alt="">
                        <p>Иванов Иван Иванович</p>
                        <div class="info-comment">
                            <span>22.05.2020 г.</span>
                        </div>
                        <div class="control">
                            <a href="#!" class="home tooltipped" data-position="left" data-tooltip="Профиль"><i class="material-icons">home</i></a>
                        </div>
                    </div>
                    <div class="item">
                        <img src="/adm/images/profile.png" alt="">
                        <p>Иванов Иван Иванович</p>
                        <div class="info-comment">
                            <span>22.05.2020 г.</span>
                        </div>
                        <div class="control">
                            <a href="#!" class="home tooltipped" data-position="left" data-tooltip="Профиль"><i class="material-icons">home</i></a>
                        </div>
                    </div>

                </div>
            </div> --}}
        </div>
    </div>

    {{-- <div class="fixed-action-btn">
        <a class="btn-floating btn-large green pulse">
            <i class="large material-icons">add</i>
        </a>
        <ul>
            <li><a href="#!" class="btn-floating red tooltipped" data-position="left" data-tooltip="Добавить категорию"><i class="material-icons">category</i></a></li>
            <li><a href="#!" class="btn-floating yellow darken-1 tooltipped" data-position="left" data-tooltip="Добавить статью"><i class="material-icons">post_add</i></a></li>
            <li><a href="#!" class="btn-floating teal tooltipped" data-position="left" data-tooltip="Добавить пользователя"><i class="material-icons" >supervised_user_circle</i></a></li>
        </ul>
    </div> --}}

@endsection

@section('scripts')
    <script>
        $(function () {

            // var ctx = document.getElementById('productData').getContext('2d');
            // var chart = new Chart(ctx, {
            //     // The type of chart we want to create
            //     type: 'line',

            //     // The data for our dataset
            //     data: {
            //         labels: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            //         datasets: [{
            //             label: 'Просмотры новостей',
            //             backgroundColor: '#03A9F4',
            //             borderColor: '#0c97d6',
            //             data: [0, 10, 5, 2, 20, 30, 45, 30, 20, 10, 50, 35]
            //         }]
            //     },

            //     // Configuration options go here
            //     options: {}
            // });

            var ctx = document.getElementById('allData').getContext('2d');
            var chart = new Chart(ctx, {
                // The type of chart we want to create
                type: 'pie',

                // The data for our dataset
                data: {
                    labels: ['Авторы', 'Новости', 'Рубрики', 'Фото', 'Видео', 'Номера'],
                    datasets: [{
                        label: 'Заказы',
                        backgroundColor: ['#4caf50', '#E91E63', '#9C27B0', '#2196F3', '#FFC107', '#ee6e73'],
                        borderColor: '#fff',
                        data: [{{ $authors }}, {{ $news }}, {{ $heading }}, {{ $image }}, {{ $video }}, {{ $number }}]
                    }]
                },

                // Configuration options go here
                options: {}
            });
        })
    </script>

@endsection