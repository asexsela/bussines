@extends('layouts.admin')

@section('content')

    <div class="had-container">
        <div class="row">
            <div class="col s12">
                <h5>{{ $heading->title }}</h5>
                <div class="divider"></div>
            </div>
        </div>

        <div class="row">
            <form id="form-save" action="{{ route('heading.update', ['heading' => $heading->id]) }}">
                <div class="col s12 m6 l12 xl6">
                    {{ csrf_field() }}
                    <div class="body-block pa-2">
                        
                        <div class="input-field">
                            <input id="category-title" type="text" name="name" value="{{ $heading->name }}" class="validate" required>
                            <label for="category-title">Заголовок*</label>
                            <span class="helper-text">Рекомендуемое количество символов: 30</span>
                        </div>
                        <div class="input-field">
                            <input id="category-slug" type="text" name="slug" value="{{ $heading->slug }}">
                            <label for="category-slug">URL</label>
                            <span class="helper-text">Если оставить пустым, то сгенериурется автоматически</span>
                        </div>
                        <div class="input-field select-category">
                            <select name="category_id">
                                <option value="0" @if($heading->category_id === 0) selected @endif>Выберете категорию</option>
                                @foreach ($categories as $item)
                                    <option value="{{ $item->id }}" @if($heading->category_id === $item->id) selected @endif>{{ $item->title }}</option>
                                @endforeach
                            </select>
                            <label>Категория</label>
                        </div>
                    </div>
                </div>
                <div class="col s12 m6 l12 xl6">  
                    <div class="body-block pa-2">
                        <div class="input-field">
                            <input id="seo-title" name="seoTitle" type="text" class="validate" value="{{ $heading->seoTitle }}">
                            <label for="seo-title">SEO Заголовок</label>
                        </div>
                        <div class="input-field">
                            <textarea id="meta" name="seoDesc" class="materialize-textarea">{{ $heading->seoDesc }}</textarea>
                            <label for="meta">SEO description </label>
                        </div>

                        <div class="input-field">
                            <textarea id="desc" name="seoKeywords" class="materialize-textarea">{{ $heading->seoKeywords }}</textarea>
                            <label for="desc">SEO keywords</label>
                        </div>
                        <div class="input-field">
                            <p>Публикация</p>
                            <div class="switch">
                                <label>
                                    Нет
                                    <input type="checkbox" name="active" @if($heading->active) checked value="1" @else value="0" @endif>
                                    <span class="lever"></span>
                                    Да
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>



        <div class="fixed-action-btn">
            <a class="btn-floating btn-large blue pulse">
                <i class="large material-icons">more_vert</i>
            </a>
            <ul>
                <li><a href="{{ route('heading') }}" class="btn-floating red darken-1 tooltipped" data-position="left" data-tooltip="Отмена"><i class="material-icons">keyboard_backspace</i></a></li>
                <li><a href="#" id="save-button" class="btn-floating green tooltipped" data-position="left" data-tooltip="Сохранить"><i class="material-icons">save</i></a></li>
            </ul>
        </div>
    </div>

@endsection

@section('scripts')
  

@endsection