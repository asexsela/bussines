@extends('layouts.admin')

@section('content')
    <div class="had-container">
        <div class="row">
            <div class="col s12">
                <h5>{{ $partner->title }}</h5>
                <div class="divider"></div>
            </div>
        </div>

        <div class="row">
            <form action="{{ route('partner.update', ['partner' => $partner->id]) }}" id="form-save">

                @csrf
                <div class="col s12 m8 l12 xl8">

                    <div class="body-block pa-2">
                        <div class="input-field">
                            <input id="product-title" type="text" name="title" class="validate" value="{{ $partner->title }}">
                            <label for="product-title">Наименование*</label>
                            <span class="helper-text">Рекомендуемое количество символов: 30</span>
                        </div>
                        <div class="input-field">
                            <input id="product-url" type="text" name="url" value="{{ $partner->url }}">
                            <label for="product-url">URL</label>
                        </div>
                        <div class="input-field select-product">
                            <select name="partnercat_id">
                                <option value="0" @if($partner->partnercat_id === 0) selected @endif disabled>Выберете категорию</option>
                                @foreach ($category as $item)    
                                    <option value="{{ $item->outher_id }}" @if($partner->partnercat_id === $item->outher_id) selected @endif>{{ $item->title }}</option>
                                @endforeach
                            </select>
                            <label>Категория</label>
                        </div>
                    </div>

                </div>
                <div class="col s12 m4 l12 xl4">
                    <div class="body-block pa-2">
                        <p>Основное изображение</p>
                        <div class="file-field input-field">
                            <a href="/filemanager/dialog.php?type=2&field_id=field&relative_url=1" class="btn iframe-btn" type="button">Выбрать изображение</a>
                            <div class="file-path-wrapper">
                                <input id="field" class="file-path validate" name="img" type="text" value="{{ $partner->img }}">
                            </div>
                        </div>
                        <p>Предпросмотр изображения</p>
                        <img 
                            class="no-cropper"
                            width="100%" 
                            style="max-width:100%;display: block;width: auto;margin: 0 auto;"
                            src="{{ asset($partner->img) }}">
                    </div>
                </div>
            </form>
        </div>



        <div class="fixed-action-btn">
            <a class="btn-floating btn-large blue pulse">
                <i class="large material-icons">more_vert</i>
            </a>
            <ul>
                <li><a href="{{ route('partner') }}" class="btn-floating red darken-1 tooltipped" data-position="left" data-tooltip="Отмена"><i class="material-icons">keyboard_backspace</i></a></li>
                <li><a href="#!" id="save-button" class="btn-floating green tooltipped" data-position="left" data-tooltip="Сохранить"><i class="material-icons">save</i></a></li>
            </ul>
        </div>
    </div>
@endsection