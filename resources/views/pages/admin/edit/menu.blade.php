@extends('layouts.admin')

@section('content')
    <div class="had-container">
        <div class="row">
            <div class="col s12">
                <h5>{{ $menu->name }}</h5>
                <div class="divider"></div>
            </div>
        </div>

        <div class="row">
            <div class="col s12 m6 l12 xl6">
                <form id="form-save-menu" action="{{ route('menu.update', ['menu' => $menu->id]) }}">
                    {{ csrf_field() }}
                    <div class="body-block pa-2">
                        <div class="input-field">
                            <input id="menu-name" type="text" name="name" class="validate" required value="{{ $menu->name }}">
                            <label for="menu-name">Название*</label>
                            <span class="helper-text" data-error="Заполните это поле">Обязательное поле</span>
                        </div>


                    </div>
            </div>
            <div class="col s12 m6 l12 xl6">     

                <div id="sortable">
                    @foreach ($menu->items as $item)    
                        <div data-id="{{ $item->id }}" class="item-menu">
                            <div class="title">
                                <div class="img">
                                    <a href="/filemanager/dialog.php?type=2&field_id=img{{ $item->id }}&relative_url=1" class="change iframe-btn">+</a>
                                    <input type="hidden" class="item-img" id="img{{ $item->id }}" value="{{ $item->img }}">
                                    @if ($item->img)
                                        <img src="{{ asset($item->img) }}" alt="">
                                    @else
                                        <img src="{{ asset('images/photos.png') }}" alt="">
                                    @endif
                                </div>
                                <p>{{ $item->name }}</p>
                                <span class="arrow">&#8250;</span>
                                <span data-url="{{ route('menu.item.destroy', ['item' => $item->id]) }}" class="remove-item">&times;</span>
                            </div>
                            <div class="content">
                                <div class="row">
                                    <div class="col s12 l6">
                                        <div class="input-field">
                                            <input class="item-name" type="text" value="{{ $item->name }}" required>
                                            <label>Название*</label>
                                        </div>
                                    </div>
                                    <div class="col s12 l6">
                                        <div class="input-field">
                                            <input class="item-url" type="text" value="{{ $item->url }}" required>
                                            <label>URL*</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach

                </div>
                <a class="add-item-menu btn-floating btn-small waves-effect waves-light green"><i class="material-icons">add</i></a>

            </div>
            

        </div>



        <div class="fixed-action-btn">
            <a class="btn-floating btn-large blue pulse">
                <i class="large material-icons">more_vert</i>
            </a>
            <ul>
                <li><a id="back_parent" href="{{ route('menu') }}" class="btn-floating red darken-1 tooltipped" data-position="left" data-tooltip="Отмена"><i class="material-icons">keyboard_backspace</i></a></li>
                <li><a href="#" id="save-button-menu" class="btn-floating green tooltipped" data-position="left" data-tooltip="Сохранить"><i class="material-icons">save</i></a></li>
            </ul>
        </div>
    </div>

@endsection

@section('scripts') 
    <script>
        $('.item-menu').on('mousedown', function() {
            $('.item-menu-placeholder').css('height', $(this).height())
        })
    </script>
@endsection