@extends('layouts.admin')

@section('content')
    <div class="had-container">
        <div class="row">
            <div class="col s12">
                <h5>{{ $news->title }}</h5>
                <div class="divider"></div>
            </div>
        </div>
        <div class="row">
            <form action="{{ route('heading.news.update', ['news' => $news->id]) }}" id="form-save">
                @csrf
                <div class="col s12 m8 l12 xl8">
                    <div class="body-block pa-2">
                        <div class="input-field">
                            <input id="product-title" type="text" name="title" class="validate" value="{{ $news->title }}">
                            <label for="product-title">Заголовок*</label>
                            <span class="helper-text">Рекомендуемое количество символов: 30</span>
                        </div>
                        <div class="input-field">
                            <textarea id="description" class="materialize-textarea" name="description">{{ $news->description }}</textarea>
                            <label for="description">Небольшое описание</label>
                        </div>
                        <div class="input-field">
                            <textarea id="editor" name="body">{!! $news->body !!}</textarea>
                        </div>
                        
                    </div>
                    <div class="body-block pa-2">
                        <div class="input-field">
                            <input id="seo-title" type="text" class="validate" name="seoTitle" value="{{ $news->seoTitle }}"> 
                            <label for="seo-title">SEO title</label>
                        </div>
                        <div class="input-field">
                            <textarea id="meta" class="materialize-textarea" name="seoDesc">{{ $news->seoDesc }}</textarea>
                            <label for="meta">SEO описание</label>
                        </div>

                        <div class="input-field">
                            <textarea id="desc" class="materialize-textarea" name="seoKeywords">{{ $news->seoKeywords }}</textarea>
                            <label for="desc">SEO ключевые слова</label>
                        </div>

                    </div>
                   
                </div>
                <div class="col s12 m4 l12 xl4">
                    <div class="body-block pa-2">
                        <div class="input-field">
                            <input id="product-slug" type="text" class="validate" name="slug" value="{{ $news->slug }}">
                            <label for="product-slug">URL</label>
                            <span class="helper-text">Если оставить пустым, то сгенерируется автоматически</span>
                        </div>
                        <div class="input-field">
                            <input id="product-price" type="text" class="validate" name="author_name" value="{{ $news->author_name }}">
                            <label for="product-price">Автор</label>
                        </div>
                        <div class="input-field select-product">
                            <select name="category_id">
                                <option value="0" @if($news->category_id === 0) selected @endif>Выберете категорию</option>
                                @foreach ($category as $item)    
                                    <option value="{{ $item->id }}" @if($news->category_id === $item->id) selected @endif>{{ $item->title }}</option>
                                @endforeach
                            </select>
                            <label>Категория</label>
                        </div>
                        <div class="input-field select-product">
                            <select name="heading_id">
                                <option value="0" @if($news->heading_id === 0) selected @endif>Выберете рубрику</option>
                                @foreach ($headings as $item)    
                                    <option value="{{ $item->id }}" @if($news->heading_id === $item->id) selected @endif>{{ $item->name }}</option>
                                @endforeach
                            </select>
                            <label>Рубрика</label>
                        </div>
                        <div class="input-field">
                            <div class="chips"></div>
                            <span class="helper-text">Введите слово и нажмите Enter</span>
                        </div> 
                        <div class="row">
                            <div class="col s12 xl4">
                                <div class="input-field">
                                    <p>Публикация</p>
                                    <div class="switch">
                                        <label>
                                            Нет
                                            <input type="checkbox" name="isActive"  @if($news->isActive) checked value="1" @else value="0" @endif>
                                            <span class="lever"></span>
                                            Да
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col s12 xl4">
                                <div class="input-field">
                                    <p>Тема дня</p>
                                    <div class="switch">
                                        <label>
                                            Нет
                                            <input type="checkbox" name="themeIsDay" @if($news->themeIsDay) checked value="1" @else value="0" @endif>
                                            <span class="lever"></span>
                                            Да
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="input-field">
                            <p>Дата публикации</p>
                            <input type="text" class="datepicker" name="published_at" value="{{ Date::parse($news->published_at)->format('d.m.Y') }}" placeholder="Выберете дату публикации">
                            <span class="helper-text">Если поставить будущую дату, то измениться дата пуюликации</span>
                        </div> 

                    </div>
                    <div class="body-block pa-2">
                        <p>Основное изображение</p>
                        <div class="file-field input-field">
                            <a href="/filemanager/dialog.php?type=2&field_id=field&relative_url=1" class="btn iframe-btn" type="button">Выбрать изображение</a>

                            <div class="file-path-wrapper">
                                <input id="field" class="file-path validate" name="img" type="text" value="{{ $news->img }}">
                            </div>
                        </div>
                        <p>Предпросмотр изображения</p>
                        <div id="preview-img">
                            <img 
                                id="image" 
                                width="100%" 
                                style="max-width:100%; display: block;"
                                data-cropper="{{ $news->cropper }}"
                                src="{{ asset($news->img) }}">
                        </div> 
                    </div>

                </div>
                <div class="col s12">
                    <div class="body-block pa-2 howToViewInPage">
                        <p>Как это будет выглядеть на сайте</p>
                        <div class="images">
                            <div class="big img" data-style="{{ $news->big_style }}">
                                <img src="{{ asset($news->img) }}" alt="">
                            </div>
                            <div class="small img" data-style="{{ $news->small_style }}">
                                <img src="{{ asset($news->img) }}" alt="">
                            </div>
                        </div>
                        <div class="images" style="visibility:hidden;position:absolute;">
                            <div class="big big_img"> </div>
                            <div class="small big_img"></div>
                        </div>
                        <input type="hidden" name="big_style">
                        <input type="hidden" name="small_style">
                        <input type="hidden" name="cropper">
                    </div>
                </div>
            </form>
        </div>



        <div class="fixed-action-btn">
            <a class="btn-floating btn-large blue pulse">
                <i class="large material-icons">more_vert</i>
            </a>
            <ul>
                <li><a href="{{ route('heading.news') }}" class="btn-floating red darken-1 tooltipped" data-position="left" data-tooltip="Отмена"><i class="material-icons">keyboard_backspace</i></a></li>
                <li><a href="#!" id="save-button" class="btn-floating green tooltipped" data-position="left" data-tooltip="Сохранить"><i class="material-icons">save</i></a></li>
            </ul>
        </div>
    </div>
@endsection

@section('scripts')
    @php
        if($news->tags) {
            $tags = explode(',', $news->tags);
        } else {
            $tags = null;
        }
    @endphp
<script>

        $('.chips').chips({
            placeholder: 'Теги',
            secondaryPlaceholder: '+Тег',
            data: [
                @if($tags)
                    @foreach ($tags as $tag)
                        {tag: "{{ trim($tag) }}"},     
                    @endforeach
                @endif
    
            ],
        });

        // if(document.querySelector('#editor')) {

        //     ClassicEditor
        //         .create(document.querySelector('#editor'), {
        //             language: 'ru',
        //             ckfinder: {
        //                 uploadUrl: "{{route('heading.news.upload', ['_token' => csrf_token() ])}}",
        //             }
        //         })
        //         .then(editor => {

        //             let images = $('.ck-content').find('img');

        //             for (let i = 0; i < images.length; i++) {
        //                 const e = images[i],
        //                     src = $(e).attr('src');
        //                     if(src[0] != '/' && src[0] != 'h') {
        //                         $(e).attr('src', '/'+src);
        //                     }
        //             }

        //         })
        //         .catch(error => {
        //                 console.log(error);
        //         });

        // }


    </script>

@endsection