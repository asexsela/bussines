@extends('layouts.admin')

@section('content')

    <div class="had-container">
        <div class="row">
            <div class="col s12">
                <h5>{{ $category->title }}</h5>
                <div class="divider"></div>
            </div>
        </div>

        <div class="row">
            <div class="col s12 m6 l12 xl6">
                <form id="form-save" action="{{ route('category.update', ['category' => $category->id]) }}">
                    {{ csrf_field() }}
                    <div class="body-block pa-2">
                        <div class="input-field">
                            <input id="category-title" type="text" name="title" value="{{ $category->title }}" class="validate" required>
                            <label for="category-title">Заголовок*</label>
                            <span class="helper-text">Рекомендуемое количество символов: 30</span>
                        </div>
                        <div class="input-field">
                            <input id="category-slug" type="text" name="slug" value="{{ $category->slug }}">
                            <label for="category-slug">URL</label>
                            <span class="helper-text">Если оставить пустым, то сгенериурется автоматически</span>
                        </div>
                        <div class="input-field select-category">
                            <select name="category_id">
                                <option @if($category->category_id === 0) value="0" selected @endif>Выберете категорию</option>
                                @foreach ($categories as $item)
                                    @if ($category->id !== $item->id)
                                        <option value="{{ $item->id }}" @if($category->category_id === $item->id) selected @endif>{{ $item->title }}</option>
                                    @endif
                                @endforeach
                            </select>
                            <label>Категория</label>
                        </div>
                        <div class="input-field select-category">
                            <select name="heading_id">
                                <option selected value="0">Выберете рубрику</option>
                                @foreach ($headings as $item)
                                    <option value="{{ $item->id }}" @if($category->heading_id === $item->id) selected @endif>{{ $item->name }}</option>
                                @endforeach
                            </select>
                            <label>Рубрика</label>
                        </div>
                    </div>
                </form>
            </div>
        </div>



        <div class="fixed-action-btn">
            <a class="btn-floating btn-large blue pulse">
                <i class="large material-icons">more_vert</i>
            </a>
            <ul>
                <li><a href="{{ route('category') }}" class="btn-floating red darken-1 tooltipped" data-position="left" data-tooltip="Отмена"><i class="material-icons">keyboard_backspace</i></a></li>
                <li><a href="#" id="save-button" class="btn-floating green tooltipped" data-position="left" data-tooltip="Сохранить"><i class="material-icons">save</i></a></li>
            </ul>
        </div>
    </div>

@endsection

@section('scripts')
  

@endsection