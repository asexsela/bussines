@extends('layouts.admin')

@section('content')
    <div class="had-container">
        <div class="row">
            <div class="col s12">
                <h5>{{ $voiting->title }}</h5>
                <div class="divider"></div>
            </div>
        </div>

        <div class="row">
            <form action="{{ route('voiting.update', ['voiting' => $voiting->id]) }}" id="form-save">

                @csrf
                <div class="col s12 m8 l12 xl8">
                    <div class="body-block pa-2">
                        <div class="input-field">
                            <input id="product-title" type="text" name="title" class="validate" value="{{ $voiting->title }}">
                            <label for="product-title">Заголовок*</label>
                        </div>
                        <div class="input-field">
                            <p>Дата окончания</p>
                            <input type="text" class="datepicker" name="date_end" value="{{ Date::parse($voiting->date_end)->format('d.m.Y ') }}" placeholder="Выберете дату публикации">
                        </div> 
                        <div class="input-field">
                            <p>На главную</p>
                            <div class="switch">
                                <label>
                                    Нет
                                    <input type="checkbox" name="isIndex" value="{{ $voiting->isIndex }}" @if($voiting->isIndex) checked value="1" @else value="0" @endif>
                                    <span class="lever"></span>
                                    Да
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col s12 m4 l12 xl4">

                    <div id="voiting_items" class="body-block pa-2">
                        <p>Варианты ответа</p>
                        <div class="content">
                            @foreach ($voiting->items as $key => $item)
                           
                                <div class="input-field">
                                    <input type="text" name="items[]" value="{{ $item->title }}" placeholder="Заголовок">
                                    <span class="remove-item" data-url="{{ route('voiting.item.destroy', ['item' => $item->id]) }}"><i class="material-icons">delete</i></span>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <a class="add-item btn-floating btn-small waves-effect waves-light green"><i class="material-icons">add</i></a>

                </div>
            </form>
        </div>



        <div class="fixed-action-btn">
            <a class="btn-floating btn-large blue pulse">
                <i class="large material-icons">more_vert</i>
            </a>
            <ul>
                <li><a id="back_parent" href="{{ route('voiting') }}" class="btn-floating red darken-1 tooltipped" data-position="left" data-tooltip="Отмена"><i class="material-icons">keyboard_backspace</i></a></li>
                <li><a href="#!" id="save-button" class="btn-floating green tooltipped" data-position="left" data-tooltip="Сохранить"><i class="material-icons">save</i></a></li>
            </ul>
        </div>
    </div>
@endsection
