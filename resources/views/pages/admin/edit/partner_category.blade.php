@extends('layouts.admin')

@section('content')

    <div class="had-container">
        <div class="row">
            <div class="col s12">
                <h5>{{ $category->title }}</h5>
                <div class="divider"></div>
            </div>
        </div>

        <div class="row">
            <div class="col s12 m6 l12 xl6">
                <form id="form-save" action="{{ route('parnter.category.update', ['category' => $category->id]) }}">
                    {{ csrf_field() }}
                    <div class="body-block pa-2">
                        <div class="input-field">
                            <input id="category-title" type="text" name="title" value="{{ $category->title }}" class="validate" required>
                            <label for="category-title">Заголовок*</label>
                            <span class="helper-text">Рекомендуемое количество символов: 30</span>
                        </div>
                    </div>
                </form>
            </div>
        </div>



        <div class="fixed-action-btn">
            <a class="btn-floating btn-large blue pulse">
                <i class="large material-icons">more_vert</i>
            </a>
            <ul>
                <li><a href="{{ route('parnter.category') }}" class="btn-floating red darken-1 tooltipped" data-position="left" data-tooltip="Отмена"><i class="material-icons">keyboard_backspace</i></a></li>
                <li><a href="#" id="save-button" class="btn-floating green tooltipped" data-position="left" data-tooltip="Сохранить"><i class="material-icons">save</i></a></li>
            </ul>
        </div>
    </div>

@endsection

@section('scripts')
  

@endsection