@extends('layouts.admin')

@section('content')
    <div class="had-container">
        <div class="row">
            <div class="col s12">
                <h5>{{ $banner->title }}</h5>
                <div class="divider"></div>
            </div>
        </div>

        <div class="row">
            <form action="{{ route('banner.update', ['banner' => $banner->id]) }}" id="form-save">

                @csrf
              
                <div class="col s12 m8 l12 xl8">
                    <div class="body-block pa-2">
                        <div class="input-field">
                            <input id="product-title" type="text" name="title" class="validate" value="{{ $banner->title }}">
                            <label for="product-title">Заголовок*</label>
                        </div>
                        <div class="input-field">
                            <input id="product-url" type="text" name="url" class="validate" value="{{ $banner->url }}">
                            <label for="product-url">URL</label>
                        </div>

                    </div>
                </div>
                <div class="col s12 m4 l12 xl4">

                    <div class="body-block pa-2">
                        <p>Большой баннер</p>
                        <div class="file-field input-field">
                            <a href="/filemanager/dialog.php?type=2&field_id=field&relative_url=1" class="btn iframe-btn" type="button">Выбрать изображение</a>
                            <div class="file-path-wrapper">
                                <input id="field" class="file-path validate" name="img_big" type="text" value="{{ $banner->img_big }}">
                            </div>
                        </div>
                        <p>Предпросмотр изображения</p>
                        <img 
                            class="no-cropper"
                            width="100%" 
                            style="max-width:100%;display: block;width: auto;margin: 0 auto;"
                            src="{{ asset($banner->img_big) }}">
                    </div>

                    <div class="body-block pa-2">
                        <p>Маленкий баннер</p>
                        <div class="file-field input-field">
                            <a href="/filemanager/dialog.php?type=2&field_id=field2&relative_url=1" class="btn iframe-btn" type="button">Выбрать изображение</a>
                            <div class="file-path-wrapper">
                                <input id="field2" class="file-path validate" name="img_small" type="text" value="{{ $banner->img_small }}">
                            </div>
                        </div>
                        <p>Предпросмотр изображения</p>
                        <img id="image-file" width="100%" style="max-width:100%; display: block;" src="{{ asset($banner->img_small) }}">
                    </div>

                </div>
            </form>
        </div>



        <div class="fixed-action-btn">
            <a class="btn-floating btn-large blue pulse">
                <i class="large material-icons">more_vert</i>
            </a>
            <ul>
                <li><a id="back_parent" href="{{ route('banner') }}" class="btn-floating red darken-1 tooltipped" data-position="left" data-tooltip="Отмена"><i class="material-icons">keyboard_backspace</i></a></li>
                <li><a href="#!" id="save-button" class="btn-floating green tooltipped" data-position="left" data-tooltip="Сохранить"><i class="material-icons">save</i></a></li>
            </ul>
        </div>
    </div>
@endsection
