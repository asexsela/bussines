@extends('layouts.admin')

@section('content')

    <div class="had-container">
        <div class="row">
            <div class="col s12">
                <h5>Программы</h5>
                <div class="divider"></div>
            </div>
        </div>
        <div class="row">
            <div class="col s6">

            </div>
            <div class="col s12 l3 offset-l3">
                <div class="input-field">
                    <input id="search" type="text" class="validate">
                    <label for="search">Поиск</label>
                </div>
            </div>
        </div>
        @if ($news->count() <= 0)
            <div class="row">
                <p style="
                text-align: center;
                font-size: 20px;
                color: red;
            ">Дневники не найдены</p>
            </div>
        @else
        <div class="row">
            <div class="col s12">
                <table class="responsive-table highlight" id="table">
                    <thead>
                        <tr>
                            <th>id</th>
                            <th class="image">Изображение</th>
                            <th>Автор</th>
                            <th>Публикация</th>
                            <th>Наименование</th>
                            <th>Обновление</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach ($news as $item)

                            <tr>
                                <td>{{ $item->id }}</td>
                                <td><img class="materialboxed" src="{{ asset($item->img) }}" alt=""></td>
                                <td>{{ $item->author_name }}</td>
                                <td>
                                    @if($item->isActive)
                                        <span class="yes">Да</span>
                                    @else
                                        <span class="no">Нет</span>
                                    @endif
                                </td>
                                <td>{{ $item->title }}</td>
                                <td>
                                    {{ Date::parse($item->created_at)->format('j F Y, H:i ') }} ({{ Date::parse($item->updated_at)->ago() }})
                                    <div class="control">
                                        <a href="{{ route('notes.edit', ['note' => $item->id]) }}" class="edit tooltipped" data-position="top" data-tooltip="Редактировать"><i class="material-icons">edit</i></a>
                                        <a href="{{ route('notes.destroy', ['note' => $item->id]) }}" class="delete tooltipped" data-position="top" data-tooltip="Удалить"><i class="material-icons">delete</i></a>
                                    </div>
                                </td>
                            </tr>

                        @endforeach
                        
                    </tbody>
                </table>
            </div>
            <div class="col s12">
                {{ $news->links('components.admin.pagination') }}
            </div>
        </div>
        @endif
        
    </div>
    <div class="fixed-action-btn">
        <a href="{{ route('notes.create') }}" class="btn-floating btn-large green waves-effect pulse tooltipped" data-position="left" data-tooltip="Добавить">
            <i class="large material-icons">add</i>
        </a>
    </div>

@endsection

@section('scripts')
    <script>

        $(document).ready(function(){

            $("#search").keyup(function(){
                _this = this;
            
                $.each($("#table tbody tr"), function() {
                    if($(this).text().toLowerCase().indexOf($(_this).val().toLowerCase()) === -1) {
                        $(this).hide();
                    } else {
                        $(this).show();  
                    }              
                });
            });
        });

    </script>

@endsection