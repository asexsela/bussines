@extends('layouts.admin')

@section('content')

<div class="had-container">
    <div class="row">
        <div class="col s12">
            <h5>Основные настройки сайта</h5>
            <div class="divider"></div>
        </div>
    </div>
    <div class="row">
        <div class="col s12">
            <ul class="tabs">
                <li class="tab col s2"><a class="active" href="#site">Сайт</a></li>
                <li class="tab col s2"><a href="#policy_tab">Метрика</a></li>
                {{-- <li class="tab col s2"><a href="#personal_data_tab">Обработка персональных данных</a></li>
                <li class="tab col s2"><a href="#scripts">Скрипты</a></li> --}}
            </ul>
        </div>
    </div>
    <div class="row">
        <form action="{{ route('base.save') }}" id="form-save-setting">

            @csrf

            <div class="row">
                
                <div id="site" class="col s12">
                    <div class="col s12 m6 l12 xl4">
                        <div class="body-block pa-2">
                            <div class="input-field">
                                <input id="name" type="text" name="name" value="{{ $setting->name }}">
                                <label for="name">Название сайта</label>
                            </div>
                        </div>
                    </div>
                    <div class="col s12 m6 l12 xl4">
                        <div class="body-block pa-2">
                            <p>Логотип header</p>
                            <div class="file-field input-field">
                                <a href="/filemanager/dialog.php?type=2&field_id=logo_header&relative_url=1" class="btn iframe-btn" type="button">Выбрать изображение</a>
                                <div class="file-path-wrapper">
                                    <input id="logo_header" class="file-path validate" name="logo_header" type="text" value="{{ $setting->logo_header }}">
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- <div class="col s12 m6 l12 xl4">
                        <div class="body-block pa-2">
                            <p>Логотип footer</p>
                            <div class="file-field input-field">
                                <a href="/filemanager/dialog.php?type=2&field_id=logo_footer&relative_url=1" class="btn iframe-btn" type="button">Выбрать изображение</a>
                                <div class="file-path-wrapper">
                                    <input id="logo_footer" class="file-path validate" name="logo_footer" type="text" value="{{ $setting->logo_footer }}">
                                </div>
                            </div>
                        </div>
                    </div> --}}
                    <div class="col s12 m6 l12 xl6">
                        <div class="body-block pa-2">
                            <h6>Контакты</h6>
                            <div class="input-field">
                                <textarea id="mailto" class="materialize-textarea" name="mailto">{{ $setting->mailto }}</textarea>
                                <label for="mailto">Почта(для писем, 1 адрес)</label>
                            </div>
                            <div class="input-field">
                                <textarea id="mails" class="materialize-textarea" name="mails">{{ $setting->mails }}</textarea>
                                <label for="mails">Почта(для сайта, через запятую)</label>
                            </div>
                            <div class="input-field">
                                <textarea id="phones" class="materialize-textarea" name="phones">{{ $setting->phones }}</textarea>
                                <label for="phones">Телефоны(через запятую)</label>
                            </div>
                            <div class="input-field">
                                <textarea id="address" class="materialize-textarea" name="address">{{ $setting->address }}</textarea>
                                <label for="address">Адрес</label>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col s12 m6 l12 xl6">
                        
                        <div class="body-block pa-2">
                            <h6>Соц. сети</h6>
                            <div class="input-field">
                                <input id="vk" type="text" name="vk" value="{{ $setting->vk }}">
                                <label for="vk">Вконтакте</label>
                            </div>
                            <div class="input-field">
                                <input id="fb" type="text" name="fb" value="{{ $setting->fb }}">
                                <label for="fb">Facebook</label>
                            </div>
                            <div class="input-field">
                                <input id="inst" type="text" name="inst" value="{{ $setting->inst }}">
                                <label for="inst">Instagram</label>
                            </div>
                            <div class="input-field">
                                <input id="youtube" type="text" name="youtube" value="{{ $setting->youtube }}">
                                <label for="youtube">YouTube</label>
                            </div>
                        </div>
                    </div>



                </div>
                <div id="policy_tab" class="col s12">
                    <div class="col s12 l6">
                        <div class="body-block pa-2">
                            <h6>Header</h6>
                            <textarea id="header-metrick">{!! $setting->header_metrick !!}</textarea>
                        </div>
                    </div>
                    <div class="col s12 l6">
                        <div class="body-block pa-2">
                            <h6>Footer</h6>
                            <textarea id="footer-metrick">{!! $setting->footer_metrick !!}</textarea>
                        </div>
                    </div>
                </div>
                {{-- <div id="personal_data_tab" class="col s12">
                    <div class="body-block pa-2">
                        <h6>Политика конфиденциальности</h6>
                        <div class="input-field">
                            <textarea id="policy" class="materialize-textarea" name="policy"></textarea>
                            <label for="policy">Политика конфиденциальности</label>
                        </div>
                    </div>
                </div>
                <div id="scripts" class="col s12">Test 4</div> --}}
            </div>

        </form>
    </div>
    <div class="fixed-action-btn">
        <a href="#!" id="save-button-setting" class="btn-floating btn-large blue pulse">
            <i class="large material-icons">save</i>
        </a>
    </div>
</div>
<style>
.CodeMirror {
font-family: Arial, monospace;
font-size: 13px;
}
</style>
@endsection
@section('scripts')
    <script>

      var mixedMode = {
        name: "htmlmixed",
        scriptTypes: [{
                matches: /\/x-handlebars-template|\/x-mustache/i,
                mode: null
            },
            {
                matches: /(text|application)\/(x-)?vb(a|script)/i,
                mode: "vbscript"
            }]
      };

    

      var editor = CodeMirror.fromTextArea(document.getElementById("header-metrick"), {
            mode: mixedMode,
            lineNumbers: true,
            theme: 'material',
      }).on("change",function(editor) {
            $('#header-metrick').text(editor.getValue())
      });

      var editor = CodeMirror.fromTextArea(document.getElementById("footer-metrick"), {
            mode: mixedMode,
            lineNumbers: true,
            theme: 'material',
      }).on("change",function(editor) {
            $('#footer-metrick').text(editor.getValue())
      });
    
    </script>
@endsection