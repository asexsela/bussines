@extends('layouts.admin')

@section('content')

    <div class="had-container">
        <div class="row">
            <div class="col s12">
                <h5>Новая категория</h5>
                <div class="divider"></div>
            </div>
        </div>

        <div class="row">
            <div class="col s12 m6 l12 xl6">
                <form id="form-save" action="{{ route('category.save') }}">
                    {{ csrf_field() }}
                    <div class="body-block pa-2">
                        <div class="input-field">
                            <input id="category-title" type="text" name="title" class="validate" required>
                            <label for="category-title">Заголовок*</label>
                            <span class="helper-text">Рекомендуемое количество символов: 30</span>
                        </div>
                        <div class="input-field">
                            <input id="category-slug" type="text" name="slug">
                            <label for="category-slug">URL</label>
                            <span class="helper-text">Если оставить пустым, то сгенериурется автоматически</span>
                        </div>
                        <div class="input-field select-category">
                            <select name="category_id">
                                <option selected value="0">Выберете категорию</option>
                                @foreach ($categories as $item)
                                    <option value="{{ $item->id }}">{{ $item->title }}</option>
                                @endforeach
                            </select>
                            <label>Категория</label>
                        </div>
                        <div class="input-field select-category">
                            <select name="heading_id">
                                <option selected>Выберете рубрику</option>
                                @foreach ($headings as $item)
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                @endforeach
                            </select>
                            <label>Рубрика</label>
                        </div>

                    </div>
                </form>
            </div>
        </div>



        <div class="fixed-action-btn">
            <a class="btn-floating btn-large blue pulse">
                <i class="large material-icons">more_vert</i>
            </a>
            <ul>
                <li><a id="back_parent" href="{{ route('category') }}" class="btn-floating red darken-1 tooltipped" data-position="left" data-tooltip="Отмена"><i class="material-icons">keyboard_backspace</i></a></li>
                <li><a href="#" id="save-button" class="btn-floating green tooltipped" data-position="left" data-tooltip="Сохранить"><i class="material-icons">save</i></a></li>
            </ul>
        </div>
    </div>

@endsection

@section('scripts')
  

@endsection