@extends('layouts.admin')

@section('content')
    <div class="had-container">
        <div class="row">
            <div class="col s12">
                <h5>Новое меню</h5>
                <div class="divider"></div>
            </div>
        </div>

        <div class="row">
            <div class="col s12 m6 l12 xl6">
                <form id="form-save-menu" action="{{ route('menu.save') }}">
                    {{ csrf_field() }}
                    <div class="body-block pa-2">
                        <div class="input-field">
                            <input id="menu-name" type="text" name="name" class="validate" required>
                            <label for="menu-name">Название*</label>
                            <span class="helper-text">Это название для вывода меню</span>
                        </div>


                    </div>
            </div>
            <div class="col s12 m6 l12 xl6">     

                <div id="sortable">
                    <div id="1" data-id="" class="item-menu">
                        <div class="title">
                            <div class="img">
                                <a href="/filemanager/dialog.php?type=2&field_id=img1&relative_url=1" class="change iframe-btn">+</a>
                                <input type="hidden" class="item-img" id="img1" value="">
                                <img src="{{ asset('images/photos.png') }}" alt="">
                            </div>
                            <p>Item 1</p>
                            <span class="arrow">&#8250;</span>
                            <span class="remove-item">&times;</span>
                        </div>
                        <div class="content">
                            <div class="row">
                                <div class="col s12 l6">
                                    <div class="input-field">
                                        <input class="item-name" type="text" required>
                                        <label>Название*</label>
                                    </div>
                                </div>
                                <div class="col s12 l6">
                                    <div class="input-field">
                                        <input class="item-url" type="text" required>
                                        <label>URL*</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <a class="add-item-menu btn-floating btn-small waves-effect waves-light green"><i class="material-icons">add</i></a>

            </div>
            

        </div>



        <div class="fixed-action-btn">
            <a class="btn-floating btn-large blue pulse">
                <i class="large material-icons">more_vert</i>
            </a>
            <ul>
                <li><a id="back_parent" href="{{ route('menu') }}" class="btn-floating red darken-1 tooltipped" data-position="left" data-tooltip="Отмена"><i class="material-icons">keyboard_backspace</i></a></li>
                <li><a href="#" id="save-button-menu" class="btn-floating green tooltipped" data-position="left" data-tooltip="Сохранить"><i class="material-icons">save</i></a></li>
            </ul>
        </div>
    </div>

@endsection

@section('scripts') 
    <script>
        $('.item-menu').on('mousedown', function() {
            $('.item-menu-placeholder').css('height', $(this).height())
        })
    </script>
@endsection