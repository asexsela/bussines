@extends('layouts.admin')

@section('content')

    <div class="had-container">
        <div class="row">
            <div class="col s12">
                <h5>Новая рубрика</h5>
                <div class="divider"></div>
            </div>
        </div>

        <div class="row">
            <div class="col s12 m6 l12 xl6">
                <form id="form-save" action="{{ route('heading.save') }}">
                    {{ csrf_field() }}
                    <div class="body-block pa-2">
                        <div class="input-field">
                            <input id="category-title" type="text" name="name" class="validate" required>
                            <label for="category-title">Заголовок*</label>
                            <span class="helper-text">Рекомендуемое количество символов: 30</span>
                        </div>
                        <div class="input-field">
                            <input id="category-slug" type="text" name="slug">
                            <label for="category-slug">URL</label>
                            <span class="helper-text">Если оставить пустым, то сгенериурется автоматически</span>
                        </div>
                        <div class="input-field select-category">
                            <select name="category_id">
                                <option value="0" selected>Выберете категорию</option>
                                @foreach ($categories as $item)
                                    <option value="{{ $item->id }}">{{ $item->title }}</option>
                                @endforeach
                            </select>
                            <label>Категория</label>
                        </div>

                    </div>
                </form>
            </div>
            <div class="col s12 m6 l12 xl6">  
                <div class="body-block pa-2">
                    <div class="input-field">
                        <input id="seo-title" name="seoTitle" type="text" class="validate">
                        <label for="seo-title">SEO Заголовок</label>
                    </div>
                    <div class="input-field">
                        <textarea id="meta" name="seoDesc" class="materialize-textarea"></textarea>
                        <label for="meta">SEO description </label>
                    </div>

                    <div class="input-field">
                        <textarea id="desc" name="seoKeywords" class="materialize-textarea"></textarea>
                        <label for="desc">SEO keywords</label>
                    </div>
                    <div class="input-field">
                        <p>Публикация</p>
                        <div class="switch">
                            <label>
                                Нет
                                <input type="checkbox" name="active" value="1" checked>
                                <span class="lever"></span>
                                Да
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <div class="fixed-action-btn">
            <a class="btn-floating btn-large blue pulse">
                <i class="large material-icons">more_vert</i>
            </a>
            <ul>
                <li><a id="back_parent" href="{{ route('heading') }}" class="btn-floating red darken-1 tooltipped" data-position="left" data-tooltip="Отмена"><i class="material-icons">keyboard_backspace</i></a></li>
                <li><a href="#" id="save-button" class="btn-floating green tooltipped" data-position="left" data-tooltip="Сохранить"><i class="material-icons">save</i></a></li>
            </ul>
        </div>
    </div>

@endsection

@section('scripts')
  

@endsection