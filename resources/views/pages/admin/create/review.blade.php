@extends('layouts.admin')

@section('content')
    <div class="had-container">
        <div class="row">
            <div class="col s12">
                <h5>Новый отзыв</h5>
                <div class="divider"></div>
            </div>
        </div>

        <div class="row">
            <form action="{{ route('review.save') }}" id="form-save">

                @csrf
                <input type="hidden" name="admin" value="1">
                <div class="col s12 m8 l12 xl8">
                    <div class="body-block pa-2">
                        <div class="input-field">
                            <input id="product-title" type="text" name="name" class="validate" value="">
                            <label for="product-title">Имя*</label>
                        </div>
                        <div class="input-field">
                            <p>Отзыв*</p>
                            <textarea id="body" class="materialize-textarea" name="body"></textarea>
                        </div>
                        
                    </div>
    
                </div>
                <div class="col s12 m4 l12 xl4">
                    <div class="body-block pa-2">
                        <div class="input-field">
                            <p>Публикация</p>
                            <div class="switch">
                                <label>
                                    Нет
                                    <input type="checkbox" name="active" value="1" checked>
                                    <span class="lever"></span>
                                    Да
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="body-block pa-2">
                        <p>Основное изображение</p>
                        <div class="file-field input-field">
     
                            <a href="/filemanager/dialog.php?type=2&field_id=field&relative_url=1" class="btn iframe-btn">Выбрать изображение</a>

                            <div class="file-path-wrapper">
                                <input id="field" class="file-path validate" name="img" type="text" value="/images/profile.png">
                            </div>
                        </div>
                        <p>Предпросмотр</p>
                        <div id="preview-img">
                            <img 
                                class="no-cropper"
                                width="100%" 
                                style="max-width:100%;display: block;width: auto;margin: 0 auto;"
                                src="{{ asset('/images/profile.png') }}">
                        </div> 
                    </div>
                </div>
            </form>
        </div>



        <div class="fixed-action-btn">
            <a class="btn-floating btn-large blue pulse">
                <i class="large material-icons">more_vert</i>
            </a>
            <ul>
                <li><a id="back_parent" href="{{ route('review') }}" class="btn-floating red darken-1 tooltipped" data-position="left" data-tooltip="Отмена"><i class="material-icons">keyboard_backspace</i></a></li>
                <li><a href="#!" id="save-button" class="btn-floating green tooltipped" data-position="left" data-tooltip="Сохранить"><i class="material-icons">save</i></a></li>
            </ul>
        </div>
    </div>
@endsection


@section('scripts')
    <script>
        if(document.querySelector('#body')) {

            tinymce.init({
                selector: '#body',
                plugins: '',
                menubar: '',
                toolbar: ' bold italic underline strikethrough | fontsizeselect formatselect',
                toolbar_sticky: false,
                fontsize_formats: "6px 7px 8px 9px 10px 11px 12px 13px 14px 15px 16px 17px 18px 24px 36px",
                quickbars_selection_toolbar: '',
                noneditable_noneditable_class: "mceNonEditable",
                toolbar_drawer: 'sliding',
                contextmenu: "",
                language: 'ru',
                height: 200,
                setup: function (editor) {
                    editor.on('change', function () {
                        editor.save();
                    });
                }
            });

        }
    </script>
@endsection