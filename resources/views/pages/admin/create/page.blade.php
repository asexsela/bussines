@extends('layouts.admin')

@section('content')
    <div class="had-container">
        <div class="row">
            <div class="col s12">
                <h5>Новая страница</h5>
                <div class="divider"></div>
            </div>
        </div>

        <div class="row">
            <form action="{{ route('page.save') }}" id="form-save">

                @csrf
                <div class="col s12 m8 l12 xl8">
                    <div class="body-block pa-2">
                        <div class="input-field">
                            <input id="product-title" type="text" name="title" class="validate" value="" require>
                            <label for="product-title">Заголовок*</label>
                            <span class="helper-text">Рекомендуемое количество символов: 30</span>
                        </div>
                        <div class="input-field">
                            <textarea id="description" class="materialize-textarea" name="description"></textarea>
                            <label for="description">Небольшое описание</label>
                        </div>
                        <div class="input-field">
                            <textarea id="editor" name="body"></textarea>
                        </div>
                    </div>
                    <div class="body-block pa-2">
                        <div class="input-field">
                            <input id="seo-title" type="text" class="validate" name="seoTitle"> 
                            <label for="seo-title">SEO title</label>
                        </div>
                        <div class="input-field">
                            <textarea id="meta" class="materialize-textarea" name="seoDesc"></textarea>
                            <label for="meta">SEO описание</label>
                        </div>

                        <div class="input-field">
                            <textarea id="desc" class="materialize-textarea" name="seoKeywords"></textarea>
                            <label for="desc">SEO ключевые слова</label>
                        </div>

                    </div>

                </div>
                <div class="col s12 m4 l12 xl4">
                    <div class="body-block pa-2">
                        <div class="input-field">
                            <input id="product-slug" type="text" class="validate" name="slug" value="">
                            <label for="product-slug">URL</label>
                            <span class="helper-text">Если оставить пустым, то сгенерируется автоматически</span>
                        </div>
                    </div>

                    <div class="body-block pa-2">
                        <p>Основное изображение</p>
                        <div class="file-field input-field">
     
                            <a href="/filemanager/dialog.php?type=2&field_id=field&relative_url=1" class="btn iframe-btn">Выбрать изображение</a>

                            <div class="file-path-wrapper">
                                <input id="field" class="file-path validate" name="img" type="text" value="">
                            </div>
                        </div>
                        <p>Предпросмотр</p>
                        <div id="preview-img">
                            <img 
                                class="no-cropper"
                                width="100%" 
                                style="max-width:100%;display: block;width: auto;margin: 0 auto;"
                                src="">
                        </div> 
                    </div>

                </div>
            </form>
        </div>



        <div class="fixed-action-btn">
            <a class="btn-floating btn-large blue pulse">
                <i class="large material-icons">more_vert</i>
            </a>
            <ul>
                <li><a id="back_parent" href="{{ route('page') }}" class="btn-floating red darken-1 tooltipped" data-position="left" data-tooltip="Отмена"><i class="material-icons">keyboard_backspace</i></a></li>
                <li><a href="#!" id="save-button" class="btn-floating green tooltipped" data-position="left" data-tooltip="Сохранить"><i class="material-icons">save</i></a></li>
            </ul>
        </div>
    </div>
@endsection

@section('scripts')
    <script>

        $('.chips').chips({
            placeholder: 'Теги',
            secondaryPlaceholder: '+Тег',
        });

        

    </script>

@endsection