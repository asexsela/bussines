@extends('layouts.admin')

@section('content')

    <div class="had-container">
        <div class="row">
            <div class="col s12">
                <h5>Реклама</h5>
                <div class="divider"></div>
            </div>
        </div>
        <div class="row">
            <div class="col s6">

            </div>
            <div class="col s12 l3 offset-l3">
                <div class="search-block">
                    <div class="input-field">
                        <input id="search" type="text" class="validate">
                        <label for="search">Поиск</label>
                    </div>
                    <div class="result"></div>
                </div>
            </div>
        </div>
        @if ($banners->count() <= 0)
            <div class="row">
                <p style="
                text-align: center;
                font-size: 20px;
                color: red;
            ">Реклама не найдена</p>
            </div>
        @else
        <div class="row">
            
            <div class="col s12">
                <table class="responsive-table highlight" id="table">
                    <thead>
                        <tr>
                            <th>id</th>
                            <th>Просмотры\Переходы</th>
                            <th class="image">Изображение</th>
                            <th>Заголовок</th>
                            <th>URL</th>
                            <th>Обновление</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach ($banners as $item)

                            <tr>
                                <td>{{ $item->id }}</td>
                                <td style="width: 30px;">
                                    <p style="display: flex;align-items: center;justify-content: center;color: #c5c5c5;font-weight: bold;">
                                        <i style="color: #c5c5c5;" class="material-icons">remove_red_eye</i>
                                        {{ $item->views->count() }}
                                    </p>
                                    <p style="display: flex;align-items: center;justify-content: center;color: #c5c5c5;font-weight: bold;">
                                        <i style="color: #c5c5c5;" class="material-icons">forward</i>
                                        {{ $item->clicks->count() }}
                                    </p>
                                </td>
                                <td><img class="materialboxed" src="{{ asset($item->img_small) }}" alt=""></td>
                                <td>{{ $item->title }}</td>
                                <td>{{ $item->title }}</td>
                                <td>
                                    {{ Date::parse($item->created_at)->format('j F Y, H:i ') }} ({{ Date::parse($item->updated_at)->ago() }})
                                    <div class="control">
                                        <a href="{{ route('banner.edit', ['banner' => $item->id]) }}" class="edit tooltipped" data-position="top" data-tooltip="Редактировать"><i class="material-icons">edit</i></a>
                                        <a href="{{ route('banner.destroy', ['banner' => $item->id]) }}" class="delete tooltipped" data-position="top" data-tooltip="Удалить"><i class="material-icons">delete</i></a>
                                    </div>
                                </td>
                            </tr>

                        @endforeach
                        
                    </tbody>
                </table>
            </div>
            <div class="col s12">
                {{ $banners->links('components.admin.pagination') }}
            </div>
        </div>
        @endif
        
    </div>
    <div class="fixed-action-btn">
        <a href="{{ route('banner.create') }}" class="btn-floating btn-large green waves-effect pulse tooltipped" data-position="left" data-tooltip="Добавить">
            <i class="large material-icons">add</i>
        </a>
    </div>

@endsection

@section('scripts')
    <script>

        $(document).ready(function(){

            $("#search").keyup(function(){
                let count = $(this).val().length
                    text = $(this).val(),
                    token = $('[name="csrf-token"]').attr('content');

                $.ajax({
                    type: 'post',
                    url: "{{ route('banner.search') }}",
                    headers: {'X-CSRF-TOKEN': token},
                    data: {
                        string: text,
                    },
                    success: function(data) {
                        $('.result').html('');

                        if(count > 0 && data.length > 0) {
                            $('.result').fadeIn().css('display', 'flex');
                            data.forEach(e => {
                                $('.result').append(`
                                    <a href="/admin/banner/${e.id}">${e.title}</a>
                                `)
                            });
                        } else {
                            $('.result').html('');
                            $('.result').fadeOut();

                        }
                    },
                    error: function(err) {
                        console.log(err)
                    }
                });

            });
        });

    </script>

@endsection