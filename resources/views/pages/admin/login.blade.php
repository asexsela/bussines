@extends('layouts.admin')

@section('content')
    <div id="admin-login">
        <div class="preview">
            <img class="animated fadeIn slow" src="{{ asset('adm/images/dev-logo.png') }}" alt="">
            <h1 class="animated fadeInUp slow">Лаборатория новых технологий</h1>
            <p class="animated fadeInUp slow">Добро пожаловать в админ панель сайта {{ $site_name }}</p>
        </div>
        <div class="login">
            <form id="login" class="admin-form" method="POST" action="{{ route('login') }}">
                @csrf
                <h3>Вход</h3>
                <div class="row">
                    <div class="input-field col s12">
                    <input id="email" type="email" class="validate" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                    <label for="email">Почта</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                    <input id="password" type="password" class="validate" name="password" required autocomplete="current-password">
                    <label for="password">Пароль</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col s6">
                        {{-- <a class="forgot" href="/forgot">Забыли пароль?</a> --}}
                    </div>
                    <div class="col s6">
                        <button type="submit" class="waves-effect waves-light btn right">Вход</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        let audio = 'http://www.orangefreesounds.com/wp-content/uploads/2020/01/Free-ambient-background-music.mp3',
            play = new Audio(audio);
            play.play();
            play.volume = 0.2;

    </script>
@endsection