@extends('layouts.base')

@section('seo')
    <title>Авторы | Вологодские новости</title>
@endsection

@section('content')

    <section class="headline">
        <div class="container bc">
            <div class="breadcrumbs">
                <a href="/">Главная</a>
                <span>Авторы</span>
            </div>
            <h1>Авторы</h1>
        </div>
    </section>
    
    <section class="authors">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-8 offset-lg-1">
                   <div class="row">
                       @foreach ($authors as $key => $author)
                        @if ($key === 5)
                            <section class="sec4 mob">
                                <div class="container">
                                    @php
                                        $banner = $banners->whereNotNull('img_small')->random(1)[0];
                                    @endphp
                                    <a href="{{ $banner->url }}" target="blank" banner-small data-id="{{ $banner->id }}">
                                        <img src="{{ asset($banner->img_small) }}" alt="">
                                    </a>
                                </div>
                            </section>
                        @endif   
                        <div class="col-12 col-sm-6">
                            <div class="author_item">
                                <img src="{{ asset($author->img) }}" alt="">
                                <div class="body">
                                    <div class="desc">
                                        <h3>{{ $author->title }}</h3>
                                        <p>{{ $author->description }}</p>
                                    </div>
                                    <a href="{{ route('page.author_blogs', ['author' => $author->id]) }}" class="more_news">В блог автора</a>
                                </div>
                            </div>
                        </div>
                       @endforeach

                   </div>
                </div>
                <div class="col-3 d-none d-lg-block">
                    <div class="box-ad">
                        @php
                            $count = ($count_banner < 3) ? $count_banner : 3;
                        @endphp
                        @foreach ($banners->whereNotNull('img_small')->random($count) as $item)
                            <a href="{{ $item->url }}" target="blank" banner-small data-id="{{ $item->id }}">
                                <img src="{{ asset($item->img_small) }}" alt="">
                            </a>
                        @endforeach
                    </div>
                </div>
                <div class="col-12 mb-5 mt-3">
                    {{ $authors->links() }}
                </div>
            </div>
        </div>
    </section>

   
    
    <section class="sec5">
        <div class="container">
            <div class="row">
                <div class="col-6">
                    <div class="posts">
                        <div class="head">
                            <p><img src="{{ asset('images/new-post.svg') }}" alt="">Самые популярные посты</p>
                        </div>
                        <div class="posts-tables">

                            @foreach ($best_posts as $item)
                                
                                <div class="post">
                                    <img src="{{ asset($item->img) }}" alt="">
                                    <div class="body">
                                        <a href="{{ route('page.article', ['article' => $item->id]) }}"><h3>{{ $item->title }}</h3></a>
                                        <div class="footer">
                                            <p>
                                                <span>{{ Date::parse($item->created_at)->format('j F Y, H:i ') }}</span>
                                                <span class="author">{{ $item->author_name }}</span>
                                            </p>
                                        </div>
                                    </div>
                                </div>

                            @endforeach

                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="posts">
                        <div class="head">
                            <p><img src="{{ asset('images/best_authors.svg') }}" alt="">Самые читаемые авторы</p>
                        </div>
                        <div class="posts-tables">
                            @foreach ($best_authors as $item)
                                
                                <div class="post">
                                    <img src="{{ asset($item->img) }}" alt="">
                                    <div class="body">
                                        <a href="{{ route('page.author_blogs', ['author' => $item->id]) }}"><h3>{{ $item->title }}</h3></a>
                                        <div class="footer">
                                            <p>
                                                <span>{{ Date::parse($item->created_at)->format('j F Y, H:i ') }}</span>
                                                <span class="author">{{ $item->title }}</span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="space"></div>

@endsection