@extends('layouts.base')

@section('seo')
    <title>Рубрики | Вологодские новости</title>
@endsection

@section('content')
    <section class="sec1">
        <div class="container">
            @php
                $banner = $banners->whereNotNull('img_big')->random(1)[0];
            @endphp
            <a href="{{ $banner->url }}" target="blank" banner-big data-id="{{ $banner->id }}">
                <img src="{{ asset($banner->img_big) }}" alt="">
            </a>
        </div>
    </section>
    <section class="headline">
        <div class="container">
            <h1>Рубрики</h1>
        </div>
    </section>

    <section class="headings">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-9">
                    <div class="row">
                        
                        @foreach ($headings as $heading)
                            @if ($heading->category_id === 0 )
                                <div class="col-6 col-md-4"><a class="btn1" href="{{ route('page.heading', ['slug' => $heading->slug]) }}">{{ $heading->name }}</a></div>
                            @endif
                        @endforeach

                    </div>
                    @foreach ($categories as $category)
                        @if ($category->headings->count())
                            <div class="row">
                                <div class="col-12">
                                    <h2>{{ $category->title }}</h2>
                                </div>
                                @foreach ($category->headings as $heading)
                                    <div class="col-6 col-md-4"><a class="btn1" href="{{ route('page.heading', ['slug' => $heading->slug]) }}">{{ $heading->name }}</a></div>
                                @endforeach
                            </div>
                        @endif
                    @endforeach
                </div>
                <section class="sec4 mob">
                    <div class="container">
                        @php
                            $banner = $banners->whereNotNull('img_small')->random(1)[0];
                        @endphp
                        <a href="{{ $banner->url }}" target="blank" banner-small data-id="{{ $banner->id }}">
                            <img src="{{ asset($banner->img_small) }}" alt="">
                        </a>
                    </div>
                </section>
                <div class="col-3 d-none d-lg-block">
                    <div class="box-ad">
                        @php
                            $count = ($count_banner < 6) ? $count_banner : 6;
                        @endphp
                        @foreach ($banners->whereNotNull('img_small')->random($count) as $item)
                            <a href="{{ $item->url }}" target="blank" banner-small data-id="{{ $item->id }}">
                                <img src="{{ asset($item->img_small) }}" alt="">
                            </a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>

    @include('components.slider_arhives')
    <div class="space"></div>
@endsection