@extends('layouts.base')

@section('seo')
    <title>Отзывы | Вологодские новости</title>
@endsection

@section('content')
<div class="space"></div>

    <section class="headline">
        <div class="container bc">
            <div class="breadcrumbs">
                <a href="/">Главная</a>
                <span>Отзывы</span>
            </div>
            {{-- <h1></h1> --}}
        </div>
    </section>
    
    <section class="reviews pt-4">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-9">
                    <section class="headline">
                        <h1>Отзывы</h1>
                    </section>
                    <div class="row">
                        <div class="col-12 col-lg-9">
    
                            <form id="reviews" method="post" action="{{ route('review.save') }}">
                                <h2>Оставить отзыв</h2>
                                @csrf
                                <div class="input">
                                    <div class="input-form">
                                        <label>Имя*</label>
                                        <input type="text" name="firstName" required>
                                    </div>
                                    <div class="input-form">
                                        <label>Фамилия*</label>
                                        <input type="text" name="secondName" required>
                                    </div>
                                </div>
                                <div class="body-form">
                                    <label>Ваш отзыв</label>
                                    <textarea name="body" id="editor" required></textarea>
                                </div>
                                <button type="submit" class="more_news">Отправить</button>
        
                            </form>
                            @if(session()->has('message'))
                                <div class="alert alert-success">
                                    {{ session()->get('message') }}
                                </div>
                            @endif
                            @if(session()->has('error'))
                                <div class="alert alert-danger">
                                    {{ session()->get('error') }}
                                </div>
                            @endif
                        </div>
                        <div class="col-12">                   
    
                            <div class="box_reviews">
                                @foreach ($reviews as $item)
                                    
                                    <div class="item_review">
                                        @if(file_exists(public_path($item->img)))
                                            <img src="{{ asset($item->img) }}" alt="{{ $item->name }}">
                                        @else
                                            <img src="{{ asset('images/profile.png') }}" alt="{{ $item->name }}">
                                        @endif
                                        <div class="body">
                                            <h3>{{ $item->name }}</h3>
                                            <span>{{ Date::parse($item->created_at)->format('d.m.Y') }}</span>
                                            <div class="text">{!! $item->body !!}</div>
                                        </div>
                                        <a href="#" class="more">+</a>
                                    </div>
        
                                @endforeach
                            </div>
        
                        </div>
                    </div>
                </div>
                <div class="col-3 d-none d-lg-block">
                    <div class="box-ad">
                        @php
                            $count = ($count_banner < 6) ? $count_banner : 6;
                        @endphp
                        @foreach ($banners->whereNotNull('img_small')->random($count) as $item)
                            <a href="{{ $item->url }}" target="blank" banner-small data-id="{{ $item->id }}">
                                <img src="{{ asset($item->img_small) }}" alt="">
                            </a>
                        @endforeach
                    </div>
                </div>
            </div>

  

        </div>
        @if ($reviews->count() <= 0)    
            <div class="container">
                <p class="empty">Отзывов не найдено</p>
            </div>
        @endif
        <div class="container">

            <div class="row">
                
                
            </div>

        </div>
    </section>

    <section class="pagination">
        <div class="container">
            {{ $reviews->links() }}
        </div>
    </section>
    @include('components.slider_arhives')
    <div class="space"></div>

@endsection

@section('scripts')
    <script>
        if(document.querySelector('#editor')) {

            tinymce.init({
                selector: '#editor',
                plugins: '',
                menubar: '',
                toolbar: ' bold italic underline strikethrough | fontsizeselect formatselect',
                toolbar_sticky: false,
                fontsize_formats: "6px 7px 8px 9px 10px 11px 12px 13px 14px 15px 16px 17px 18px 24px 36px",
                quickbars_selection_toolbar: '',
                noneditable_noneditable_class: "mceNonEditable",
                toolbar_drawer: 'sliding',
                contextmenu: "",
                language: 'ru',
                height: 200,
                setup: function (editor) {
                    editor.on('change', function () {
                        editor.save();
                    });
                }
            });

        }
    </script>
@endsection