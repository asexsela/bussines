@extends('layouts.base')

@section('seo')
    <title>Новости | Вологодские новости</title>
@endsection

@section('content')

    <section class="sec1">
        <div class="container">
            @php
                $banner = $banners->whereNotNull('img_big')->random(1)[0];
            @endphp
            <a href="{{ $banner->url }}" target="blank" banner-big data-id="{{ $banner->id }}">
                <img src="{{ asset($banner->img_big) }}" alt="">
            </a>
        </div>
    </section>
    <section class="headline">
        <div class="container">
            <h1>Новости</h1>
        </div>
    </section>
    <section class="controls-news">
        <div class="container">
            <div class="box-control">
                <a class="btn1 @if($type == 'all') active @endif" href="{{ route('page.news') }}">За всё время</a>
                <a class="btn1 @if($type == 'week') active @endif" href="{{ route('page.news', ['type' => 'week']) }}">За неделю</a>
                <a class="btn1 @if($type == 'month') active @endif" href="{{ route('page.news', ['type' => 'month']) }}">За месяц</a>
                <a class="date @if($type == 'date') active @endif" href="#!">
                    <img src="{{ asset('images/date.svg') }}" alt="">
                    <input type="text" class="date_picker" placeholder="Выбрать дату" data-url="{{ route('page.news', ['type' => 'date']) }}"></span>
                </a>
                
            </div>
        </div>
    </section>

    <section class="box_news">
        @if ($news->count() <= 0)    
            <div class="container">
                <p class="empty">Новостей не найдено</p>
            </div>
        @endif
        <div class="container">
            <div class="row">
                @foreach ($news as $key => $item)
                    @if($key >= 2) 
                        @continue;
                    @endif
                    <div class="col-12 col-md-6">
                        <div class="news">
                            <a href="{{ route('page.new', ['news' => $item->id]) }}">
                               
                                @if ($item->img)
                                    <div class="img big" 
                                        data-bigstyle="{{ $item->big_style }}"  
                                        data-smallstyle="{{ $item->small_style }}">
                                        <span class="label">{{ Date::parse($item->published_at)->format('d.m.Y') }}</span>
                                        <img class="news_image" src="{{ asset($item->img) }}" alt="">
                                    </div>
                                @else
                                    <div class="img">
                                        <span class="label">{{ Date::parse($item->published_at)->format('d.m.Y') }}</span>
                                        <img class="news_image" src="{{ asset('/images/photos.png') }}" alt="">
                                    </div>
                                @endif
                                <h3 class="news-title">{{ $item->title }}</h3>
                                <p class="news-desc">{{ $item->description }}</p>
                            </a>
                            <div class="footer_news">
                                <span class="data">{{ Date::parse($item->published_at)->format('j F Y') }}</span>
                                <div class="control">
                                    <a href="#">
                                        <img src="{{ asset('images/eye.svg') }}" alt="">
                                        <span>{{ $item->views->count() }}</span>
                                    </a>
                                    <a href="#" class="like" data-id="{{ $item->id }}" data-type="news">
                                        <img src="{{ asset('images/like.svg') }}" alt="">
                                        <span>{{ $item->likes->count() }}</span>
                                    </a>
                                    <a href="#" class="dislike" data-id="{{ $item->id }}" data-type="news">
                                        <img src="{{ asset('images/dislike.svg') }}" alt="">
                                        <span>{{ $item->dislikes->count() }}</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach

            </div>
            <section class="sec4 mob">
                <div class="container">
                    @php
                        $banner = $banners->whereNotNull('img_small')->random(1)[0];
                    @endphp
                    <a href="{{ $banner->url }}" tagret="blank" banner-small data-id="{{ $banner->id }}">
                        <img src="{{ asset($banner->img_small) }}" alt="">
                    </a>
                </div>
            </section>
            <div class="row">
                <div class="col-12 col-lg-9">
                    <div class="row">
                        @foreach ($news as $key => $item)
                            @if ($key === 2 || $key === 5)
                                <div class="col-12 col-md-8">
                                    <div class="news">
                                        <a href="{{ route('page.new', ['news' => $item->id]) }}">
                                            @if ($item->img)
                                                <div class="img big"  
                                                    data-bigstyle="{{ $item->big_style }}"  
                                                    data-smallstyle="{{ $item->small_style }}">
                                                    <span class="label">{{ Date::parse($item->published_at)->format('d.m.Y') }}</span>
                                                    <img class="news_image" src="{{ asset($item->img) }}" alt="">
                                                </div>
                                            @else
                                                <div class="img">
                                                    <span class="label">{{ Date::parse($item->published_at)->format('d.m.Y') }}</span>
                                                    <img class="news_image" src="{{ asset('/images/photos.png') }}" alt="">
                                                </div>
                                            @endif
                                            <h3 class="news-title">{{ $item->title }}</h3>
                                            <p class="news-desc">{{ $item->description }}</p>
                                        </a>
                                        <div class="footer_news">
                                            <span class="data">{{ Date::parse($item->published_at)->format('j F Y') }}</span>
                                            <div class="control">
                                                <a href="#">
                                                    <img src="{{ asset('images/eye.svg') }}" alt="">
                                                    <span>{{ $item->views->count() }}</span>
                                                </a>
                                                <a href="#" class="like" data-id="{{ $item->id }}" data-type="news">
                                                    <img src="{{ asset('images/like.svg') }}" alt="">
                                                    <span>{{ $item->likes->count() }}</span>
                                                </a>
                                                <a href="#" class="dislike" data-id="{{ $item->id }}" data-type="news">
                                                    <img src="{{ asset('images/dislike.svg') }}" alt="">
                                                    <span>{{ $item->dislikes->count() }}</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @elseif($key === 3 || $key === 4)
                                <div class="col-12 col-md-4">
                                    <div class="news">
                                        <a href="{{ route('page.new', ['news' => $item->id]) }}">
                                            @if ($item->img)
                                                <div class="img small"  
                                                    data-bigstyle="{{ $item->big_style }}"  
                                                    data-smallstyle="{{ $item->small_style }}">
                                                    <span class="label">{{ Date::parse($item->published_at)->format('d.m.Y') }}</span>
                                                    <img class="news_image" src="{{ asset($item->img) }}" alt="">
                                                </div>
                                            @else
                                                <div class="img">
                                                    <span class="label">{{ Date::parse($item->published_at)->format('d.m.Y') }}</span>
                                                    <img class="news_image" src="{{ asset('/images/photos.png') }}" alt="">
                                                </div>
                                            @endif
                                            <h3 class="news-title">{{ $item->title }}</h3>
                                            <p class="news-desc">{{ $item->description }}</p>
                                        </a>
                                        <div class="footer_news">
                                            <span class="data">{{ Date::parse($item->published_at)->format('j F Y') }}</span>
                                            <div class="control">
                                                <a href="#">
                                                    <img src="{{ asset('images/eye.svg') }}" alt="">
                                                    <span>{{ $item->views->count() }}</span>
                                                </a>
                                                <a href="#" class="like" data-id="{{ $item->id }}" data-type="news">
                                                    <img src="{{ asset('images/like.svg') }}" alt="">
                                                    <span>{{ $item->likes->count() }}</span>
                                                </a>
                                                <a href="#" class="dislike" data-id="{{ $item->id }}" data-type="news">
                                                    <img src="{{ asset('images/dislike.svg') }}" alt="">
                                                    <span>{{ $item->dislikes->count() }}</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    </div>
    
                </div>
                <div class="col-3 d-none d-lg-block">
                    @if ($news->count() > 2)    
                        <div class="box-ad">
                            @php
                                $count = ($count_banner < 3) ? $count_banner : 3;
                            @endphp
                            @foreach ($banners->whereNotNull('img_small')->random($count) as $item)
                                @if ($news->count() < 4)
                                    @break
                                @endif
                                <a href="{{ $item->url }}" target="blank" banner-small data-id="{{ $item->id }}">
                                    <img src="{{ asset($item->img_small) }}" alt="">
                                </a>
                            @endforeach
                        </div>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-3 d-none d-lg-block">
                    @if (count($news) > 5) 
                        <div class="box_top_news">
                            <div class="head">
                                <p><img src="{{ asset('images/partner.svg') }}" alt="">Новости партнёров</p>
                            </div>
                            <div class="news_lists">

                                @foreach ($partner_news as $item)
                                
                                    <div class="news">
                                        <a href="{{ route('page.new', ['news' => $item->id]) }}">
                                            @if ($item->img)
                                                <div class="img">
                                                    <span class="label">{{ Date::parse($item->published_at)->format('d.m.Y') }}</span>
                                                    <img class="news_image" src="{{ asset($item->img) }}" alt="">
                                                </div>
                                            @else
                                                <div class="img">
                                                    <span class="label">{{ Date::parse($item->published_at)->format('d.m.Y') }}</span>
                                                    <img class="news_image" src="{{ asset('/images/photos.png') }}" alt="">
                                                </div>
                                            @endif
                                            <h3 class="news-title">{{ $item->title }}</h3>
                                            <p class="news-desc">{{ $item->description }}</p>
                                        </a>
                                    </div>

                                @endforeach

                            </div>
                        </div>  
                    @endif
                </div>
                <div class="col-12 col-lg-9">
                    <div class="row">
                        @foreach ($news as $key => $item)
                            @if ($key === 6 || $key === 9 || $key === 10)
                                <div class="col-12 col-md-8">
                                    <div class="news">
                                        <a href="{{ route('page.new', ['news' => $item->id]) }}">
                                            @if ($item->img)
                                                <div class="img big"  data-bigstyle="{{ $item->big_style }}"  data-smallstyle="{{ $item->small_style }}">
                                                    <span class="label">{{ Date::parse($item->published_at)->format('d.m.Y') }}</span>
                                                    <img class="news_image" src="{{ asset($item->img) }}" alt="">
                                                </div>
                                            @else
                                                <div class="img">
                                                    <span class="label">{{ Date::parse($item->published_at)->format('d.m.Y') }}</span>
                                                    <img class="news_image" src="{{ asset('/images/photos.png') }}" alt="">
                                                </div>
                                            @endif
                                            <h3 class="news-title">{{ $item->title }}</h3>
                                            <p class="news-desc">{{ $item->description }}</p>
                                        </a>
                                        <div class="footer_news">
                                            <span class="data">{{ Date::parse($item->published_at)->format('j F Y') }}</span>
                                            <div class="control">
                                                <a href="#">
                                                    <img src="{{ asset('images/eye.svg') }}" alt="">
                                                    <span>{{ $item->views->count() }}</span>
                                                </a>
                                                <a href="#" class="like" data-id="{{ $item->id }}" data-type="news">
                                                    <img src="{{ asset('images/like.svg') }}" alt="">
                                                    <span>{{ $item->likes->count() }}</span>
                                                </a>
                                                <a href="#" class="dislike" data-id="{{ $item->id }}" data-type="news">
                                                    <img src="{{ asset('images/dislike.svg') }}" alt="">
                                                    <span>{{ $item->dislikes->count() }}</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @elseif($key === 7 || $key === 8 || $key === 11)
                                <div class="col-12 col-md-4">
                                    <div class="news">
                                        <a href="{{ route('page.new', ['news' => $item->id]) }}">
                                            @if ($item->img)
                                                <div class="img small" data-bigstyle="{{ $item->big_style }}"  data-smallstyle="{{ $item->small_style }}">
                                                    <span class="label">{{ Date::parse($item->published_at)->format('d.m.Y') }}</span>
                                                    <img class="news_image" src="{{ asset($item->img) }}" alt="">
                                                </div>
                                            @else
                                                <div class="img">
                                                    <span class="label">{{ Date::parse($item->published_at)->format('d.m.Y') }}</span>
                                                    <img class="news_image" src="{{ asset('/images/photos.png') }}" alt="">
                                                </div>
                                            @endif
                                            <h3 class="news-title">{{ $item->title }}</h3>
                                            <p class="news-desc">{{ $item->description }}</p>
                                        </a>
                                        <div class="footer_news">
                                            <span class="data">{{ Date::parse($item->published_at)->format('j F Y') }}</span>
                                            <div class="control">
                                                <a href="#">
                                                    <img src="{{ asset('images/eye.svg') }}" alt="">
                                                    <span>{{ $item->views->count() }}</span>
                                                </a>
                                                <a href="#" class="like" data-id="{{ $item->id }}" data-type="news">
                                                    <img src="{{ asset('images/like.svg') }}" alt="">
                                                    <span>{{ $item->likes->count() }}</span>
                                                </a>
                                                <a href="#" class="dislike" data-id="{{ $item->id }}" data-type="news">
                                                    <img src="{{ asset('images/dislike.svg') }}" alt="">
                                                    <span>{{ $item->dislikes->count() }}</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="section pagination">
        <div class="container">
            {{ $news->links() }}
        </div>
    </div>
    @include('components.slider_arhives')
    <div class="space"></div>
@endsection