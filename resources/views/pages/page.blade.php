@extends('layouts.base')

@section('seo')
    @if ($page->seoTitle)
        <title>{{ $page->seoTitle }} | Вологодские новости</title>
        <meta property="og:title" content="{{ $page->seoTitle }} | Вологодские новости" />
    @else
        <title>{{ $page->title }} | Вологодские новости</title>
        <meta property="og:title" content="{{ $page->title }} | Вологодские новости" />
    @endif
    @if ($page->seoDesc)
        <meta name="description" content="{{ $page->seoDesc }}" />
        <meta property="og:description" content="{{ $page->seoDesc }}" />
    @endif
    @if ($page->seoKeywords)
        <meta name="keywords" content="{{ $page->seoKeywords }}">
    @endif
    @if ($page->img)
        <meta property="og:image" content="{{ asset($page->img) }}"/>
        <link rel="image_src" href="{{ asset($page->img) }}" />
    @else
        <meta property="og:image" content="{{ asset('images/site_logo.png') }}"/>
        <link rel="image_src" href="{{ asset('images/site_logo.png') }}" />
    @endif
@endsection

@section('content')
    <div class="space"></div>
    <section class="headline">
        <div class="container bc">
            <div class="breadcrumbs">
                <a href="/">Главная</a>
                {{-- <span>{{ $page->title }}</span> --}}
            </div>
        </div>
    </section>
    
    <section class="page">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-9">
                    <section class="headline">
                        <h1>{{ $page->title }}</h1>
                    </section>
                    {!! $page->body !!}

                    @if ($menu)
                        <div class="other_news d-none d-md-block">
                            <div class="head">
                                <p><img src="{{ asset('images/other_news.svg') }}" alt="">Меню</p>
                            </div>
                            <div class="row">
                                @foreach ($menu as $item)    
                                    @if ($item->name !== $page->title)
                                        
                                        <div class="col">
                                            <div class="news">
                                                <a href="{{ url($item->url) }}">
                                                    @if ($item->img)
                                                        <img class="news_image" src="{{ asset($item->img) }}" alt="">
                                                    @else
                                                        <img class="news_image" src="{{ asset('/images/photos.png') }}" alt="">
                                                    @endif
                                                    <h3 class="news-title">{{ $item->name }}</h3>
                                                </a>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                                
                            </div>
                        </div>

                        <div class="outher_news d-block d-md-none">
                            <div class="row mt-5">
                                <div class="col-12">
                                    <div class="head">
                                        <p>
                                            Меню
                                        </p>
                                    </div>
                                </div>
        
                                <div class="box_news">
                                    @foreach ($menu as $item)    
                                        @if ($item->name !== $page->title)
                                            <div class="col">
                                                <div class="news">
                                                    <a href="{{ url($item->url) }}">
                                                        @if ($item->img)
                                                            <img class="news_image" src="{{ asset($item->img) }}" alt="">
                                                        @else
                                                            <img class="news_image" src="{{ asset('/images/photos.png') }}" alt="">
                                                        @endif
                                                        <h3 class="news-title">{{ $item->name }}</h3>
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
        
                            </div>
                        </div>
                    @else

                        <div class="other_news d-none d-md-block">
                            <div class="head">
                                <p><img src="{{ asset('images/other_news.svg') }}" alt="">Новости</p>
                            </div>
                            <div class="row">
                                @foreach ($page_outher_news as $item)    
                                    <div class="col">
                                        <div class="news">
                                            <a href="{{ route('page.new', ['news' => $item->id]) }}">
                                                <span class="label">{{ Date::parse($item->created_at)->format('d.m.Y') }}</span>
                                                @if ($item->img)
                                                    <img class="news_image" src="{{ asset($item->img) }}" alt="">
                                                @else
                                                    <img class="news_image" src="{{ asset('/images/photos.png') }}" alt="">
                                                @endif
                                                <h3 class="news-title">{{ $item->title }}</h3>
                                                <p class="news-desc">{{ $item->description }}</p>
                                            </a>
                                        </div>
                                    </div>
                                @endforeach
                                
                            </div>
                        </div>

                        <div class="outher_news d-block d-md-none">
                            <div class="row mt-5">
                                <div class="col-12">
                                    <div class="head">
                                        <p>
                                            Новости
                                        </p>
                                    </div>
                                </div>
        
                                <div class="box_news">
                                    @foreach ($page_outher_news as $item)    
                                        <div class="news">
                                            <a href="{{ route('page.new', ['news' => $item->id]) }}">
                                                <span class="label">{{ Date::parse($item->created_at)->format('d.m.Y') }}</span>
                                                @if ($item->img)
                                                    <img class="news_image" src="{{ asset($item->img) }}" alt="">
                                                @else
                                                    <img class="news_image" src="{{ asset('/images/photos.png') }}" alt="">
                                                @endif
                                                <h3 class="news-title">{{ $item->title }}</h3>
                                                <p class="news-desc">{{ $item->description }}</p>
                                            </a>
                                        </div>
                                    @endforeach
                                </div>
        
                            </div>
                        </div>
                    @endif


                </div>
                <div class="col-3 d-none d-lg-block">
                    <div class="box-ad">
                        @php
                            $count = ($count_banner < 6) ? $count_banner : 6;
                        @endphp
                        @foreach ($banners->whereNotNull('img_small')->random($count) as $item)
                            <a href="{{ $item->url }}" tagret="blank" banner-small data-id="{{ $item->id }}">
                                <img src="{{ asset($item->img_small) }}" alt="">
                            </a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection