@extends('layouts.base')

@section('seo')
    <title>Партнеры | Вологодские новости</title>
@endsection

@section('content')
    <section class="sec1">
        <div class="container">
            @php
                $banner = $banners->whereNotNull('img_big')->random(1)[0];
            @endphp
            <a href="{{ $banner->url }}" target="blank" banner-big data-id="{{ $banner->id }}">
                <img src="{{ asset($banner->img_big) }}" alt="">
            </a>
        </div>
    </section>
    <section class="headline">
        <div class="container">
            <h1>Партнеры</h1>
        </div>
    </section>
    
    <section class="partners">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="buttons">
                        @foreach ($categories as $item)
                            <a href="{{ route('page.partners', ['type' => $item->title]) }}" class="more_news @if($item->title === $type->title) active @endif">{{ $item->title }}</a>
                        @endforeach
                    </div>
                </div>
                <div class="col-12 col-lg-10 offset-lg-1 mt-4">
                    <div class="row">
                        @if ($partners->count() <= 0)    
                            <div class="container">
                                <p class="empty">Партнеров не найдено</p>
                            </div>
                        @endif
                        @foreach ($partners as $key => $item)
                            @if ($key === 3)
                            <section class="sec4 mob">
                                <div class="container">
                                    @php
                                        $banner = $banners->whereNotNull('img_small')->random(1)[0];
                                    @endphp
                                    <a href="{{ $banner->url }}" target="blank" banner-small data-id="{{ $banner->id }}">
                                        <img src="{{ asset($banner->img_small) }}" alt="">
                                    </a>
                                </div>
                            </section>
                            @endif
                            <div class="col-12 col-lg-6">
                                <div class="partner_item">
                                    <a target="blank" href="{{ $item->url }}">
                                        <img src="{{ asset($item->img) }}" alt="">
                                        <div class="body">
                                            <div class="desc">
                                                <h3>{{ $item->title }}</h3>
                                                <p>{{ $item->description }}</p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>


                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="space"></div>
    
    <section class="pagination">
        <div class="container">
            {{ $partners->links() }}
        </div>
    </section>


    <div class="space"></div>

@endsection