@extends('layouts.base')

@section('seo')
    <title>Архив номеров | Вологодские новости</title>
    {{-- <title>{{ json_decode($setting->seo)->home->title }}</title>
    <meta name="description" content="{{ json_decode($setting->seo)->home->description }}" />
    <meta name="keywords" content="{{ json_decode($setting->seo)->home->keywords }}">
    <meta property="og:title" content="{{ json_decode($setting->seo)->home->title }}" />
    <meta property="og:description" content="{{ json_decode($setting->seo)->home->description }}" /> --}}
@endsection

@section('content')
    <section class="sec1">
        <div class="container">
            @php
                $banner = $banners->whereNotNull('img_big')->random(1)[0];
                @endphp
            <a href="{{ $banner->url }}" target="blank" banner-big data-id="{{ $banner->id }}">
                <img src="{{ asset($banner->img_big) }}" alt="">
            </a>
        </div>
    </section>
    
    <section class="arhives_news pt-4">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <section class="headline">
                        <h1>Архив номеров</h1>
                    </section>
                    <div class="slider-year only">
                        <div class="sliders-years-box">
                            @foreach ($archives as $item)
                                <a class="year @if($item->title === $archive->title) active @endif" href="{{ route('page.arhives_news', ['type' => $item->title]) }}" role="tab">{{ $item->title }}</a>
                            @endforeach
                        </div>
                        <a href="" class="prev"><</a>
                        <a href="" class="next">></a>
                    </div>
                </div>
            </div>
            <div class="row">
                @foreach ($numbers as $key => $item)    
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                        <div class="jornal">
                            @if ($item->img)
                                <img src="{{ asset($item->img) }}" alt="">
                            @else
                                <img src="{{ asset('/images/photos.png') }}" alt="">
                            @endif
                            <div class="body">
                                <div>
                                    <h3>{{ $item->title }}</h3>
                                    <span>{{ $item->description }}</span>
                                </div>
                                @isset ($item->file)
                                    <a href="{{ asset($item->file) }}" class="more_news">Скачать</a>
                                @endisset
                            </div>
                        </div>
                    </div>
                   @if ($key % 4 === 0)
                        <div class="spaser"></div>
                   @endif
                @endforeach
            </div>

        </div>
    </section>
    
    <section class="pagination">
        <div class="container">
            {{-- pagination --}}
        </div>
    </section>


    <div class="space"></div>

@endsection