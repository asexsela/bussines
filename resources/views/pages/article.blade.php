@extends('layouts.base')

@section('seo')
    @if ($article->seoTitle)
        <title>{{ $article->seoTitle }} | Вологодские новости</title>
        <meta property="og:title" content="{{ $article->seoTitle }} | Вологодские новости" />
    @else
        <title>{{ $article->title }} | Вологодские новости</title>
        <meta property="og:title" content="{{ $article->title }} | Вологодские новости" />
    @endif
    @if ($article->seoDesc)
        <meta name="description" content="{{ $article->seoDesc }}" />
        <meta property="og:description" content="{{ $article->seoDesc }}" />
    @endif
    @if ($article->seoKeywords)
        <meta name="keywords" content="{{ $article->seoKeywords }}">
    @endif
    @if ($article->img)
        <meta property="og:image" content="{{ asset($article->img) }}"/>
        <link rel="image_src" href="{{ asset($article->img) }}" />
    @else
        <meta property="og:image" content="{{ asset('images/site_logo.png') }}"/>
        <link rel="image_src" href="{{ asset('images/site_logo.png') }}" />
    @endif
@endsection

@section('content')
<div class="space"></div>

    <section class="headline">
        <div class="container bc">
            <div class="breadcrumbs">
                <a href="/">Главная</a>
                <a href="{{ route('page.authors') }}">Авторы</a>
                <a href="{{ route('page.author_blogs', ['author' => $author->id]) }}">{{ $author->title }}</a>
                {{-- <span>{{ $article->title }}</span> --}}
            </div>
            <div class="toback">
                <a href="{{ route('page.author_blogs', ['author' => $author->id]) }}">< Вернуться</a>
            </div>
        </div>
    </section>

    <section class="headline">
        <div class="container">
            <h1 style="padding-top: 20px;">{{ $article->title }}</h1>
        </div>
    </section>


    <section class="article">
        <div class="container">
            <div class="row">

                <div class="col-12 col-lg-9">
                    <div class="header">
                        <p>
                            <span>Автор: {{ $author->description }}</span>
                            <span>{{ Date::parse($article->created_at)->format('j F Y, H:i ') }}</span>
                        </p> 
                        @if ($article->tags)    
                            <p style="width: 55%;">
                                @php
                                    $tags = explode(',', $article->tags);
                                @endphp
                                @foreach ($tags as $tag)
                                    <a class="tag" href="#!">#{{ trim($tag) }}</a>
                                @endforeach
                            </p>                        
                        @endif                        
                    </div>
                    <div class="body">
                        {{-- <img class="poster_image" src="{{ asset() }}" alt=""> --}}
                        {{-- <h3>{{ $article->title }}</h3> --}}
                        {!! $article->body !!}
                    </div>
                    <div class="spaser"></div>
                    <div class="footer_poster">
                        <div class="control">
                            <a href="#">
                                <img src="{{ asset('images/eye.svg') }}" alt="">
                                <span>{{ $article->views->count() }}</span>
                            </a>
                            <a href="#!" class="like" data-id="{{ $article->id }}" data-type="blog">
                                <img src="{{ asset('images/like.svg') }}" alt="">
                                <span>{{ $article->likes->count() }}</span>
                            </a>
                            <a href="#" class="dislike" data-id="{{ $article->id }}" data-type="blog">
                                <img src="{{ asset('images/dislike.svg') }}" alt="">
                                <span>{{ $article->dislikes->count() }}</span>
                            </a>
                            <div class="share">
                                <span>Поделиться</span>
                                <a href="#" id="vk"></a>
                                <a href="#!" id="fb" data-url="{{ config('app.url') }}" data-desc="{{ $article->description }}" data-title="{{ $article->title }}" data-img="{{ asset($article->img) }}"><img src="{{ asset('images/fb.svg') }}" alt=""></a>
                                {{-- <a href="#" id="ok" data-url="{{ config('app.url') }}" data-title="{{ $article->title }}" data-img="{{ asset($article->img) }}"><img src="{{ asset('images/ok.svg') }}" alt=""></a> --}}
                            </div>
                        </div>
                    </div>
                                       
                </div>
                <div class="d-none d-lg-block col-3">
                    <div class="box-ad">
                        @php
                            $count = ($count_banner < 3) ? $count_banner : 3;
                        @endphp
                        @foreach ($banners->whereNotNull('img_small')->random($count) as $item)
                            <a href="{{ $item->url }}" target="blank" banner-small data-id="{{ $item->id }}">
                                <img src="{{ asset($item->img_small) }}" alt="">
                            </a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <section class="article">
        <div class="container">
            <div class="d-none d-md-block">
                <div class="row mt-5">
                    <div class="col-12">
                        <div class="head">
                            <p>
                                <img src="{{ asset('images/media.svg') }}" alt="">
                                Другие материалы
                            </p>
                        </div>
                    </div>
    
                    @foreach ($outher_news as $item)
                        
                        <div class="col-3">
                            <div class="news">
                                <a href="{{ route('page.article', ['article' => $item->id]) }}" title="{{ $item->title }}">
                                    <span class="label">{{ Date::parse($item->created_at)->format('d.m.Y') }}</span>
                                    @if ($item->img)
                                        <img class="news_image" src="{{ asset($item->img) }}" alt="">
                                    @else
                                        <img class="news_image" src="{{ asset('/images/photos.png') }}" alt="">
                                    @endif
                                    <h3 class="news-title">{{ $item->title }}</h3>
                                    <p class="news-desc">{{ $item->description }}</p>
                                </a>
                                <div class="footer_news">
                                    <span class="data">{{ Date::parse($item->created_at)->format('j F Y, H:i ') }}</span>
                                    <div class="control">
                                        <a href="#">
                                            <img src="{{ asset('images/eye.svg') }}" alt="">
                                            <span>{{ $item->views->count() }}</span>
                                        </a>
                                        <a href="#!" class="like" data-id="{{ $item->id }}" data-type="blog">
                                            <img src="{{ asset('images/like.svg') }}" alt="">
                                            <span>{{ $item->likes->count() }}</span>
                                        </a>
                                        <a href="#" class="dislike" data-id="{{ $item->id }}" data-type="blog">
                                            <img src="{{ asset('images/dislike.svg') }}" alt="">
                                            <span>{{ $item->dislikes->count() }}</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
    
                    @endforeach
    
                </div>
            </div>

            <div class="outher_news d-block d-md-none">

                <div class="row mt-5">
                    <div class="col-12">
                        <div class="head">
                            <p>
                                Другие материалы
                            </p>
                        </div>
                    </div>
    
                    <div class="box_news">
                        @foreach ($outher_news as $item)
                        
                            <div class="news">
                                <a href="{{ route('page.article', ['article' => $item->id]) }}" title="{{ $item->title }}">
                                    <span class="label">{{ Date::parse($item->created_at)->format('d.m.Y') }}</span>
                                    @if ($item->img)
                                        <img class="news_image" src="{{ asset($item->img) }}" alt="">
                                    @else
                                        <img class="news_image" src="{{ asset('/images/photos.png') }}" alt="">
                                    @endif
                                    <h3 class="news-title">{{ $item->title }}</h3>
                                    <p class="news-desc">{{ $item->description }}</p>
                                </a>
                                <div class="footer_news">
                                    <span class="data">{{ Date::parse($item->created_at)->format('j F Y, H:i ') }}</span>
                                    <div class="control">
                                        <a href="#">
                                            <img src="{{ asset('images/eye.svg') }}" alt="">
                                            <span>{{ $item->views->count() }}</span>
                                        </a>
                                        <a href="#!" class="like" data-id="{{ $item->id }}" data-type="blog">
                                            <img src="{{ asset('images/like.svg') }}" alt="">
                                            <span>{{ $item->likes->count() }}</span>
                                        </a>
                                        <a href="#" class="dislike" data-id="{{ $item->id }}" data-type="blog">
                                            <img src="{{ asset('images/dislike.svg') }}" alt="">
                                            <span>{{ $item->dislikes->count() }}</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                    
                        @endforeach
                    </div>
    
                </div>
            </div>
        </div>
    </section>


    <div class="space"></div>

@endsection

@section('scripts')
    <script>
        // let images = $('.body').find('img');
        //     for (let i = 0; i < images.length; i++) {
        //         const e = images[i],
        //             src = $(e).attr('src');
        //             if(src[0] != '/' && src[0] != 'h') {
        //                 $(e).attr('src', '/'+src);
        //             }
        //     }

        $(function() {
            let token = $('[name="csrf-token"]').attr('content');
            $.ajax({
                type: 'post',
                url: '/blog/views',
                headers: {'X-CSRF-TOKEN': token},
                data: {
                    uid: localStorage.getItem('uuid'),
                    id: {{ $article->id }},
                },
                success: function(data) {
                    console.log(data)
                },
                error: function(err) {
                    console.log(err)
                }
            })
        })

        document.getElementById('vk').innerHTML = VK.Share.button({
            url: "{{ config('app.url') }}",
            title:" {{ $article->title }}",
            image: "{{ asset($article->img) }}",
            noparse: true,
            
        }, {
            type: 'custom', 
            text: `<img src="{{ asset('images/vk.svg') }}" alt="">`
        });
    </script>
@endsection