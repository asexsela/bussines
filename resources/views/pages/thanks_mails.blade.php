@extends('layouts.base')

@section('seo')
    <title>Благодарственные письма | Вологодские новости</title>
@endsection

@section('content')

    <section class="headline">
        <div class="container bc">
            <div class="breadcrumbs">
                <a href="#">Главная</a>
                <span>Благодарственные письма</span>
            </div>
            <h1></h1>
        </div>
    </section>
    <section class="headline">
        <div class="container">
            <h1 style="padding-top: 20px;">Благодарственные письма</h1>
        </div>
    </section>
    <section class="thank_mails">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-8 offset-md-2">
                    <div class="row">

                        @foreach ($letters as $item)    
                            <div class="col-6 col-md-4 mb-4">
                                <a data-fancybox="gallery" href="{{ asset($item->img) }}">
                                    <img src="{{ asset($item->img) }}" alt="">
                                </a>
                            </div>
                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <div class="space"></div>

@endsection