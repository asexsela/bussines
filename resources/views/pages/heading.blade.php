@extends('layouts.base')

@section('seo')
    @if ($heading->seoTitle)
        <title>{{ $heading->seoTitle }} | Вологодские новости</title>
        <meta property="og:title" content="{{ $heading->seoTitle }} | Вологодские новости" />
    @else
        <title>{{ empty($heading->title) ? $heading->name : $heading->title }} | Вологодские новости</title>
        <meta property="og:title" content="{{ empty($heading->title) ? $heading->name : $heading->title }} | Вологодские новости" />
    @endif
    @if ($heading->seoDesc)
        <meta name="description" content="{{ $heading->seoDesc }}" />
        <meta property="og:description" content="{{ $heading->seoDesc }}" />
    @endif
    @if ($heading->seoKeywords)
        <meta name="keywords" content="{{ $heading->seoKeywords }}">
    @endif
@endsection

@section('content')

<div class="space"></div>

    <section class="headline">
        <div class="container bc">
            <div class="breadcrumbs">
                <a href="/">Главная</a>
                <a href="{{ route('page.headings') }}">Рубрики</a>
                <span>{{ empty($heading->title) ? $heading->name : $heading->title }}</span>
            </div>
            <div class="toback">
                <a href="{{ route('page.headings') }}">< Вернуться</a>
            </div>
        </div>
    </section>
    @if (isset($heading->sub_categories) && $heading->sub_categories->count())

        <section class="headings pt-4">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-lg-9">
                        {{-- <div class="row">
                            
                            @foreach ($headings as $heading)
                                @if ($heading->category_id === 0 )
                                    <div class="col-6 col-md-4"><a class="btn1" href="{{ route('page.heading', ['slug' => $heading->slug]) }}">{{ $heading->name }}</a></div>
                                @endif
                            @endforeach

                        </div> --}}
                        <div class="row">
                            <div class="col-12">
                                <h2>{{ $heading->name }}</h2>
                            </div>
                            @foreach ($heading->sub_categories as $heading)
                                <div class="col-6 col-md-4">
                                    <a class="btn1" href="{{ route('page.heading.category', ['slug' => $heading->slug]) }}">{{ $heading->title }}</a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <section class="sec4 mob">
                        <div class="container">
                            @php
                                $banner = $banners->whereNotNull('img_small')->random(1)[0];
                            @endphp
                            <a href="{{ $banner->url }}" target="blank" banner-small data-id="{{ $banner->id }}">
                                <img src="{{ asset($banner->img_small) }}" alt="">
                            </a>
                        </div>
                    </section>
                    <div class="col-3 d-none d-lg-block">
                        <div class="box-ad">
                            @php
                                $count = ($count_banner < 6) ? $count_banner : 6;
                            @endphp
                            @foreach ($banners->whereNotNull('img_small')->random($count) as $item)
                                <a href="{{ $item->url }}" target="blank" banner-small data-id="{{ $banner->id }}">
                                    <img src="{{ asset($item->img_small) }}" alt="">
                                </a>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @else
        @if (request()->route()->getName() === 'page.heading.category')
            <section class="controls-news pt-4">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <h1>{{ empty($heading->title) ? $heading->name : $heading->title }}</h1>
                        </div>
                    </div>
                </div>

                <div class="container">
                    <div class="box-control">
                        <a class="btn1 @if($type == 'all') active @endif" href="{{ route('page.heading.category', ['slug' => $heading->slug]) }}">За всё время</a>
                        <a class="btn1 @if($type == 'week') active @endif" href="{{ route('page.heading.category', ['slug' => $heading->slug, 'type' => 'week']) }}">За неделю</a>
                        <a class="btn1 @if($type == 'month') active @endif" href="{{ route('page.heading.category', ['slug' => $heading->slug, 'type' => 'month']) }}">За месяц</a>
                        <a class="date @if($type == 'date') active @endif" href="#!">
                            <img src="{{ asset('images/date.svg') }}" alt="">
                            <input type="text" class="date_picker" placeholder="Выбрать дату" data-url="{{ route('page.heading', ['slug' => $heading->slug, 'type' => 'date']) }}"></span>
                        </a>
                    </div>
                </div>
            </section>
        @else
            <section class="controls-news pt-4">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <h1>{{ empty($heading->title) ? $heading->name : $heading->title }}</h1>
                        </div>
                    </div>
                </div>

                <div class="container">
                    <div class="box-control">
                        <a class="btn1 @if($type == 'all') active @endif" href="{{ route('page.heading', ['slug' => $heading->slug]) }}">За всё время</a>
                        <a class="btn1 @if($type == 'week') active @endif" href="{{ route('page.heading', ['slug' => $heading->slug, 'type' => 'week']) }}">За неделю</a>
                        <a class="btn1 @if($type == 'month') active @endif" href="{{ route('page.heading', ['slug' => $heading->slug, 'type' => 'month']) }}">За месяц</a>
                        <a class="date @if($type == 'date') active @endif" href="#!">
                            <img src="{{ asset('images/date.svg') }}" alt="">
                            <input type="text" class="date_picker" placeholder="Выбрать дату" data-url="{{ route('page.heading', ['slug' => $heading->slug, 'type' => 'date']) }}"></span>
                        </a>
                    </div>
                </div>
            </section>
        @endif

        <section class="box_news">
            @if ($news->count() <= 0)    
                <div class="container">
                    <p class="empty">Новостей не найдено</p>
                </div>
            @endif
            <div class="container">
                <div class="row">
                    @foreach ($news as $key => $new)
                        @if($key > 1) 
                            @continue;
                        @endif
                        <div class="col-12 col-md-6">
                            <div class="news">
                                <a href="@if(request()->route()->getName() === 'page.heading.category')
                                    {{ route('page.heading_page.category', ['slug' => $heading->slug, 'id' => $new->id]) }} 
                                @else 
                                    {{ route('page.heading_page', ['slug' => $heading->slug, 'id' => $new->id]) }} 
                                @endif">
                                    @if ($new->img)
                                        <div class="img big" data-bigstyle="{{ $new->big_style }}"  data-smallstyle="{{ $new->small_style }}">
                                            <span class="label">{{ Date::parse($new->created_at)->format('d.m.Y') }}</span>
                                            <img class="news_image" src="{{ asset($new->img) }}" alt="">
                                        </div>
                                    @else
                                        <div class="img">
                                            <span class="label">{{ Date::parse($new->created_at)->format('d.m.Y') }}</span>
                                            <img class="news_image" src="{{ asset('/images/photos.png') }}" alt="">
                                        </div>
                                    @endif
                                    <h3 class="news-title">{{ $new->title }}</h3>
                                    <p class="news-desc">{{ $new->description }}</p>
                                </a>
                                <div class="footer_news">
                                    <span class="data">{{ Date::parse($new->created_at)->format('j F Y') }}</span>
                                    <div class="control">
                                        <a href="#">
                                            <img src="{{ asset('images/eye.svg') }}" alt="">
                                            <span>{{ $new->views->count() }}</span>
                                        </a>
                                        <a href="#!" class="like" data-id="{{ $new->id }}" data-type="heading">
                                            <img src="{{ asset('images/like.svg') }}" alt="">
                                            <span>{{ $new->likes->count() }}</span>
                                        </a>
                                        <a href="#" class="dislike" data-id="{{ $new->id }}" data-type="heading">
                                            <img src="{{ asset('images/dislike.svg') }}" alt="">
                                            <span>{{ $new->dislikes->count() }}</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach

                </div>
                <section class="sec4 mob">
                    <div class="container">
                        @php
                            $banner = $banners->whereNotNull('img_small')->random(1)[0];
                        @endphp
                        <a href="{{ $banner->url }}" target="blank" banner-small data-id="{{ $banner->id }}">
                            <img src="{{ asset($banner->img_small) }}" alt="">
                        </a>
                    </div>
                </section>
                <div class="row">
                    <div class="col-12 col-lg-9">
                        <div class="row">
                            @foreach ($news as $key => $new)
                                @if ($key === 3 || $key === 4 || $key === 7)
                                    <div class="col-12 col-md-8">
                                        <div class="news">
                                            <a href="@if(request()->route()->getName() === 'page.heading.category')
                                                        {{ route('page.heading_page.category', ['slug' => $heading->slug, 'id' => $new->id]) }} 
                                                    @else 
                                                        {{ route('page.heading_page', ['slug' => $heading->slug, 'id' => $new->id]) }} 
                                                    @endif">
                                                @if ($new->img)
                                                    <div class="img big" data-bigstyle="{{ $new->big_style }}"  data-smallstyle="{{ $new->small_style }}">
                                                        <span class="label">{{ Date::parse($new->created_at)->format('d.m.Y') }}</span>
                                                        <img class="news_image" src="{{ asset($new->img) }}" alt="">
                                                    </div>
                                                @else
                                                    <div class="img">
                                                        <span class="label">{{ Date::parse($new->created_at)->format('d.m.Y') }}</span>
                                                        <img class="news_image" src="{{ asset('/images/photos.png') }}" alt="">
                                                    </div>
                                                @endif
                                                <h3 class="news-title">{{ $new->title }}</h3>
                                                <p class="news-desc">{{ $new->description }}</p>
                                            </a>
                                            <div class="footer_news">
                                                <span class="data">{{ Date::parse($new->created_at)->format('j F Y') }}</span>
                                                <div class="control">
                                                    <a href="#">
                                                        <img src="{{ asset('images/eye.svg') }}" alt="">
                                                        <span>{{ $new->views->count() }}</span>
                                                    </a>
                                                    <a href="#!" class="like" data-id="{{ $new->id }}" data-type="heading">
                                                        <img src="{{ asset('images/like.svg') }}" alt="">
                                                        <span>{{ $new->likes->count() }}</span>
                                                    </a>
                                                    <a href="#" class="dislike" data-id="{{ $new->id }}" data-type="heading">
                                                        <img src="{{ asset('images/dislike.svg') }}" alt="">
                                                        <span>{{ $new->dislikes->count() }}</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @elseif( $key == 2 || $key === 5 || $key === 6)
                                    <div class="col-12 col-md-4">
                                        <div class="news">
                                            <a href="{{ route('page.heading_page', ['slug' => $heading->slug, 'id' => $new->id]) }}">
                                                @if ($new->img)
                                                    <div class="img small" data-bigstyle="{{ $new->big_style }}"  data-smallstyle="{{ $new->small_style }}">
                                                        <span class="label">{{ Date::parse($new->created_at)->format('d.m.Y') }}</span>
                                                        <img class="news_image" src="{{ asset($new->img) }}" alt="">
                                                    </div>
                                                @else
                                                    <div class="img">
                                                        <span class="label">{{ Date::parse($new->created_at)->format('d.m.Y') }}</span>
                                                        <img class="news_image" src="{{ asset('/images/photos.png') }}" alt="">
                                                    </div>
                                                @endif
                                                <h3 class="news-title">{{ $new->title }}</h3>
                                                <p class="news-desc">{{ $new->description }}</p>
                                            </a>
                                            <div class="footer_news">
                                                <span class="data">{{ Date::parse($new->created_at)->format('j F Y') }}</span>
                                                <div class="control">
                                                    <a href="#">
                                                        <img src="{{ asset('images/eye.svg') }}" alt="">
                                                        <span>{{ $new->views->count() }}</span>
                                                    </a>
                                                    <a href="#!" class="like" data-id="{{ $new->id }}" data-type="heading">
                                                        <img src="{{ asset('images/like.svg') }}" alt="">
                                                        <span>{{ $new->likes->count() }}</span>
                                                    </a>
                                                    <a href="#" class="dislike" data-id="{{ $new->id }}" data-type="heading">
                                                        <img src="{{ asset('images/dislike.svg') }}" alt="">
                                                        <span>{{ $new->dislikes->count() }}</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </div>               
                    </div>
                    @if ($news->count() > 4)
                        
                        <div class="col-3 d-none d-lg-block">
                            @php
                                $count = ($count_banner < 3) ? $count_banner : 3;
                            @endphp
                            <div class="box-ad">
                                @foreach ($banners->whereNotNull('img_small')->random($count) as $item)
                                    @if ($news->count() < 2)
                                        @break
                                    @endif
                                    <a href="{{ $item->url }}" target="blank" banner-small data-id="{{ $item->id }}">
                                        <img src="{{ asset($item->img_small) }}" alt="">
                                    </a>
                                @endforeach
                            </div>
                        </div>
                    @endif
                </div>
                <div class="row">
                    @foreach ($news as $key => $new)
                        @if ($key === 8 || $key === 10 || $key === 11 || $key === 12)
                            <div class="col-12 col-md-3">
                                <div class="news">
                                    <a href="{{ route('page.heading_page', ['slug' => $heading->slug, 'id' => $new->id]) }}">
                                        @if ($new->img)
                                            <div class="img small" data-bigstyle="{{ $new->big_style }}"  data-smallstyle="{{ $new->small_style }}">
                                                <span class="label">{{ Date::parse($new->created_at)->format('d.m.Y') }}</span>
                                                <img class="news_image" src="{{ asset($new->img) }}" alt="">
                                            </div>
                                        @else
                                            <div class="img">
                                                <span class="label">{{ Date::parse($new->created_at)->format('d.m.Y') }}</span>
                                                <img class="news_image" src="{{ asset('/images/photos.png') }}" alt="">
                                            </div>
                                        @endif
                                        <h3 class="news-title">{{ $new->title }}</h3>
                                        <p class="news-desc">{{ $new->description }}</p>
                                    </a>
                                    <div class="footer_news">
                                        <span class="data">{{ Date::parse($new->created_at)->format('j F Y') }}</span>
                                        <div class="control">
                                            <a href="#">
                                                <img src="{{ asset('images/eye.svg') }}" alt="">
                                                <span>{{ $new->views->count() }}</span>
                                            </a>
                                            <a href="#!" class="like" data-id="{{ $new->id }}" data-type="heading">
                                                <img src="{{ asset('images/like.svg') }}" alt="">
                                                <span>{{ $new->likes->count() }}</span>
                                            </a>
                                            <a href="#" class="dislike" data-id="{{ $new->id }}" data-type="heading">
                                                <img src="{{ asset('images/dislike.svg') }}" alt="">
                                                <span>{{ $new->dislikes->count() }}</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @elseif($key === 9 || $key === 13)
                            <div class="col-12 col-md-6">
                                <div class="news">
                                    <a href="{{ route('page.heading_page', ['slug' => $heading->slug, 'id' => $new->id]) }}">
                                        @if ($new->img)
                                            <div class="img big" data-bigstyle="{{ $new->big_style }}"  data-smallstyle="{{ $new->small_style }}">
                                                <span class="label">{{ Date::parse($new->created_at)->format('d.m.Y') }}</span>
                                                <img class="news_image" src="{{ asset($new->img) }}" alt="">
                                            </div>
                                        @else
                                            <div class="img">
                                                <span class="label">{{ Date::parse($new->created_at)->format('d.m.Y') }}</span>
                                                <img class="news_image" src="{{ asset('/images/photos.png') }}" alt="">
                                            </div>
                                        @endif
                                        <h3 class="news-title">{{ $new->title }}</h3>
                                        <p class="news-desc">{{ $new->description }}</p>
                                    </a>
                                    <div class="footer_news">
                                        <span class="data">{{ Date::parse($new->created_at)->format('j F Y') }}</span>
                                        <div class="control">
                                            <a href="#">
                                                <img src="{{ asset('images/eye.svg') }}" alt="">
                                                <span>{{ $new->views->count() }}</span>
                                            </a>
                                            <a href="#!" class="like" data-id="{{ $new->id }}" data-type="heading">
                                                <img src="{{ asset('images/like.svg') }}" alt="">
                                                <span>{{ $new->likes->count() }}</span>
                                            </a>
                                            <a href="#" class="dislike" data-id="{{ $new->id }}" data-type="heading">
                                                <img src="{{ asset('images/dislike.svg') }}" alt="">
                                                <span>{{ $new->dislikes->count() }}</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="row">
                            @foreach ($news as $key => $new)
                                @if($key === 14)
                                    <div class="col-12 col-md-6">
                                        <div class="news">
                                            <a href="{{ route('page.heading_page', ['slug' => $heading->slug, 'id' => $new->id]) }}">
                                                @if ($new->img)
                                                    <div class="img big" data-bigstyle="{{ $new->big_style }}"  data-smallstyle="{{ $new->small_style }}">
                                                        <span class="label">{{ Date::parse($new->created_at)->format('d.m.Y') }}</span>
                                                        <img class="news_image" src="{{ asset($new->img) }}" alt="">
                                                    </div>
                                                @else
                                                    <div class="img">
                                                        <span class="label">{{ Date::parse($new->created_at)->format('d.m.Y') }}</span>
                                                        <img class="news_image" src="{{ asset('/images/photos.png') }}" alt="">
                                                    </div>
                                                @endif
                                                <h3 class="news-title">{{ $new->title }}</h3>
                                                <p class="news-desc">{{ $new->description }}</p>
                                            </a>
                                            <div class="footer_news">
                                                <span class="data">{{ Date::parse($new->created_at)->format('j F Y') }}</span>
                                                <div class="control">
                                                    <a href="#">
                                                        <img src="{{ asset('images/eye.svg') }}" alt="">
                                                        <span>{{ $new->views->count() }}</span>
                                                    </a>
                                                    <a href="#!" class="like" data-id="{{ $new->id }}" data-type="heading">
                                                        <img src="{{ asset('images/like.svg') }}" alt="">
                                                        <span>{{ $new->likes->count() }}</span>
                                                    </a>
                                                    <a href="#" class="dislike" data-id="{{ $new->id }}" data-type="heading">
                                                        <img src="{{ asset('images/dislike.svg') }}" alt="">
                                                        <span>{{ $new->dislikes->count() }}</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @elseif($key === 15)
                                    <div class="col-12 col-md-3">
                                        <div class="news">
                                            <a href="{{ route('page.heading_page', ['slug' => $heading->slug, 'id' => $new->id]) }}">
                                                @if ($new->img)
                                                    <div class="img small" data-bigstyle="{{ $new->big_style }}"  data-smallstyle="{{ $new->small_style }}">
                                                        <span class="label">{{ Date::parse($new->created_at)->format('d.m.Y') }}</span>
                                                        <img class="news_image" src="{{ asset($new->img) }}" alt="">
                                                    </div>
                                                @else
                                                    <div class="img">
                                                        <span class="label">{{ Date::parse($new->created_at)->format('d.m.Y') }}</span>
                                                        <img class="news_image" src="{{ asset('/images/photos.png') }}" alt="">
                                                    </div>
                                                @endif
                                                <h3 class="news-title">{{ $new->title }}</h3>
                                                <p class="news-desc">{{ $new->description }}</p>
                                            </a>
                                            <div class="footer_news">
                                                <span class="data">{{ Date::parse($new->created_at)->format('j F Y') }}</span>
                                                <div class="control">
                                                    <a href="#">
                                                        <img src="{{ asset('images/eye.svg') }}" alt="">
                                                        <span>{{ $new->views->count() }}</span>
                                                    </a>
                                                    <a href="#!" class="like" data-id="{{ $new->id }}" data-type="heading">
                                                        <img src="{{ asset('images/like.svg') }}" alt="">
                                                        <span>{{ $new->likes->count() }}</span>
                                                    </a>
                                                    <a href="#" class="dislike" data-id="{{ $new->id }}" data-type="heading">
                                                        <img src="{{ asset('images/dislike.svg') }}" alt="">
                                                        <span>{{ $new->dislikes->count() }}</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @elseif($key === 16 || $key === 17)
                                    <div class="col-12 col-md-3">
                                        <div class="news">
                                            <a href="{{ route('page.heading_page', ['slug' => $heading->slug, 'id' => $new->id]) }}">
                                                @if ($new->img)
                                                    <div class="img small" data-bigstyle="{{ $new->big_style }}"  data-smallstyle="{{ $new->small_style }}">
                                                        <span class="label">{{ Date::parse($new->created_at)->format('d.m.Y') }}</span>
                                                        <img class="news_image" src="{{ asset($new->img) }}" alt="">
                                                    </div>
                                                @else
                                                    <div class="img">
                                                        <span class="label">{{ Date::parse($new->created_at)->format('d.m.Y') }}</span>
                                                        <img class="news_image" src="{{ asset('/images/photos.png') }}" alt="">
                                                    </div>
                                                @endif
                                                <h3 class="news-title">{{ $new->title }}</h3>
                                                <p class="news-desc">{{ $new->description }}</p>
                                            </a>
                                            <div class="footer_news">
                                                <span class="data">{{ Date::parse($new->created_at)->format('j F Y') }}</span>
                                                <div class="control">
                                                    <a href="#">
                                                        <img src="{{ asset('images/eye.svg') }}" alt="">
                                                        <span>{{ $new->views->count() }}</span>
                                                    </a>
                                                    <a href="#!" class="like" data-id="{{ $new->id }}" data-type="heading">
                                                        <img src="{{ asset('images/like.svg') }}" alt="">
                                                        <span>{{ $new->likes->count() }}</span>
                                                    </a>
                                                    <a href="#" class="dislike" data-id="{{ $new->id }}" data-type="heading">
                                                        <img src="{{ asset('images/dislike.svg') }}" alt="">
                                                        <span>{{ $new->dislikes->count() }}</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </div>  
                    </div>
                </div>
            </div>
        </section>

        <div class="section pagination">
            <div class="container">
                {{ $news->links() }}
            </div>
        </div>

    @endif

    @include('components.slider_arhives')
    <div class="space"></div>
@endsection