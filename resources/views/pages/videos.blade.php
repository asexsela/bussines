@extends('layouts.base')

@section('seo')
    <title>Видео | Вологодские новости</title>
@endsection

@section('content')
    @php
        $page = request()->get('page') ?? 1;
    @endphp
   <section class="sec1">
        <div class="container">
            @php
                $banner = $banners->whereNotNull('img_big')->random(1)[0];
            @endphp
            <a href="{{ $banner->url }}" target="blank" banner-big data-id="{{ $banner->id }}">
                <img src="{{ asset($banner->img_big) }}" alt="">
            </a>
        </div>
    </section>
    <section class="headline">
        <div class="container">
            <h1>Видео</h1>
        </div>
    </section>
    
    <section class="videos">
       <div class="container">
           <div class="row">
                @foreach ($videos as $key => $video)
                    @if ($key >= 2)
                        @continue
                    @endif
                        <div class="col-12 col-md-6">
                            <div class="news">
                                <a data-fancybox href="{{ $video->video }}">
                                    <div class="video" data-page="{{ $page }}">
                                        @if ($page == 1)
                                            <span class="label">Последнее видео</span>
                                        @else
                                            <span class="label">{{ Date::parse($video->created_at)->format('d.m.Y') }}</span>
                                        @endif
                                        <img class="play" src="{{ asset('images/play1.png') }}" alt="">
                                        <img class="news_image" src="{{ asset($video->img) }}" alt="">
                                    </div>
                                </a>
                                <a href="{{ route('page.video', ['video' => $video->id]) }}">
                                    <h3>{{ $video->title }}</h3>
                                    <p class="news-desc">{{ $video->description }}</p>
                                </a>
                                <div class="footer_news">
                                    <span class="data">{{ Date::parse($video->created_at)->format('j F Y, H:i ') }}</span>
                                    <div class="control">
                                        <a href="#">
                                            <img src="{{ asset('images/eye.svg') }}" alt="">
                                            <span>{{ $video->views->count() }}</span>
                                        </a>
                                        <a href="#!" class="like" data-id="{{ $video->id }}" data-type="video">
                                            <img src="{{ asset('images/like.svg') }}" alt="">
                                            <span>{{ $video->likes->count() }}</span>
                                        </a>
                                        <a href="#" class="dislike" data-id="{{ $video->id }}" data-type="video">
                                            <img src="{{ asset('images/dislike.svg') }}" alt="">
                                            <span>{{ $video->dislikes->count() }}</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                @endforeach
               
           </div>
       </div>
       
       <div class="space"></div>
       <section class="sec4 mob">
            <div class="container">
                @php
                    $banner = $banners->whereNotNull('img_small')->random(1)[0];
                @endphp
                <a href="{{ $banner->url }}" target="blank" banner-small data-id="{{ $banner->id }}">
                    <img src="{{ asset($banner->img_small) }}" alt="">
                </a>
            </div>
        </section>
       <div class="container">
           <div class="row">
                @foreach ($videos as $key => $video)
                    @if ($key >= 2)
                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 mt-5">
                            <div class="news min">
                                <a data-fancybox href="{{ $video->video }}">
                                    <div class="video">
                                        <span class="label">{{ Date::parse($video->created_at)->format('d.m.Y') }}</span>
                                        <img class="play" src="{{ asset('images/play1.png') }}" alt="">
                                        <img class="news_image" src="{{ asset($video->img) }}" alt="">
                                    </div>
                                </a>
                                <a href="{{ route('page.video', ['video' => $video->id]) }}">
                                    <h3>{{ $video->title }}</h3>
                                    <p class="news-desc">{{ $video->description }}</p>
                                </a>
                                <div class="footer_news">
                                    <span class="data">{{ Date::parse($video->created_at)->format('j F Y, H:i ') }}</span>
                                    <div class="control">
                                        <a href="#">
                                            <img src="{{ asset('images/eye.svg') }}" alt="">
                                            <span>{{ $video->views->count() }}</span>
                                        </a>
                                        <a href="#!" class="like" data-id="{{ $video->id }}" data-type="video">
                                            <img src="{{ asset('images/like.svg') }}" alt="">
                                            <span>{{ $video->likes->count() }}</span>
                                        </a>
                                        <a href="#" class="dislike" data-id="{{ $video->id }}" data-type="video">
                                            <img src="{{ asset('images/dislike.svg') }}" alt="">
                                            <span>{{ $video->dislikes->count() }}</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>    
       </div>
    </section>
    <div class="space"></div>
    <section class="pagination">
        <div class="container">
            {{ $videos->links() }}
        </div>
    </section>
    <div class="space"></div>

@endsection