@extends('layouts.base')

@section('seo')
    @if ($video->seoTitle)
        <title>{{ $video->seoTitle }} | Вологодские новости</title>
        <meta property="og:title" content="{{ $video->seoTitle }} | Вологодские новости" />
    @else
        <title>{{ $video->title }} | Вологодские новости</title>
        <meta property="og:title" content="{{ $video->title }} | Вологодские новости" />
    @endif
    @if ($video->seoDesc)
        <meta name="description" content="{{ $video->seoDesc }}" />
        <meta property="og:description" content="{{ $video->seoDesc }}" />
    @endif
    @if ($video->seoKeywords)
        <meta name="keywords" content="{{ $video->seoKeywords }}">
    @endif
    @if ($video->img)
        <meta property="og:image" content="{{ asset($video->img) }}"/>
        <link rel="image_src" href="{{ asset($video->img) }}" />
    @else
        <meta property="og:image" content="{{ asset('images/site_logo.png') }}"/>
        <link rel="image_src" href="{{ asset('images/site_logo.png') }}" />
    @endif
@endsection

@section('content')
<div class="space"></div>

    <section class="headline">
        <div class="container bc">
            <div class="breadcrumbs">
                <a href="/">Главная</a>
                <a href="/videos">Видео</a>
                <span>{{ $video->title }}</span>
            </div>
            <div class="toback">
                <a href="{{ route('page.videos') }}">< Вернуться</a>
            </div>
        </div>
    </section>

    <section class="headline">
        <div class="container">
            <h1 style="padding-top: 20px;">{{ $video->title }}</h1>
        </div>
    </section>

    <section class="video pt-4">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-9">
                    <div class="news">
                        <a data-fancybox href="{{ $video->video }}">
                            <div class="video">
                                <span class="label">{{ Date::parse($video->created_at)->format('d.m.Y') }}</span>
                                <img class="play" src="{{ asset('images/play1.png') }}" alt="">
                                <img class="news_image" src="{{ asset($video->img) }}" alt="">
                            </div>
                        </a>
                        <h3>{{ $video->title }}</h3>
                        <div class="footer_news">
                            <span class="data">{{ Date::parse($video->created_at)->format('j F Y, H:i ') }}</span>
                            <div class="control">
                                <a href="#">
                                    <img src="{{ asset('images/eye.svg') }}" alt="">
                                    <span>{{ $video->views->count() }}</span>
                                </a>
                                <a href="#!" class="like" data-id="{{ $video->id }}" data-type="video">
                                    <img src="{{ asset('images/like.svg') }}" alt="">
                                    <span>{{ $video->likes->count() }}</span>
                                </a>
                                <a href="#" class="dislike" data-id="{{ $video->id }}" data-type="video">
                                    <img src="{{ asset('images/dislike.svg') }}" alt="">
                                    <span>{{ $video->dislikes->count() }}</span>
                                </a>
                                <div class="share">
                                    <span>Поделиться</span>
                                    <a href="#!" id="vk"></a>
                                    <a href="#!" id="fb" data-url="{{ config('app.url') }}" data-desc="{{ $video->description }}" data-title="{{ $video->title }}" data-img="{{ asset($video->img) }}"><img src="{{ asset('images/fb.svg') }}" alt=""></a>
                                    {{-- <a href="#" id="ok" data-url="{{ config('app.url') }}" data-title="{{ $video->title }}" data-img="{{ asset($video->img) }}"><img src="{{ asset('images/ok.svg') }}" alt=""></a> --}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="body">
                        {{ $video->description }}
                        <br>
                        <br>
                        {!! $video->body !!}
                    </div>
                    <div class="space"></div>
                    
                    <span class="name">
                        Автор: {{ $video->author }}
                    </span>
                    {{-- <p> 
                        <a href="#"class="more_news">
                            <svg width="16" height="16" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg">
                                <path d="M12.473 7.946C14.3911 7.946 15.946 7.06266 15.946 5.973C15.946 4.88334 14.3911 4 12.473 4C10.5549 4 9 4.88334 9 5.973C9 7.06266 10.5549 7.946 12.473 7.946Z" fill="#A33D32"/>
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M12.525 9.08101C10.041 9.08101 9.052 8.04001 9.052 7.43701V10.02C9.052 11.11 10.607 11.992 12.525 11.992C14.445 11.992 16 11.11 16 10.02V7.49901C16 8.10301 15.01 9.08101 12.525 9.08101Z" fill="#A33D32"/>
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M12.525 13.072C10.303 13.072 9.052 12.071 9.052 11.468V14.027C9.052 15.117 10.607 16 12.525 16C14.445 16 16 15.117 16 14.027V11.437C16 12.04 14.748 13.072 12.525 13.072Z" fill="#A33D32"/>
                                <path d="M3.937 3.946C6.11135 3.946 7.874 3.06266 7.874 1.973C7.874 0.883342 6.11135 0 3.937 0C1.76266 0 0 0.883342 0 1.973C0 3.06266 1.76266 3.946 3.937 3.946Z" fill="#A33D32"/>
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M4.062 5.08101C1.247 5.08101 0.125 4.04001 0.125 3.43701V6.02001C0.125 7.11001 1.888 7.99201 4.062 7.99201C6.238 7.99201 8 7.11001 8 6.02001V3.49901C8 4.10301 6.877 5.08101 4.062 5.08101Z" fill="#A33D32"/>
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M4.062 9.07201C1.543 9.07201 0.125 8.07101 0.125 7.46801V10.027C0.125 11.117 1.888 12 4.062 12C6.238 12 8 11.117 8 10.027V7.43701C8 8.04001 6.58 9.07201 4.062 9.07201Z" fill="#A33D32"/>
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M4.062 13.072C1.543 13.072 0.125 12.071 0.125 11.468V14.027C0.125 15.117 1.888 16 4.062 16C6.238 16 8 15.117 8 14.027V11.437C8 12.04 6.58 13.072 4.062 13.072Z" fill="#A33D32"/>
                            </svg>
                                
                        Оформить подписку
                        </a>
                    </p> --}}
                </div>
                <div class="d-none d-lg-block col-3">
                    <div class="box-ad">
                        @php
                            $count = ($count_banner < 3) ? $count_banner : 3;
                        @endphp
                        @foreach ($banners->whereNotNull('img_small')->random($count) as $item)
                            <a href="{{ $item->url }}" target="blank" banner-small data-id="{{ $item->id }}">
                                <img src="{{ asset($item->img_small) }}" alt="">
                            </a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="sec6 vid d-none d-lg-block">
        <div class="container">
            <div class="head">
                <p><img src="{{ asset('images/media.svg') }}" alt="">Материалы по теме</p>
                <span class="pagingInfo v">
                    <a href="#" class="prev"><</a>
                    <span class="page_count_video"></span>
                    <a href="#" class="next">></a>
                </span>
            </div>
            <div class="slider_video">
                @foreach ($outhers as $item)
                        <div class="news min">
                            <a data-fancybox href="{{ $item->video }}">
                                <div class="video">
                                    <span class="label">{{ Date::parse($item->created_at)->format('d.m.Y') }}</span>
                                    <img class="play" src="{{ asset('images/play1.png') }}" alt="">
                                    <img class="news_image" src="{{ asset($item->img) }}" alt="">
                                </div>
                            </a>
                            <a href="{{ route('page.video', ['video' => $item->id]) }}">
                                <h3>{{ $item->title }}</h3>
                                <p class="news-desc">{{ $item->description }}</p>
                            </a>
                            <div class="footer_news">
                                <span class="data">{{ Date::parse($item->created_at)->format('j F Y, H:i ') }}</span>
                                <div class="control">
                                    <a href="#">
                                        <img src="{{ asset('images/eye.svg') }}" alt="">
                                        <span>{{ $item->views->count() }}</span>
                                    </a>
                                    <a href="#!" class="like" data-id="{{ $item->id }}" data-type="video">
                                        <img src="{{ asset('images/like.svg') }}" alt="">
                                        <span>{{ $item->likes->count() }}</span>
                                    </a>
                                    <a href="#" class="dislike" data-id="{{ $item->id }}" data-type="video">
                                        <img src="{{ asset('images/dislike.svg') }}" alt="">
                                        <span>{{ $item->dislikes->count() }}</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                @endforeach
            </div>
        </div>
    </section>

    <section class="sec6 vid d-block d-lg-none">
        <div class="container">
            <div class="head">
                <p><img src="{{ asset('images/media.svg') }}" alt="">Материалы по теме</p>
                <span class="pagingInfo v">
                    <a href="#" class="prev"><</a>
                    <span class="page_count_video"></span>
                    <a href="#" class="next">></a>
                </span>
            </div>
            <div class="box_video">
                @foreach ($outhers as $item)
                        <div class="news min">
                            <a data-fancybox href="{{ $item->video }}">
                                <div class="video">
                                    <span class="label">{{ Date::parse($item->created_at)->format('d.m.Y') }}</span>
                                    <img class="play" src="{{ asset('images/play1.png') }}" alt="">
                                    <img class="news_image" src="{{ asset($item->img) }}" alt="">
                                </div>
                            </a>
                            <a href="{{ route('page.video', ['video' => $item->id]) }}">
                                <h3>{{ $item->title }}</h3>
                                <p class="news-desc">{{ $item->description }}</p>
                            </a>
                            <div class="footer_news">
                                <span class="data">{{ Date::parse($item->created_at)->format('j F Y, H:i ') }}</span>
                                <div class="control">
                                    <a href="#">
                                        <img src="{{ asset('images/eye.svg') }}" alt="">
                                        <span>{{ $item->views->count() }}</span>
                                    </a>
                                    <a href="#!" class="like" data-id="{{ $item->id }}" data-type="video">
                                        <img src="{{ asset('images/like.svg') }}" alt="">
                                        <span>{{ $item->likes->count() }}</span>
                                    </a>
                                    <a href="#" class="dislike" data-id="{{ $item->id }}" data-type="video">
                                        <img src="{{ asset('images/dislike.svg') }}" alt="">
                                        <span>{{ $item->dislikes->count() }}</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                @endforeach
            </div>
        </div>
    </section>
    <div class="space"></div>


    @include('components.slider_arhives')

@endsection

@section('scripts')
    <script>
        // let images = $('.body').find('img');
        //     for (let i = 0; i < images.length; i++) {
        //         const e = images[i],
        //             src = $(e).attr('src');
        //             if(src[0] != '/' && src[0] != 'h') {
        //                 $(e).attr('src', '/'+src);
        //             }
        //     }

        $(function() {
            let token = $('[name="csrf-token"]').attr('content');
            $.ajax({
                type: 'post',
                url: '/video/views',
                headers: {'X-CSRF-TOKEN': token},
                data: {
                    uid: localStorage.getItem('uuid'),
                    id: {{ $video->id }},
                },
                success: function(data) {
                    console.log(data)
                },
                error: function(err) {
                    console.log(err)
                }
            })
        })

        document.getElementById('vk').innerHTML = VK.Share.button({
            url: "{{ config('app.url') }}",
            title:" {{ $video->title }}",
            image: "{{ asset($video->img) }}",
            noparse: true,
            
        }, {
            type: 'custom', 
            text: `<img src="{{ asset('images/vk.svg') }}" alt="">`
        });


    </script>
@endsection