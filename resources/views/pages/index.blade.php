@extends('layouts.base')

@section('seo')
    <title>Новости области сегодня. Читайте последние новости дня | Вологодские новости</title>
    <meta property="og:title" content="Новости области сегодня. Читайте последние новости дня | Вологодские новости" />
@endsection

@section('content')

    <section class="sec1">
        <div class="container">
            @php
                $banner = $banners->whereNotNull('img_big')->random(1)[0];
            @endphp
            <a href="{{ $banner->url }}" target="blank" banner-big data-id="{{ $banner->id }}" data-type="big">
                <img src="{{ asset($banner->img_big) }}" alt="">
            </a>
        </div>
    </section>
    <section class="sec2">
        <div class="container">
            <div class="row">
                <div class="col-3 d-none d-lg-block">
                    <div class="box_top_news">
                        <div class="head">
                            <p><img src="{{ asset('images/news.svg') }}" alt="">Топ лучших новостей</p>
                        </div>
                        <div class="news_lists">
                            @php
                                $i = 0;
                            @endphp
                            @foreach ($top_news as $k => $news)
                                <div data-views="{{ $news->views->count() }}" class="list @if($i === 0) active @endif" data-key="{{ $i }}">
                                    <span>{{ Date::parse($news->published_at)->format('d.m.Y') }}</span>
                                    <a href="@if($news->isType() === 'heading-page/') {{ url($news->isType().$news->heading->slug.'/'.$news->id) }} @else {{ url($news->isType().$news->id) }} @endif">
                                        <p class="title">{{ $news->title }}</p>
                                        <div class="body">
                                            <img src="{{ asset($news->img) }}" alt="">
                                        </div>
                                    </a>
                                </div>
                                @php
                                    $i++;
                                @endphp
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-9 col-lg-6 px-0">
                    <div class="news_is_day">
                        <div class="news">
                            <a href="{{ route('page.heading_page', ['slug' => $news_is_day['themeIsDay']->heading->slug,'id' => $news_is_day['themeIsDay']->id]) }}">
                                <div class="img small" 
                                    data-bigstyle="{{ $news_is_day['themeIsDay']->big_style }}"  
                                    data-smallstyle="{{ $news_is_day['themeIsDay']->small_style }}"  
                                    data-small 
                                    style="{{ $news_is_day['themeIsDay']->small_style }}">
                                    <span class="label">Тема дня</span>
                                    <img class="news_image" src="{{ asset($news_is_day['themeIsDay']->img) }}" alt="">
                                </div>
                                <h3>{{ $news_is_day['themeIsDay']->title }}</h3>
                                <p class="news-desc">{{ $news_is_day['themeIsDay']->description }}</p>
                            </a>
                            <div class="footer_news">
                                <span class="data">{{ Date::parse($news_is_day['themeIsDay']->published_at)->format('j F Y') }}</span>
                                <div class="control">
                                    <a href="#">
                                        <img src="{{ asset('images/eye.svg') }}" alt="">
                                        <span>{{ $news_is_day['themeIsDay']->views->count() }}</span>
                                    </a>
                                    <a href="#" class="like" data-id="{{ $news_is_day['themeIsDay']->id }}" data-type="heading">
                                        <img src="{{ asset('images/like.svg') }}" alt="">
                                        <span>{{ $news_is_day['themeIsDay']->likes->count() }}</span>
                                    </a>
                                    <a href="#" class="dislike" data-id="{{ $news_is_day['themeIsDay']->id }}" data-type="heading">
                                        <img src="{{ asset('images/dislike.svg') }}" alt="">
                                        <span>{{ $news_is_day['themeIsDay']->dislikes->count() }}</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <index-voiting></index-voiting>
                        <div class="news">
                            <a href="{{ route('page.new', ['news' => $news_is_day['news_last']->id]) }}">
                                <div class="img small" 
                                    data-bigstyle="{{ $news_is_day['news_last']->big_style }}"  
                                    data-smallstyle="{{ $news_is_day['news_last']->small_style }}"  
                                    data-small 
                                    style="{{ $news_is_day['news_last']->small_style }}">
                                    <span class="label">Последняя новость</span>
                                    <img class="news_image" src="{{ asset($news_is_day['news_last']->img) }}" alt="">
                                </div>
                                <h3>{{ $news_is_day['news_last']->title }}</h3>
                                <p class="news-desc">{{ $news_is_day['news_last']->description }}</p>
                            </a>
                            <div class="footer_news">
                                <span class="data">{{ Date::parse($news_is_day['news_last']->published_at)->format('j F Y') }}</span>
                                <div class="control">
                                    <a href="#">
                                        <img src="{{ asset('images/eye.svg') }}" alt="">
                                        <span>{{ $news_is_day['news_last']->views->count() }}</span>
                                    </a>
                                    <a href="#" class="like" data-id="{{ $news_is_day['news_last']->id }}" data-type="news">
                                        <img src="{{ asset('images/like.svg') }}" alt="">
                                        <span>{{ $news_is_day['news_last']->likes->count() }}</span>
                                    </a>
                                    <a href="#" class="dislike" data-id="{{ $news_is_day['news_last']->id }}" data-type="news">
                                        <img src="{{ asset('images/dislike.svg') }}" alt="">
                                        <span>{{ $news_is_day['news_last']->dislikes->count() }}</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="news">
                            <a href="{{ route('page.new', ['news' => $news_is_day['partners']->id]) }}">
                                <div class="img small" 
                                    data-bigstyle="{{ $news_is_day['partners']->big_style }}"  
                                    data-smallstyle="{{ $news_is_day['partners']->small_style }}"  
                                    data-small 
                                    style="{{ $news_is_day['partners']->small_style }}">
                                    <span class="label">Новость партнера</span>
                                    <img class="news_image" src="{{ asset($news_is_day['partners']->img) }}" alt="">
                                </div>
                                <h3>{{ $news_is_day['partners']->title }}</h3>
                                <p class="news-desc">{{ $news_is_day['partners']->description }}</p>
                            </a>
                            <div class="footer_news">
                                <span class="data">{{ Date::parse($news_is_day['partners']->published_at)->format('j F Y') }}</span>
                                <div class="control">
                                    <a href="#">
                                        <img src="{{ asset('images/eye.svg') }}" alt="">
                                        <span>{{ $news_is_day['partners']->views->count() }}</span>
                                    </a>
                                    <a href="#" class="like" data-id="{{ $news_is_day['partners']->id }}" data-type="news">
                                        <img src="{{ asset('images/like.svg') }}" alt="">
                                        <span>{{ $news_is_day['partners']->likes->count() }}</span>
                                    </a>
                                    <a href="#" class="dislike" data-id="{{ $news_is_day['partners']->id }}" data-type="news">
                                        <img src="{{ asset('images/dislike.svg') }}" alt="">
                                        <span>{{ $news_is_day['partners']->dislikes->count() }}</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <a href="{{ route('page.news') }}" class="more_news">Показать больше</a>
                </div>
                <div class="col-3 d-none d-md-block">
                    <div class="box-ad">
                        @php
                            $count = ($count_banner < 4) ? $count_banner : 4;
                        @endphp
                        @foreach ($banners->whereNotNull('img_small')->random($count) as $item)
                            <a data-id="{{ $item->id }}" href="{{ $item->url }}" target="blank" banner-small data-type="small">
                                <img src="{{ asset($item->img_small) }}" alt="">
                            </a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="sec7 mob">
        <div class="container">
            <div class="head">
                <p>Наши партнёры</p>
            </div>
            <div class="gallery">
                @foreach ($parnters_slider as $item)    
                    <img src="{{ asset($item->img) }}" alt="">
                @endforeach
            </div>
        </div>
    </section>
    <section class="sec3">
        <div class="container">
            <div class="head">
                <p>Рубрики</p>
            </div>
            <div class="spaser"></div>
            <div class="row">
                @foreach ($heading_news as $key => $news)
                    @if ($key === 0 || $key === 2 || $key === 4)    
                        <div class="col-12 col-sm-6 col-lg-3">
                            <div class="news">
                                <a href="{{ route('page.heading_page', ['slug' => $news->heading->slug, 'id' => $news->id]) }}">
                                    <div class="img small" 
                                        data-bigstyle="{{ $news->big_style }}"  
                                        data-smallstyle="{{ $news->small_style }}"  
                                        data-small 
                                        style="{{ $news->small_style }}">
                                        <span class="label">{{ $news->heading->name }}</span>
                                        <img class="news_image" src="{{ asset($news->img) }}" alt="">
                                    </div> 
                                    <h3>{{ $news->title }}</h3>
                                    <p class="news-desc">{{ $news->description }}</p>
                                </a>
                                <div class="footer_news">
                                    <span class="data">{{ Date::parse($news->published_at)->format('j F Y') }}</span>
                                    <div class="control">
                                        <a href="#">
                                            <img src="{{ asset('images/eye.svg') }}" alt="">
                                            <span>{{ $news->views->count() }}</span>
                                        </a>
                                        <a href="#!" class="like" data-id="{{ $news->id }}" data-type="heading">
                                            <img src="{{ asset('images/like.svg') }}" alt="">
                                            <span>{{ $news->likes->count() }}</span>
                                        </a>
                                        <a href="#" class="dislike" data-id="{{ $news->id }}" data-type="heading">
                                            <img src="{{ asset('images/dislike.svg') }}" alt="">
                                            <span>{{ $news->dislikes->count() }}</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @elseif($key === 1 || $key === 3)
                        <div class="col-12 col-sm-6 col-lg-6">
                            <div class="news">
                                <a href="{{ route('page.heading_page', ['slug' => $news->heading->slug, 'id' => $news->id]) }}">
                                    <div class="img big" 
                                        data-bigstyle="{{ $news->big_style }}"  
                                        data-smallstyle="{{ $news->small_style }}"
                                        data-small 
                                        style="{{ $news->small_style }}">
                                        <span class="label">{{ $news->heading->name }}</span>
                                        <img class="news_image" src="{{ asset($news->img) }}" alt="">
                                    </div> 
                                    <h3>{{ $news->title }}</h3>
                                    <p class="news-desc">{{ $news->description }}</p>
                                </a>
                                <div class="footer_news">
                                    <span class="data">{{ Date::parse($news->published_at)->format('j F Y') }}</span>
                                    <div class="control">
                                        <a href="#">
                                            <img src="{{ asset('images/eye.svg') }}" alt="">
                                            <span>{{ $news->views->count() }}</span>
                                        </a>
                                        <a href="#!" class="like" data-id="{{ $news->id }}" data-type="heading">
                                            <img src="{{ asset('images/like.svg') }}" alt="">
                                            <span>{{ $news->likes->count() }}</span>
                                        </a>
                                        <a href="#" class="dislike" data-id="{{ $news->id }}" data-type="heading">
                                            <img src="{{ asset('images/dislike.svg') }}" alt="">
                                            <span>{{ $news->dislikes->count() }}</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="news min">
                        <a data-fancybox href="{{ $last_video->video }}">
                            <div class="video small" 
                                data-bigstyle="{{ $news->big_style }}"
                                data-smallstyle="{{ $news->small_style }}"
                                data-small 
                                style="{{ $news->small_style }}">
                                <span class="label">Видео</span>
                                <img class="play" src="{{ asset('images/play1.png') }}" alt="">
                                <img class="news_image" src="{{ asset($last_video->img) }}" alt="">
                            </div>
                        </a>
                        <a href="{{ route('page.video', ['video' => $last_video->id]) }}">
                            <h3>{{ $last_video->title }}</h3>
                            <p class="news-desc">{{ $last_video->description }}</p>
                        </a>
                        <div class="footer_news">
                            <span class="data">{{ Date::parse($last_video->created_at)->format('j F Y') }}</span>
                            <div class="control">
                                <a href="#">
                                    <img src="{{ asset('images/eye.svg') }}" alt="">
                                    <span>{{ $last_video->views->count() }}</span>
                                </a>
                                <a href="#!" class="like" data-id="{{ $last_video->id }}" data-type="video">
                                    <img src="{{ asset('images/like.svg') }}" alt="">
                                    <span>{{ $last_video->likes->count() }}</span>
                                </a>
                                <a href="#" class="dislike" data-id="{{ $last_video->id }}" data-type="video">
                                    <img src="{{ asset('images/dislike.svg') }}" alt="">
                                    <span>{{ $last_video->dislikes->count() }}</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="space"></div>
            <div class="row">
                <div class="col-12">
                    <a href="{{ route('page.headings') }}" class="more_news">Показать больше</a>
                </div>
            </div>
            <div class="space"></div>

        </div>
    </section>
    <section class="insta">
        <svg width="47" height="47" viewBox="0 0 21 20" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M10.5 5.19298C7.729 5.19298 5.49267 7.33986 5.49267 10C5.49267 12.6602 7.729 14.807 10.5 14.807C13.271 14.807 15.5073 12.6602 15.5073 10C15.5073 7.33986 13.271 5.19298 10.5 5.19298ZM10.5 13.1242C8.708 13.1242 7.2456 11.7203 7.2456 10C7.2456 8.2797 8.708 6.8758 10.5 6.8758C12.292 6.8758 13.7544 8.2797 13.7544 10C13.7544 11.7203 12.292 13.1242 10.5 13.1242ZM15.7124 3.87579C15.0654 3.87579 14.543 4.37736 14.543 4.99845C14.543 5.61954 15.0654 6.12111 15.7124 6.12111C16.3594 6.12111 16.8818 5.62189 16.8818 4.99845C16.882 4.85097 16.8519 4.7049 16.7932 4.56861C16.7345 4.43232 16.6484 4.30849 16.5397 4.2042C16.4311 4.09992 16.3021 4.01723 16.1601 3.96088C16.0182 3.90452 15.866 3.87561 15.7124 3.87579ZM20.2607 10C20.2607 8.70626 20.2729 7.42423 20.1973 6.13283C20.1216 4.63283 19.7651 3.30158 18.6226 2.2047C17.4775 1.10548 16.0933 0.765639 14.5308 0.692982C13.1831 0.620326 11.8476 0.632045 10.5024 0.632045C9.15478 0.632045 7.81933 0.620326 6.47411 0.692982C4.91161 0.765639 3.5249 1.10783 2.38232 2.2047C1.2373 3.30392 0.883295 4.63283 0.807611 6.13283C0.731927 7.42658 0.744134 8.70861 0.744134 10C0.744134 11.2914 0.731927 12.5758 0.807611 13.8672C0.883295 15.3672 1.23974 16.6985 2.38232 17.7953C3.52734 18.8945 4.91161 19.2344 6.47411 19.307C7.82177 19.3797 9.15722 19.368 10.5024 19.368C11.8501 19.368 13.1855 19.3797 14.5308 19.307C16.0933 19.2344 17.48 18.8922 18.6226 17.7953C19.7676 16.6961 20.1216 15.3672 20.1973 13.8672C20.2754 12.5758 20.2607 11.2938 20.2607 10ZM18.1123 15.5266C17.9341 15.9531 17.7192 16.2719 17.375 16.6C17.0308 16.9305 16.7012 17.1367 16.2568 17.3078C14.9727 17.7977 11.9233 17.6875 10.5 17.6875C9.07665 17.6875 6.0249 17.7977 4.74072 17.3102C4.29638 17.1391 3.96435 16.9328 3.62255 16.6024C3.27831 16.2719 3.06347 15.9555 2.88525 15.5289C2.37744 14.2938 2.49218 11.3664 2.49218 10C2.49218 8.63361 2.37744 5.70392 2.88525 4.47111C3.06347 4.04454 3.27831 3.7258 3.62255 3.39767C3.96679 3.06955 4.29638 2.86095 4.74072 2.68986C6.0249 2.20236 9.07665 2.31251 10.5 2.31251C11.9233 2.31251 14.9751 2.20236 16.2593 2.68986C16.7036 2.86095 17.0356 3.0672 17.3774 3.39767C17.7217 3.72814 17.9365 4.04454 18.1147 4.47111C18.6226 5.70392 18.5078 8.63361 18.5078 10C18.5078 11.3664 18.6226 14.2938 18.1123 15.5266Z" fill="#E8B8B8"/>
        </svg>  
        <h5>Будь в курсе</h5>
        <p>Отмечай нас в фото с тегом #БизнесиВласть и следи за последними обновлениями в Instagram</p>
        <a href="{{ $setting->inst }}">Подписаться</a>
    </section>
    <section class="sec4">
        <div class="container">
            @php
                $banner = $banners->whereNotNull('img_big')->random(1)[0];
            @endphp
            <a href="{{ $banner->url }}" target="blank" banner-big data-id="{{ $banner->id }}" data-type="big">
                <img src="{{ asset($banner->img_big) }}" alt="">
            </a>
        </div>
    </section>
    {{-- <section class="sec5">
        <div class="container">
            <div class="row">
                <div class="col-6">
                    <div class="posts">
                        <div class="head">
                            <p><img src="{{ asset('images/new-post.svg') }}" alt="">Свежий пост</p>
                        </div>
                        <div class="posts-lists">
                            @foreach ($new_posts as $key => $item)
                                    <div class="post @if($key === 0) first @endif">
                                        @if ($item->img)
                                            <img class="news_image" src="{{ asset($item->img) }}" alt="">
                                        @else
                                            <img class="news_image" src="{{ asset('/images/photos.png') }}" alt="">
                                        @endif
                                        <div class="body">
                                            <a href="{{ route('page.article', ['article' => $item->id]) }}"><h3>{{ $item->title }}</h3></a>
                                            <p class="news-desc">{{ $item->description }}</p>
                                            <div class="footer">
                                                <p>
                                                    <span>{{ Date::parse($item->published_at)->format('j F Y, H:i ') }}</span>
                                                    <span class="author">{{ $item->author->title }}</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    
                            @endforeach

                        </div>
                    </div>
                    <a href="{{ route('page.blogs') }}" class="more_news">Посмотреть всю ленту</a>
                </div>
                <div class="col-6">
                    <div class="posts">
                        <div class="head">
                            <p><img src="{{ asset('images/new-post.svg') }}" alt="">Самые популярные посты</p>
                        </div>
                        <div class="posts-tables">

                            @foreach ($best_posts as $item)
                                
                                <div class="post">
                                    @if ($item->img)
                                        <img class="news_image" src="{{ asset($item->img) }}" alt="">
                                    @else
                                        <img class="news_image" src="{{ asset('/images/photos.png') }}" alt="">
                                    @endif
                                    <div class="body">
                                        <a href="{{ route('page.article', ['article' => $item->id]) }}">
                                            <h3>{{ $item->title }}</h3>
                                        </a>
                                        <div class="footer">
                                            <p>
                                                <span>{{ Date::parse($item->published_at)->format('j F Y, H:i ') }}</span>
                                                <span class="author">{{ $item->author->title }}</span>
                                            </p>
                                        </div>
                                    </div>
                                </div>

                            @endforeach

                        </div>
                    </div>
                    <div class="spaser"></div>
                    <div class="posts">
                        <div class="head">
                            <p><img src="{{ asset('images/best_authors.svg') }}" alt="">Самые читаемые авторы</p>
                        </div>
                        <div class="posts-tables">
                            
                            @foreach ($best_authors as $item)
                                    
                                <div class="post">
                                    @if ($item->img)
                                        <img class="news_image" src="{{ asset($item->img) }}" alt="">
                                    @else
                                        <img class="news_image" src="{{ asset('/images/photos.png') }}" alt="">
                                    @endif
                                    <div class="body">
                                        <a href="{{ route('page.author_blogs', ['author' => $item->id]) }}">
                                            <h3>{{ $item->description }}</h3>
                                        </a>
                                        <div class="footer">
                                            <p>
                                                <span>{{ Date::parse($item->published_at)->format('j F Y, H:i ') }}</span>
                                                <span class="author">{{ $item->title }}</span>
                                            </p>
                                        </div>
                                    </div>
                                </div>

                            @endforeach
                        </div>
                    </div>
                    <a href="{{ route('page.authors') }}" class="more_news">Посмотреть всех авторов</a>
                </div>
            </div>
        </div>
    </section>

    <section class="mob_posts">
        <div class="spaser"></div>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2>Блоги</h2>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                @foreach ($new_posts as $mob_item)    
                    <div class="col-12">
                        <div class="news">
                            <a href="{{ route('page.article', ['article' => $mob_item->id]) }}">
                                <div class="img">
                                    <span class="label">{{ $mob_item->author_name }}</span>
                                    <img class="news_image" src="{{ asset($mob_item->img) }}" alt="">
                                </div> 
                                <h3>{{ $mob_item->title }}</h3>
                                <p class="news-desc">Автор: {{ $mob_item->author_name }}</p>
                            </a>
                            <div class="footer_news">
                                <span class="data">{{ Date::parse($mob_item->published_at)->format('j F Y, H:i ') }}</span>
                                <div class="control">
                                    <a href="#">
                                        <img src="{{ asset('images/eye.svg') }}" alt="">
                                        <span>{{ $mob_item->views->count() }}</span>
                                    </a>
                                    <a href="#!" class="like" data-id="{{ $mob_item->id }}" data-type="heading">
                                        <img src="{{ asset('images/like.svg') }}" alt="">
                                        <span>{{ $mob_item->likes->count() }}</span>
                                    </a>
                                    <a href="#" class="dislike" data-id="{{ $mob_item->id }}" data-type="heading">
                                        <img src="{{ asset('images/dislike.svg') }}" alt="">
                                        <span>{{ $mob_item->dislikes->count() }}</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section> --}}

    <section class="mob_rates">
        <div class="container">
            <div class="rates">
                <p>Курсы валют</p>
                <rates></rates>
            </div>
        </div>
    </section>

    <section class="sec4 mob">
        <div class="container">

            @php
                $banner = $banners->whereNotNull('img_small')->random(1)[0];
            @endphp
            <a href="{{ $banner->url }}" target="blank" banner-small data-id="{{ $banner->id }}" data-type="small">
                <img src="{{ asset($banner->img_small) }}" alt="">
            </a>

        </div>
    </section>
  @include('components.slider_arhives')
      <section class="sec7">
        <div class="container">
            <div class="head">
                <p><img src="{{ asset('images/partner.svg') }}" alt="">Наши партнёры</p>
            </div>
            <div class="row">
                @foreach ($parnters_slider as $item)    
                    <div class="col-2">
                        <img src="{{ asset($item->img) }}" alt="">
                    </div>
                @endforeach
            </div>
            <div class="row">
                <div class="col-12">
                    <a href="{{ route('page.partners') }}" class="more_news">Показать всех</a>
                </div>
            </div>
        </div>
    </section>
@endsection

