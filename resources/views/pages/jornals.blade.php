@extends('layouts.base')

@section('seo')
    <title>О нас | Вологодские новости</title>
    {{-- <title>{{ json_decode($setting->seo)->home->title }}</title>
    <meta name="description" content="{{ json_decode($setting->seo)->home->description }}" />
    <meta name="keywords" content="{{ json_decode($setting->seo)->home->keywords }}">
    <meta property="og:title" content="{{ json_decode($setting->seo)->home->title }}" />
    <meta property="og:description" content="{{ json_decode($setting->seo)->home->description }}" /> --}}
@endsection

@section('content')
    <section class="sec1">
        <div class="container">
            <img src="{{ asset('images/banner.png') }}" alt="">
        </div>
    </section>
    
    <section class="page">
        <div class="container">
            <div class="row">
                <div class="col-9">
                    <h1>ЗАГОЛОВОК</h1>
                    <p>Равным образом консультация с профессионалами из IT обеспечивает широкому кругу специалистов участие в формировании дальнейших направлений развитая системы массового участия. Значимость этих проблем настолько очевидна, что рамки и место обучения кадров обеспечивает актуальность экономической целесообразности принимаемых решений. Равным образом социально-экономическое развитие обеспечивает широкому кругу специалистов участие в формировании ключевых компонентов планируемого обновления?</p>
                    <img src="{{ asset('images/placeholder.jpg') }}" alt="">
                    <p>Разнообразный и богатый опыт дальнейшее развитие различных форм деятельности требует от нас системного анализа всесторонне сбалансированных нововведений. Не следует, однако, забывать о том, что курс на социально-ориентированный национальный проект требует от нас системного анализа системы масштабного изменения ряда параметров. Практический опыт показывает, что выбранный нами инновационный путь играет важную роль в формировании дальнейших направлений развития проекта. Не следует, однако, забывать о том, что постоянное информационно-техническое обеспечение нашей деятельности способствует подготовке и реализации существующих финансовых и административных условий! </p>
                    <p> С другой стороны социально-экономическое развитие позволяет выполнить важнейшие задания по разработке системы обучения кадров, соответствующей насущным потребностям. </p>   
                    <p>Разнообразный и богатый опыт дальнейшее развитие различных форм деятельности напрямую зависит от системы масштабного изменения ряда параметров! Соображения высшего порядка, а также постоянный количественный рост и сфера нашей активности требует определения и уточнения ключевых компонентов планируемого обновления. Соображения высшего порядка, а также выбранный нами инновационный путь влечет за собой процесс внедрения и модернизации форм...</p>
                    <div class="other_news">
                        <div class="head">
                            <p><img src="{{ asset('images/other_news.svg') }}" alt="">Другие статьи</p>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="news">
                                    <a href="">
                                        <span class="label">25.01.2019</span>
                                        <img class="news_image" src="{{ asset('images/width_news.png') }}" alt="">
                                        <h3>Космические технологии для ваших зубов</h3>
                                        <p>Потеряли зуб, два, три? Казалось бы, можно прожить и без них. Особенно...</p>
                                    </a>
                                </div>
                            </div>
                            <div class="col">
                                <div class="news">
                                    <a href="">
                                        <span class="label">25.01.2019</span>
                                        <img class="news_image" src="{{ asset('images/width_news.png') }}" alt="">
                                        <h3>Космические технологии для ваших зубов</h3>
                                        <p>Потеряли зуб, два, три? Казалось бы, можно прожить и без них. Особенно...</p>
                                    </a>
                                </div>
                            </div>
                            <div class="col">
                                <div class="news">
                                    <a href="">
                                        <span class="label">25.01.2019</span>
                                        <img class="news_image" src="{{ asset('images/width_news.png') }}" alt="">
                                        <h3>Космические технологии для ваших зубов</h3>
                                        <p>Потеряли зуб, два, три? Казалось бы, можно прожить и без них. Особенно...</p>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-3">
                    <div class="box-ad">
                        <img src="{{ asset('images/banners_min.jpg') }}" alt="">
                        <img src="{{ asset('images/banners_min.jpg') }}" alt="">
                        <img src="{{ asset('images/banners_min.jpg') }}" alt="">
                        <img src="{{ asset('images/banners_min.jpg') }}" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection