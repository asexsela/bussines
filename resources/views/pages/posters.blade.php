@extends('layouts.base')

@section('seo')
    <title>Афиша</title>
    {{-- <title>{{ json_decode($setting->seo)->home->title }}</title>
    <meta name="description" content="{{ json_decode($setting->seo)->home->description }}" />
    <meta name="keywords" content="{{ json_decode($setting->seo)->home->keywords }}">
    <meta property="og:title" content="{{ json_decode($setting->seo)->home->title }}" />
    <meta property="og:description" content="{{ json_decode($setting->seo)->home->description }}" /> --}}
@endsection

@section('content')

    <section class="sec1">
        <div class="container">
            @php
                $banner = $banners->whereNotNull('img_big')->random(1)[0];
            @endphp
            <a href="{{ $banner->url }}" target="blank" banner-big data-id="{{ $banner->id }}">
                <img src="{{ asset($banner->img_big) }}" alt="">
            </a>
        </div>
    </section>
    <section class="headline">
        <div class="container">
            <h1>Афиша</h1>
        </div>
    </section>
    
    <section class="posters">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="slider-year only">
                        <div class="sliders-years-box">
                            @foreach ($categories as $item)
                                <a class="year @if($item->title === $category->title) active @endif" href="{{ route('page.posters', ['type' => $item->title]) }}" role="tab">{{ $item->title }}</a>
                            @endforeach

                        </div>
                        <a href="" class="prev"><</a>
                        <a href="" class="next">></a>
                    </div>
                </div>
                <div class="col-10 offset-1 mt-4">
    
                    <div class="row">

                        @foreach ($posters as $item)    
                            <div class="col-12 col-lg-6">
                                <div class="partner_item poster">
                                    @if ($item->img)
                                        <img src="{{ asset($item->img) }}" alt="">
                                    @else
                                        <img src="{{ asset('/images/photos.png') }}" alt="">
                                    @endif
                                    <div class="body">
                                        <div class="desc">
                                            <h3>{{ $item->title }}</h3>
                                            <p class="news-desc">{{ $item->description }}</p>
                                        </div>
                                        <a href="{{ route('page.poster.page', ['poster' => $item->id]) }}" class="more_news">Подробнее</a>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    </section>

    
    <section class="pagination">
        <div class="container">
            {{-- pagination --}}
        </div>
    </section>


    <div class="space"></div>

@endsection