@extends('layouts.base')

@section('seo')
    @if ($poster->seoTitle !== '')
        <title>{{ $poster->seoTitle }} | Вологодские новости</title>
        <meta property="og:title" content="{{ $poster->seoTitle }} | Вологодские новости" />
    @else
        <title>{{ $poster->title }} | Вологодские новости</title>
        <meta property="og:title" content="{{ $poster->title }} | Вологодские новости" />
    @endif
    @if ($poster->seoDesc)
        <meta name="description" content="{{ $poster->seoDesc }}" />
        <meta property="og:description" content="{{ $poster->seoDesc }}" />
    @endif
    @if ($poster->seoKeywords)
        <meta name="keywords" content="{{ $poster->seoKeywords }}">
    @endif
    @if ($poster->img)
        <meta property="og:image" content="{{ asset($poster->img) }}"/>
        <link rel="image_src" href="{{ asset($poster->img) }}" />
    @else
        <meta property="og:image" content="{{ asset('images/site_logo.png') }}"/>
        <link rel="image_src" href="{{ asset('images/site_logo.png') }}" />
    @endif
@endsection

@section('content')
    {{-- <section class="headline">
        <div class="container bc">
            <div class="breadcrumbs">
                <a href="/">Главная</a>
                <a href="{{ route('page.posters') }}">Афиша</a>
                <span>{{ $poster->title }}</span>
            </div>
            <h1></h1>
        </div>
    </section> --}}

    <section class="poster pt-4">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-9">
                    <div class="header">
                        <p>
                            <span>{{ Date::parse($poster->created_at)->format('j F Y, H:i ') }}</span>
                        </p> 
                        @if ($poster->tags)    
                            <p style="width: 55%;">
                                @php
                                    $tags = explode(',', $poster->tags);
                                @endphp
                                @foreach ($tags as $tag)
                                    <a href="#!">#{{ trim($tag) }}</a>
                                @endforeach
                            </p>                        
                        @endif                     
                    </div>
                    {{-- <img class="poster_image" src="{{ asset('images/width_news.png') }}" alt=""> --}}
                    <h3>{{ $poster->title }}</h3>
                    <div class="body">
                        {!! $poster->body !!}
                    </div>
                    <div class="clear-space"></div>

                    <div class="spaser"></div>
                    <div class="footer_poster">
                        <div class="control">
                            {{-- <a href="#">
                                <img src="{{ asset('images/eye.svg') }}" alt="">
                                <span>120</span>
                            </a>
                            <a href="#">
                                <img src="{{ asset('images/like.svg') }}" alt="">
                                <span>15</span>
                            </a>
                            <a href="#">
                                <img src="{{ asset('images/dislike.svg') }}" alt="">
                                <span>10</span>
                            </a> --}}
                            <div class="share">
                                <span>Поделиться</span>
                                <a href="#!" id="vk"></a>
                                <a href="#!" id="fb" data-url="{{ config('app.url') }}" data-desc="{{ $poster->description }}" data-title="{{ $poster->title }}" data-img="{{ asset($poster->img) }}"><img src="{{ asset('images/fb.svg') }}" alt=""></a>
                            </div>
                        </div>
                        <div class="buttons">
                            @isset($poster->programm_img)    
                                <a data-fancybox="programm" href="{{ asset($poster->programm_img) }}" class="more_news">
                                    <svg width="17" height="19" viewBox="0 0 17 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M14.625 2.625H13.75V0.875H12V2.625H5V0.875H3.25V2.625H2.375C1.4125 2.625 0.625 3.4125 0.625 4.375V16.625C0.625 17.5875 1.4125 18.375 2.375 18.375H14.625C15.5875 18.375 16.375 17.5875 16.375 16.625V4.375C16.375 3.4125 15.5875 2.625 14.625 2.625ZM14.625 16.625H2.375V7.875H14.625V16.625ZM2.375 6.125V4.375H14.625V6.125H2.375ZM4.125 9.625H12.875V11.375H4.125V9.625ZM4.125 13.125H10.25V14.875H4.125V13.125Z" fill="#A33D32"/>
                                    </svg>
                                    Программа мероприятия
                                </a>
                            @endisset
                            {{-- <a href="#" class="more_news">
                                <svg width="16" height="17" viewBox="0 0 16 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M7.99977 7.25C5.72977 7.25 5.26977 3.81 5.26977 3.81C4.99977 2.02 5.81977 0 7.96977 0C10.1298 0 10.9498 2.02 10.6798 3.81C10.6798 3.81 10.2698 7.25 7.99977 7.25ZM7.99977 9.82L10.7198 8C13.1098 8 15.2398 10.33 15.2398 12.53V15.02C15.2398 15.02 11.5898 16.15 7.99977 16.15C4.34977 16.15 0.759766 15.02 0.759766 15.02V12.53C0.759766 10.28 2.69977 8.05 5.22977 8.05L7.99977 9.82Z" fill="#A33D32"/>
                                </svg>
                                Забронировать
                            </a> --}}
                        </div>
                    </div>
                </div>
                <div class="d-none d-lg-block col-3">
                    <div class="box-ad">
                        @php
                            $count = ($count_banner < 6) ? $count_banner : 6;
                        @endphp
                        @foreach ($banners->whereNotNull('img_small')->random($count) as $item)
                            <a href="{{ $item->url }}" tagret="blank" banner-small data-id="{{ $item->id }}">
                                <img src="{{ asset($item->img_small) }}" alt="">
                            </a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="sec6 vid d-none d-md-block">
        <div class="container">
            <div class="head">
                <p><img src="{{ asset('images/media.svg') }}" alt="">Дневник</p>
                {{-- <span class="pagingInfo v">
                    <a href="#" class="prev"><</a>
                    <span class="page_count_video"></span>
                    <a href="#" class="next">></a>
                </span> --}}
            </div>
            <div class="slider_video">
                @foreach ($notes as $note)     
                    <div class="news min">
                        <a href="{{ route('page.poster.note', ['note' => $note->id]) }}">
                            <span class="label">{{ Date::parse($note->created_at)->format('d.m.Y') }}</span>
                            @if ($note->img)
                                <img class="news_image" src="{{ asset($note->img) }}" alt="">
                            @else
                                <img class="news_image" src="{{ asset('/images/photos.png') }}" alt="">
                            @endif
                            <h3 class="news-title">{{ $note->title }}</h3>
                            <p class="news-desc">{{ $note->description }}</p>
                        </a>
                        <div class="footer_news">
                            <span class="data">{{ Date::parse($note->created_at)->format('j F Y, H:i ') }}</span>
                            
                        </div>
                    </div>
                @endforeach
            </div>
            @if ($notes->count() <= 0)
                <p style="text-align:center;">Записей не найдено</p>
            @endif
        </div>
    </section>
    <section class="sec6 vid d-none d-lg-block">
        <div class="container">
            <div class="head">
                <p><img src="{{ asset('images/media.svg') }}" alt="">Другие материалы</p>
            </div>
            <div class="slider_outher">
                @foreach ($outher as $item)     
                    <div class="news min">
                        <a href="{{ route('page.poster.page', ['poster' => $item->id]) }}">
                            <span class="label">{{ Date::parse($item->created_at)->format('d.m.Y') }}</span>
                            @if ($item->img)
                                <img class="news_image" src="{{ asset($item->img) }}" alt="">
                            @else
                                <img class="news_image" src="{{ asset('/images/photos.png') }}" alt="">
                            @endif
                            <h3 class="news-title">{{ $item->title }}</h3>
                            <p class="news-desc">{{ $item->description }}</p>
                        </a>
                        <div class="footer_news">
                            <span class="data">{{ Date::parse($item->created_at)->format('j F Y, H:i ') }}</span>
                            
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>

    <div class="outher_news d-block d-lg-none">
        <div class="container">
            <div class="row mt-5">
                <div class="col-12">
                    <div class="head">
                        <p>Дневник</p>
                    </div>
                </div>
        
                <div class="box_news">
                    @foreach ($notes as $note)     
                        <div class="news min">
                            <a href="{{ route('page.poster.note', ['note' => $note->id]) }}">
                                <span class="label">{{ Date::parse($note->created_at)->format('d.m.Y') }}</span>
                                @if ($note->img)
                                    <img class="news_image" src="{{ asset($note->img) }}" alt="">
                                @else
                                    <img class="news_image" src="{{ asset('/images/photos.png') }}" alt="">
                                @endif
                                <h3 class="news-title">{{ $note->title }}</h3>
                                <p class="news-desc">{{ $note->description }}</p>
                            </a>
                            <div class="footer_news">
                                <span class="data">{{ Date::parse($note->created_at)->format('j F Y, H:i ') }}</span>
                                
                            </div>
                        </div>
                    @endforeach
                </div>
                @if ($notes->count() <= 0)
                    <div class="col-12">
                        <p style="text-align:center;">Записей не найдено</p>
                    </div>
                @endif
            </div>
        </div>
            
    </div>

    <div class="outher_news d-block d-lg-none">
        <div class="container">
            <div class="row mt-5">
                <div class="col-12">
                    <div class="head">
                        <p>Другие материалы</p>
                    </div>
                </div>
        
                <div class="box_news">
                    @foreach ($outher as $item)     
                        <div class="news min">
                            <a href="{{ route('page.poster.page', ['poster' => $item->id]) }}">
                                <span class="label">{{ Date::parse($item->created_at)->format('d.m.Y') }}</span>
                                @if ($item->img)
                                    <img class="news_image" src="{{ asset($item->img) }}" alt="">
                                @else
                                    <img class="news_image" src="{{ asset('/images/photos.png') }}" alt="">
                                @endif
                                <h3 class="news-title">{{ $item->title }}</h3>
                                <p class="news-desc">{{ $item->description }}</p>
                            </a>
                            <div class="footer_news">
                                <span class="data">{{ Date::parse($item->created_at)->format('j F Y, H:i ') }}</span>
                                
                            </div>
                        </div>
                    @endforeach
                </div>
        
            </div>
        </div>
            
    </div>

    <section class="pagination">
        <div class="container">
            {{-- pagination --}}
        </div>
    </section>
    
    @include('components.slider_arhives')
    <div class="space"></div>

@endsection

@section('scripts')
    <script>


        document.getElementById('vk').innerHTML = VK.Share.button({
            url: "{{ config('app.url') }}",
            title:" {{ $poster->title }}",
            image: "{{ asset($poster->img) }}",
            noparse: true,
            
        }, {
            type: 'custom', 
            text: `<img src="{{ asset('images/vk.svg') }}" alt="">`
        });


    </script>
@endsection