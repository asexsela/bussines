@extends('layouts.base')

@section('seo')
    @if ($note->seoTitle)
        <title>{{ $note->seoTitle }} | Вологодские новости</title>
        <meta property="og:title" content="{{ $note->seoTitle }} | Вологодские новости" />
    @else
        <title>{{ $note->title }} | Вологодские новости</title>
        <meta property="og:title" content="{{ $note->title }} | Вологодские новости" />
    @endif
    @if ($note->seoDesc)
        <meta name="description" content="{{ $note->seoDesc }}" />
        <meta property="og:description" content="{{ $note->seoDesc }}" />
    @endif
    @if ($note->seoKeywords)
        <meta name="keywords" content="{{ $note->seoKeywords }}">
    @endif
    @if ($note->img)
        <meta property="og:image" content="{{ asset($note->img) }}"/>
        <link rel="image_src" href="{{ asset($note->img) }}" />
    @else
        <meta property="og:image" content="{{ asset('images/site_logo.png') }}"/>
        <link rel="image_src" href="{{ asset('images/site_logo.png') }}" />
    @endif
@endsection

@section('content')
    <div class="space"></div>
    <section class="headline">
        <div class="container bc">
            <div class="breadcrumbs">
                <a href="/">Главная</a>
                <a href="{{ route('page.posters') }}">Афиша</a>
                <a href="{{ route('page.poster.page', ['poster' => $note->poster->id]) }}">{{ $note->poster->title }}</a>
                <span>{{ $note->title }}</span>
            </div>
        </div>
    </section>

    <section class="headline">
    <div class="container">
        <h1 style="padding-top: 20px;">{{ $note->title }}</h1>
    </div>
    
    <section class="page">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-9">
                    {!! $note->body !!}

                    <div class="other_news d-none d-md-block">
                        <div class="head">
                            <p><img src="{{ asset('images/other_news.svg') }}" alt="">Другие статьи</p>
                        </div>
                        <div class="row">
                            @foreach ($outher as $item)    
                                <div class="col">
                                    <div class="news">
                                        <a href="{{ route('page.poster.programm', ['note' => $item->id]) }}">
                                            <span class="label">{{ Date::parse($item->created_at)->format('d.m.Y') }}</span>
                                            @if ($item->img)
                                                <img class="news_image" src="{{ asset($item->img) }}" alt="">
                                            @else
                                                <img class="news_image" src="{{ asset('/images/photos.png') }}" alt="">
                                            @endif
                                            <h3 class="news-title">{{ $item->title }}</h3>
                                            <p class="news-desc">{{ $item->description }}</p>
                                        </a>
                                    </div>
                                </div>
                            @endforeach
                            
                        </div>
                    </div>

                    <div class="outher_news d-block d-md-none">
                        <div class="row mt-5">
                            <div class="col-12">
                                <div class="head">
                                    <p>
                                        Похожие материалы
                                    </p>
                                </div>
                            </div>
    
                            <div class="box_news">
                                @foreach ($outher as $item)    
                                    <div class="news">
                                        <a href="{{ route('page.poster.programm', ['note' => $item->id]) }}">
                                            <span class="label">{{ Date::parse($item->created_at)->format('d.m.Y') }}</span>
                                            @if ($item->img)
                                                <img class="news_image" src="{{ asset($item->img) }}" alt="">
                                            @else
                                                <img class="news_image" src="{{ asset('/images/photos.png') }}" alt="">
                                            @endif
                                            <h3 class="news-title">{{ $item->title }}</h3>
                                            <p class="news-desc">{{ $item->description }}</p>
                                        </a>
                                    </div>
                                @endforeach
                            </div>
    
                        </div>
                    </div>

                </div>
                <div class="col-3 d-none d-lg-block">
                    <div class="box-ad">
                        @php
                            $count = ($count_banner < 4) ? $count_banner : 4;
                        @endphp
                        @foreach ($banners->whereNotNull('img_small')->random($count) as $item)
                            <a href="{{ $item->url }}" tagret="blank" banner-small data-id="{{ $item->id }}">
                                <img src="{{ asset($item->img_small) }}" alt="">
                            </a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection