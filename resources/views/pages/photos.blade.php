@extends('layouts.base')

@section('seo')
    <title>Фото | Вологодские новости</title>
@endsection

@section('content')
    <section class="sec1">
        <div class="container">
            @php
                $banner = $banners->whereNotNull('img_big')->random(1)[0];
            @endphp
            <a href="{{ $banner->url }}" target="blank" banner-big data-id="{{ $banner->id }}">
                <img src="{{ asset($banner->img_big) }}" alt="">
            </a>
        </div>
    </section>
    <section class="headline">
        <div class="container">
            <h1>Фото</h1>
        </div>
    </section>
    
    <section class="photos">
        <div class="container">
            <div class="row">
                <div class="col-9">
                    @foreach ($albums as $key => $album)    
                        <div class="row pt-4">
                            <div class="col-12">
                                <h2>{{ $album->title }} <span>{{ Date::parse($album->custom_date)->format('d.m.Y') }}</span></h2>
                                <div class="row">
                                    <div class="col-6">
                                        <div class="left">
                                            @if ($album->img)
                                                <a data-fancybox="gallery-{{ $key }}" href="{{ asset($album->img) }}">
                                                    <img src="{{ asset($album->img) }}" alt="">
                                                </a>
                                            @else
                                                <img src="{{ asset('/images/photos.png') }}" alt="">
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="right">
                                            @foreach ($album->previews as $img)
                                                <a data-fancybox="gallery-{{ $key }}" href="{{ asset($img->src) }}">
                                                    <img src="{{ asset($img->src) }}" alt="">
                                                </a>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="space"></div>
                                <a href="{{ route('page.photo', ['album' => $album->id]) }}" class="btn1">Открыть альбом</a>
                                <div class="space"></div>
                                <div class="spaser"></div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="col-3 d-none d-lg-block">
                    <div class="box-ad">
                        @php
                            $count = ($count_banner < 6) ? $count_banner : 6;
                        @endphp
                        @foreach ($banners->whereNotNull('img_small')->random($count) as $item)
                            <a href="{{ $item->url }}" target="blank" banner-small data-id="{{ $item->id }}">
                                <img src="{{ asset($item->img_small) }}" alt="">
                            </a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="mob_photo">
        <div class="container">
            <div class="box_mob_photo">
                @foreach ($albums as $item)
                    <div class="item">
                        <p>{{ $item->title }}</p>
                        <div class="img">
                            @if ($item->img)
                                <img src="{{ asset($item->img) }}" alt="">
                            @else
                                <img src="{{ asset('/images/photos.png') }}" alt="">
                            @endif
                            @foreach ($item->previews as $img)
                                <img src="{{ asset($img->src) }}" alt="">
                            @endforeach
                        </div>
                        <a href="{{ route('page.photo', ['album' => $item->id]) }}" class="btn1">Открыть альбом</a>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
    
    <div class="section pagination">
        <div class="container">
            {{ $albums->links() }}
        </div>
    </div>
    <div class="space"></div>
@endsection
