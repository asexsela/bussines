@extends('layouts.base')

@section('seo')
    <title>Поиск | Вологодские новости</title>
@endsection

@section('content')

    <section class="sec1">
        <div class="container">
            @php
                $banner = $banners->whereNotNull('img_big')->random(1)[0];
            @endphp
            <a href="{{ $banner->url }}" target="blank" banner-big data-id="{{ $banner->id }}">
                <img src="{{ asset($banner->img_big) }}" alt="">
            </a>
        </div>
    </section>
    <section class="headline">
        <div class="container">
            <h1></h1>
        </div>
    </section>
    <section class="searching">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-9">
                   <form id="search" class="toSearch">
                        <input type="text" name="search" placeholder="Поиск" value="{{ $search }}">
                        <button id="search-button"></button>
                   </form>
                </div>
                    {{-- <div class="container">
                        <div class="box-control">
                            <a class="date" href="#!">
                                <img src="{{ asset('images/date.svg') }}" alt="">
                                <input type="text" class="date_pickers" placeholder="Выбрать дату" data-url="{{ route('page.search', ['type' => 'date']) }}"></span>
                            </a>
                            
                        </div>
                    </div> --}}
                <div class="col-12 col-lg-9">
                    @if ($search || $tag)
                        <div class="box_result">
                            @if (count($result['news']) > 0)    
                                @foreach ($result['news'] as $item)
                                    <div class="item item_search">
                                        <div class="date">{{ Date::parse($item->created_at)->ago() }}</div>
                                        <div class="body">
                                            @if ($type == 'search')
                                                @if ($item->heading)
                                                    <span class="label">{{ $item->heading->name }}</span>
                                                @else
                                                    <span class="label">Новость</span>
                                                @endif
                                            @else
                                                <span class="label">#{{ $tag }}</span>
                                            @endif
                                            <a href="@if($item->isType() === 'heading-page/') {{ url($item->isType().$item->heading->slug.'/'.$item->id) }} @else {{ url($item->isType().$item->id) }} @endif">{{ $item->title }}</a>
                                        </div>
                                    </div>
                                @endforeach
                                
                            @else

                                <p class="no-str">Результатов не найдено</p>

                            @endif
                        </div>
                        @if (count($result['news']) > 0)
                            <p class="text-center mt-4 mb-5">
                                <a 
                                    href="#!" 
                                    data-url="{{ route('more_search') }}" 
                                    data-count="{{ $count }}" 
                                    data-search="{{ $search }}" 
                                    data-type="{{ $type }}" 
                                    data-tag="{{ $tag ?? '' }}" 
                                    class="search_more more_news"
                                >Показать больше</a>
                            </p>
                        @endif
                    @else
                        <p class="no-str">Введите поисковую фразу</p>
                    @endif
                </div>
                <div class="col-3 d-none d-lg-block">
                    <div class="box-ad">
                        @php
                            $count = ($count_banner < 4) ? $count_banner : 4;
                        @endphp
                        @foreach ($banners->whereNotNull('img_small')->random($count) as $item)
                            <a href="{{ $item->url }}" tagret="blank" banner-small data-id="{{ $item->id }}">
                                <img src="{{ asset($item->img_small) }}" alt="">
                            </a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
    

    <div class="space"></div>

@endsection

@section('scripts')

    <script>
        $(function() {
            let url = "{{ request()->route()->getName() }}"

            if(url === 'page.search') {

                function escapeRegExp(str) {
                    return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
                }

                function str_replace() {

                    var str = '{{ $search }}';
                    var reg = new RegExp( escapeRegExp(str).replace(/\s+/g,'|') , 'gi' );
                    let strIn = $('.item');
        
                    for (let i = 0; i < strIn.length; i++) {
                        const sting = $(strIn[i]).find('a').text(),
                              index = $(strIn[i]).find('a').text().indexOf(str);
        
                        $(strIn[i]).find('a').html(sting.replace(reg, '<span>$&</span>'))
                        
                    }

                    return 1;
                }
                
                str_replace();
            }

            $('.search_more').click(function(e) {
            e.preventDefault();

            var count = $(".item_search").length,
                page = parseInt(count / 10),
                search = $(this).data('search'),
                type = $(this).data('type'),
                tag = $(this).data('tag'),
                all = $(this).data('count'),
                token = $('[name="csrf-token"]').attr('content'),
                url = $(this).data('url');
            
                // console.log($(".item"))
            $.ajax({
                url:url,
                type:'post',
                dataType:'json',
                headers: {'X-CSRF-TOKEN': token},
                data:{
                    count: count,
                    page: page,
                    search: search,
                    type: type,
                    tag: tag,
                },
                beforeSend:function(){

                    $(".search_more").html('Загрузка...');
                },
                success:function(response){
                    $(".box_result").append(response);
                    if(type == 'search') {
                        str_replace()
                    }
                    if ($(".item_search").length >= all) {
                        $(".search_more").remove();
                    } else {
                        $(".search_more").html('Показать больше');
                    }
                   
                }
            });
        })
        })
    </script>
    
@endsection