@extends('layouts.base')

@section('seo')
    <title>Голосования | Вологодские новости</title>
@endsection

@section('content')
    <section class="sec1">
        <div class="container">
            @php
                $banner = $banners->whereNotNull('img_big')->random(1)[0];
            @endphp
            <a href="{{ $banner->url }}" target="blank" banner-big data-id="{{ $banner->id }}">
                <img src="{{ asset($banner->img_big) }}" alt="">
            </a>
        </div>
    </section>
    <section class="headline">
        <div class="container">
            <h1>Голосования</h1>
        </div>
    </section>
    
    <section class="voting">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-9">
                    <voiting></voiting>
                </div>
                <div class="col-3 d-none d-lg-block">
                    <div class="box-ad">
                        @php
                            $count = ($count_banner < 6) ? $count_banner : 6;
                        @endphp
                        @foreach ($banners->whereNotNull('img_small')->random($count) as $item)
                            <a href="{{ $item->url }}" target="blank" banner-small data-id="{{ $item->id }}">
                                <img src="{{ asset($item->img_small) }}" alt="">
                            </a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <section class="pagination">
        <div class="container">
            {{-- pagination --}}
        </div>
    </section>

    @include('components.slider_arhives')
    <div class="space"></div>

@endsection