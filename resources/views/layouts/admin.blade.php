<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Панель администратора</title>

    @include('components.admin.head')

</head>
<body>
    
    @auth
        <div class="app" id="app">
            @include('components.admin.sidebar')
            @include('components.admin.header')
            <div class="loader">
                <div class="preloader-wrapper active">
                    <div class="spinner-layer spinner-red-only">
                        <div class="circle-clipper left">
                            <div class="circle"></div>
                        </div><div class="gap-patch">
                            <div class="circle"></div>
                        </div><div class="circle-clipper right">
                            <div class="circle"></div>
                        </div>
                    </div>
                </div>
            </div>
    @endauth
    @yield('content')

    @auth
        </div>
    @endauth 

    @include('components.admin.scripts')

    @yield('scripts')




</body>
</html>