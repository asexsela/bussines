<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        @yield('seo')
        <meta property="og:url" content="{{ config('app.url') }}" />
        <meta property="og:locale" content="{{ config('app.faker_locale') }}" />
        <link href='{{ url('sitemap.xml') }}' rel='alternate' title='Sitemap' type='application/rss+xml'/>

        <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('favicons/apple-touch-icon.png') }}">
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicon/favicon-32x32.png') }}">
        <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('favicon/favicon-16x16.png') }}">
        <link rel="manifest" href="{{ asset('favicon/site.webmanifest') }}">
        <link rel="mask-icon" href="{{ asset('favicon/safari-pinned-tab.svg') }}" color="#5bbad5">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="theme-color" content="#c35656">

        <link rel="stylesheet" href="{{ asset('css/slick.css') }}">
        <link rel="stylesheet" href="{{ asset('css/jquery.fancybox.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/bootstrap-datepicker.min.css') }}">
        <link rel="stylesheet" href="{{ mix('css/app.css') }}">
        <script type="text/javascript" src="https://vk.com/js/api/share.js?93" charset="windows-1251"></script>


        @include('components.header-metrick')


    </head>
    <body>
        <div class="loader">
            <div class="spinner-grow"></div>
            <div class="spinner-grow"></div>
            <div class="spinner-grow"></div>
        </div>
        <div id="app">
            @include('components.header')
            <main>
                @yield('content')
            </main>
            @include('components.footer')
        </div>
        <div class="scrollToTop">
            <img src="{{ asset('images/scroll_to_top.svg') }}" alt="">
        </div>
        <script src="{{ mix('js/app.js') }}"></script>
        <script src="{{ asset('js/slick.min.js') }}"></script>
        <script src="{{ asset('js/jquery.fancybox.min.js') }}"></script>
        <script src="{{ asset('js/bootstrap-datepicker.min.js') }}"></script>
        <script src="{{ asset('locales/bootstrap-datepicker.ru.min.js') }}"></script>
        <script src="{{ asset('node_modules/tinymce/tinymce.js') }}"></script>
        <script src="{{ asset('js/jquery.lazyload.min.js') }}"></script>

        <script>
            $(document).ready(function(){
                $('.slider_video').on('init reInit afterChange', function(event, slick, currentSlide, nextSlide){
                    let count = slick.currentSlide,
                        num,
                        all = Math.ceil(slick.slideCount / 4);

                        if(count === 0) {
                            num = 1
                        } else {
                            num = count / 4 + 1
                        }

                    let res = num + '/' + all;
                    $('.page_count_video').text(res)
                });

                $('.slider_video').slick({
                    infinite: false,
                    slidesToShow: 4,
                    slidesToScroll: 4,
                    dots: false,
                    prevArrow: $('.v > .prev'),
                    nextArrow: $('.v > .next'),
                });

                $('.slider_img').on('init reInit afterChange', function(event, slick, currentSlide, nextSlide){
                    let count = slick.currentSlide,
                        num,
                        all = Math.ceil(slick.slideCount / 4);

                        if(count === 0) {
                            num = 1
                        } else {
                            num = count / 4 + 1
                        }
                        
                    let res = num + '/' + all;
                    $('.page_count_img').text(res)
                });

                $('.slider_img').slick({
                    infinite: false,
                    slidesToShow: 4,
                    slidesToScroll: 4,
                    dots: false,
                    prevArrow: $('.i > .prev'),
                    nextArrow: $('.i > .next'),
                });

                $('.slider_outher').on('init reInit afterChange', function(event, slick, currentSlide, nextSlide){
                    let count = slick.currentSlide,
                        num,
                        all = Math.ceil(slick.slideCount / 4);

                        if(count === 0) {
                            num = 1
                        } else {
                            num = count / 4 + 1
                        }
                        
                    let res = num + '/' + all;
                    $('.page_count_outher').text(res)
                });

                $('.slider_outher').slick({
                    infinite: false,
                    slidesToShow: 4,
                    slidesToScroll: 4,
                    dots: false,
                    prevArrow: $('.o > .prev'),
                    nextArrow: $('.o > .next'),
                });

                let big_banner = $('[banner-big]'),
                    small_banner = $('[banner-small]'),
                    arr_big = [],
                    arr_small = [];

                function setViewBanner(arr, type, url) {
                    let token = $('[name="csrf-token"]').attr('content');
                    $.ajax({
                        type: 'post',
                        url: url,
                        headers: {'X-CSRF-TOKEN': token},
                        data: {
                            uid: localStorage.getItem('uuid'),
                            arr: arr,
                            type: type
                        },
                        success: function(data) {
                            // console.log(data)
                        },
                        error: function(err) {
                            console.log(err)
                        }
                    });
                }

                for (let i = 0; i < big_banner.length; i++) {
                    const el = big_banner[i],
                        id = $(el).data('id');
                        arr_big.push(id)
                }

                for (let i = 0; i < small_banner.length; i++) {
                    const el = small_banner[i],
                        id = $(el).data('id');
                        arr_small.push(id)
                }

                if(arr_big.length) {
                    setViewBanner(arr_big, 'big', "{{ route('page.banner.view') }}")
                }
                if(arr_small.length) {

                    setViewBanner(arr_small, 'small', "{{ route('page.banner.view') }}")
                }

            $('a[banner-big]').click(function(e) {
                    e.preventDefault();
                    let id = $(this).data('id'),
                        url = $(this).attr('href');
                    setViewBanner(id, 'big', "{{ route('page.banner.click') }}")
                    window.open(url, 'blank');
            })

            $('a[banner-small]').click(function(e) {
                    e.preventDefault();
                    let id = $(this).data('id'),
                        url = $(this).attr('href');
                    setViewBanner(id, 'small', "{{ route('page.banner.click') }}")
                    window.open(url, 'blank');
            })
                

            });
        </script>

        @yield('scripts')
        @yield('style')


        @include('components.footer-metrick')
    </body>

</html>
