<footer>
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-4">
                {{-- <div class="soc_footer mob">
                    <a href=""><img src="{{ asset('images/vk_footer.svg') }}" alt=""></a>
                    <a href=""><img src="{{ asset('images/fb_footer.svg') }}" alt=""></a>
                    <a href=""><img src="{{ asset('images/inst_footer.svg') }}" alt=""></a>
                    <a href=""><img src="{{ asset('images/youtube_footer.svg') }}" alt=""></a>
                </div> --}}
                <div class="footer-contacts-box">
                    <img style="filter: brightness(0) invert(1);width:100%;" src="{{ asset($setting->logo_header) }}" alt="">
                    <p><span>Тел. редакции: </span> {{ $setting->phones }}</p>
                    <p><span>E-mail: </span>{{ $setting->mails }}</p>
                    <p><span>Адрес: </span>{{ $setting->address }}</p>
                    <p class="tel_me" >Заметили ошибку? <a href="#!" data-toggle="modal" data-target="#sendMe">Написать нам</a></p>
                </div>
            </div>
            <div class="col-8">
                <div class="box-footer_menu">
                    {{ menu(5, 'components.menus.footer') }}
                    {{ menu(6, 'components.menus.footer') }}
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="spaser"></div>
        <div class="row">
            <div class="col-4">
                <p class="copy">© 2019 «Бизнес и Власть». Все права защищены.</p>                
            </div>
            <div class="col-8">
                <div class="soc_footer">
                    <a href="{{ $setting->vk }}"><img src="{{ asset('images/vk_footer.svg') }}" alt=""></a>
                    <a href="{{ $setting->fb }}"><img src="{{ asset('images/fb_footer.svg') }}" alt=""></a>
                    <a href="{{ $setting->inst }}"><img src="{{ asset('images/inst_footer.svg') }}" alt=""></a>
                    <a href="{{ $setting->youtube }}"><img src="{{ asset('images/youtube_footer.svg') }}" alt=""></a>
                </div>
            </div>
        </div>
    </div>
</footer>
<section class="mob_footer">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="soc_footer">
                    <a href="{{ $setting->vk }}"><img src="{{ asset('images/vk_footer.svg') }}" alt=""></a>
                    <a href="{{ $setting->fb }}"><img src="{{ asset('images/fb_footer.svg') }}" alt=""></a>
                    <a href="{{ $setting->inst }}"><img src="{{ asset('images/inst_footer.svg') }}" alt=""></a>
                    <a href="{{ $setting->youtube }}"><img src="{{ asset('images/youtube_footer.svg') }}" alt=""></a>
                </div>
                <div class="col-12">
                    <div class="mob_menu">
                        {{ menu(5, 'components.menus.footer_mob') }}
                        {{ menu(6, 'components.menus.footer_mob') }}
                    </div>
                </div>
                <div class="col-12">
                    <div class="access">
                        <p>Любое использование материалов<br>допускается только с согласия редакции</p>
                    </div>
                </div>
                <div class="col-12">
                    <p class="copy">© 2019 «Бизнес и Власть». Все права защищены.</p>                
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="sendMe" tabindex="-1" role="dialog" aria-labelledby="sendMeLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="sendMeLabel">Отправить сообщение</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form id="sendme-mail">
                @csrf
                <div class="form-group">
                    <label for="mail-name">Имя</label>
                    <input type="text" class="form-control" id="mail-name" name="name" placeholder="Введите имя" required>
                </div>
                <div class="form-group">
                    <label for="mail-email">Email</label>
                    <input type="email" class="form-control" id="mail-email" name="email" placeholder="Ведите email" required>
                </div>
                <div class="form-group">
                    <label for="mail-body">Сообщение</label>
                    <textarea class="form-control" id="mail-body" rows="3" name="body" required></textarea>
                </div>
                <button type="sybmit" class="btn sendme-button">Отправить</button>
            </form>
            
        </div>

        <div class="loader">
            <div class="spinner-grow"></div>
            <div class="spinner-grow"></div>
            <div class="spinner-grow"></div>
        </div>
        <div class="success">
            <div class="f-modal-alert">
                <div class="f-modal-icon f-modal-success animate">
                    <span class="f-modal-line f-modal-tip animateSuccessTip"></span>
                    <span class="f-modal-line f-modal-long animateSuccessLong"></span>
                    <div class="f-modal-placeholder"></div>
                    <div class="f-modal-fix"></div>
                </div>
            </div>
            Ваше сообщение успешно отправлено
        </div>
        <div class="error">
            <div class="f-modal-alert">
                <div class="f-modal-icon f-modal-error animate">
                    <span class="f-modal-x-mark">
                        <span class="f-modal-line f-modal-left animateXLeft"></span>
                        <span class="f-modal-line f-modal-right animateXRight"></span>
                    </span>
                    <div class="f-modal-placeholder"></div>
                    <div class="f-modal-fix"></div>
                </div>
            </div>
            Ошибка отправки сообщения
        </div>
      </div>
    </div>
</div>