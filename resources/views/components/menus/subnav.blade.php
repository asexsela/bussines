<a href="#!">{{ $data['name'] }}</a>
<ul class="submenu">
    @foreach ($data['items'] as $item)
        <li><a href="{{ $item->url }}">{{ $item->name }}</a></li>
    @endforeach
</ul>