<div class="items">
    <h3>{{ $data['name'] }}</h3>
    <div class="footer_menu">
        @foreach ($data['items'] as $item)
            <a href="{{ $item->url }}">{{ $item->name }}</a>
        @endforeach
    </div>
</div>