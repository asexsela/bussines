<section class="sec6">
    <div class="container">
        <div class="head">
            <p><img src="{{ asset('images/arhives.svg') }}" alt="">Архив</p>
        </div>
        <div class="box-sliders">
            <div class="slider-year">
                <div class="sliders-years-box list-group" role="tablist" id="mylist">
                    @foreach ($numbers_componets as $key => $item)
                        <a class="year @if($key === 0) active @endif" href="#tab-{{ $key }}" role="tab">{{ $item->title }}</a>
                    @endforeach
                </div>
                <a href="" class="prev"><</a>
                <a href="" class="next">></a>
            </div>
            <div class="sliders_jornals-box tab-content">
                @foreach ($numbers_componets as $key => $item)
                    <div class="slider-jornals tab-pane fade @if($key === 0) active show @endif" data-year="2019" id="tab-{{ $key }}" role="tabpanel">
                        @foreach ($item->numbers as $item)         
                            <div class="jornal">
                                <img src="{{ asset($item->img) }}" alt="">
                                <div class="body">
                                    <div>
                                        <h3>{{ $item->title }}</h3>
                                        <span>{{ $item->description }}</span>
                                    </div>
                                    @isset ($item->file)
                                        <a target="blank" href="{{ asset($item->file) }}" class="more_news">Скачать</a>
                                    @endisset
                                </div>
                            </div>
                        @endforeach
                    </div>
                @endforeach
                <a href="" class="prev"><</a>
                <a href="" class="next">></a>
            </div>
        </div>
        
    </div>
</section>