<div class="navbar-fixed">
    <nav>
        <div class="nav-wrapper">
            <div class="row">
                {{-- <form class="col s3 hide-on-small-only">
                        <div class="input-field">
                            <i class="material-icons prefix">search</i>
                            <input id="icon_prefix" type="text" class="validate">
                            <label for="icon_prefix">Поиск</label>
                        </div>
                </form> --}}
                <ul class="right">
                    <li><a href="#!" class="tooltipped" data-position="bottom" data-tooltip="Профиль"><span class="online"></span>{{ auth()->user()->name }}</a></li>
                    {{-- <li><a href="/messages" class="tooltipped" data-position="bottom" data-tooltip="Сообщения"><i class="material-icons">email</i><span class="new badge"  data-badge-caption="">4</span></a></li> --}}
                    <li><a href="{{ route('review') }}" class="tooltipped" data-position="bottom" data-tooltip="Комментарии"><i class="material-icons">comment</i>
                        @if($new_reviews->count())<span class="new badge"  data-badge-caption="">{{ $new_reviews->count() }}</span>@endif
                    </a></li>
                    <li><a href="{{ route('filemanager') }}" class="tooltipped" data-position="bottom" data-tooltip="Файловый менеджер"><i class="material-icons">folder</i></a></li>
                    <li><a href="/" class="tooltipped" data-position="bottom" data-tooltip="На сайт" target="blank"><i class="material-icons">home</i></a></li>
                    <li><a href="#!" class="tooltipped" data-position="bottom" data-tooltip="Выход"
                        onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="material-icons">exit_to_app</i></a></li>
                    <li class="hide-on-large-only"><a href="/" data-target="slide-out" class="sidenav-trigger" class="tooltipped" data-position="bottom" data-tooltip="Меню"><i class="material-icons">menu</i></a></li>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </ul>
                <div class="progress">
                    <div class="indeterminate"></div>
                </div>
            </div>
        </div>
    </nav>
</div>

<a href="#name"></a>