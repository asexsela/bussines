<link rel="stylesheet" href="{{ asset('adm/css/materialize.min.css') }}">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.min.css">
<link rel="stylesheet" href="{{ asset('css/jquery.fancybox.min.css') }}">
<link rel="stylesheet" href="{{ asset('adm/css/codemirror.css') }}">
<link rel="stylesheet" href="{{ asset('adm/theme/material.css') }}">
<link rel="stylesheet" href="{{ asset('adm/css/animate.css') }}">
<link rel="stylesheet" href="{{ asset('adm/css/master.css') }}">
<link rel="stylesheet" href="{{ asset('adm/css/media.css') }}">
<script async src="https://cdnjs.cloudflare.com/ajax/libs/smoothscroll/1.4.10/SmoothScroll.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
<script type="text/javascript" src="https://vk.com/js/api/openapi.js?162"></script>
