<ul id="slide-out" class="sidenav sidenav-fixed">
    <li class="@if(request()->route()->getName() === 'dashboard') active @endif"><a href="{{ route('dashboard') }}" class="waves-effect waves-green"><i class="material-icons">dashboard</i>Приборная панель</a></li>
    <li><a href="{{ route('page') }}" class="waves-effect waves-green"><i class="material-icons">pages</i>Страницы</a></li>
    <li>
        <ul class="collapsible collapsible-accordion">
            <li>
                <a class="collapsible-header waves-effect" >Категории<i class="material-icons">category</i></a>
                <div class="collapsible-body">
                    <ul>
                        <li><a href="{{ route('category') }}">Категории</a></li>
                        <li><a href="{{ route('heading') }}">Рубрики</a></li>
                        <li><a href="{{ route('parnter.category') }}">Партнеры</a></li>
                    </ul>
                </div>
            </li>
        </ul>
    </li>
    <li>
        <ul class="collapsible collapsible-accordion">
            <li>
                <a class="collapsible-header waves-effect" >Новости<i class="material-icons">post_add</i></a>
                <div class="collapsible-body">
                    <ul>
                        <li><a href="{{ route('news') }}">Новости</a></li>
                        <li><a href="{{ route('heading.news') }}">Статьи рубрик</a></li>
                        <li><a href="{{ route('blog.news') }}">Статьи блогов</a></li>
                    </ul>
                </div>
            </li>
        </ul>
    </li>
    <li>
        <ul class="collapsible collapsible-accordion">
            <li>
                <a class="collapsible-header waves-effect" >Архивы<i class="material-icons">archive</i></a>
                <div class="collapsible-body">
                    <ul>
                        <li><a href="{{ route('archive') }}">Архивы</a></li>
                        <li><a href="{{ route('number') }}">Номера</a></li>
                    </ul>
                </div>
            </li>
        </ul>
    </li>
    <li>
        <ul class="collapsible collapsible-accordion">
            <li>
                <a class="collapsible-header waves-effect" >Афиша<i class="material-icons">post_add</i></a>
                <div class="collapsible-body">
                    <ul>
                        <li><a href="{{ route('poster_category') }}">Категории</a></li>
                        <li><a href="{{ route('posters') }}">Статьи</a></li>
                        <li><a href="{{ route('programms') }}">Дневник</a></li>
                    </ul>
                </div>
            </li>
        </ul>
    </li>
    <li><a href="{{ route('album') }}" class="waves-effect waves-green"><i class="material-icons">photo_album</i>Альбомы</a></li>
    <li><a href="{{ route('video') }}" class="waves-effect waves-green"><i class="material-icons">video_library</i>Видео</a></li>
    <li><a href="{{ route('author') }}" class="waves-effect waves-green"><i class="material-icons">supervised_user_circle</i>Авторы</a></li>
    <li><a href="{{ route('partner') }}" class="waves-effect waves-green"><i class="material-icons">supervisor_account</i>Партнеры</a></li>
    <li><a href="{{ route('voiting') }}" class="waves-effect waves-green"><i class="material-icons">record_voice_over</i>Голосования</a></li>
    <li><a href="{{ route('review') }}" class="waves-effect waves-green"><i class="material-icons">rate_review</i>Отзывы</a></li>
    <li><a href="{{ route('banner') }}" class="waves-effect waves-green"><i class="material-icons">grade</i>Реклама</a></li>
    <li><a href="{{ route('thanks_mails') }}" class="waves-effect waves-green"><i class="material-icons">mail</i>Благодарственные письма</a></li>
    <li>
        <ul class="collapsible collapsible-accordion">
            <li>
                <a class="collapsible-header">Настройки<i class="material-icons">settings_applications</i></a>
                <div class="collapsible-body">
                    <ul>
                        <li><a href="{{ route('base') }}">Основные</a></li>
                        <li><a href="{{ route('menu') }}">Меню</a></li>
                    </ul>
                </div>
            </li>
        </ul>
    </li>

    <li><div class="divider"></div></li>
    <li><a target="blank" href="//laboratory.company"><i class="material-icons">child_care</i>Сайт разработчика</a></li>  
    <li><a class="subheader">Группа ВК</a></li>
    <div id="vk_groups"></div>

</ul>