console.log(
    `%c Разработка и поддержка %c Laboratory.company %c a@laboratory.company %c`,
    'background:#f7f7f7 ; padding: 10px; border-radius: 3px 0 0 3px;  color: #007bff; font-weight: 500;',
    'background:#007bff ; padding: 10px; color: #fff; font-weight: 500;',
    'background:#f7f7f7 ; padding: 10px; border-radius: 0 3px 3px 0;  color: #007bff; font-weight: 500;',
    'background:transparent'
)

function uuid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random() * 16 | 0,
          v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
}

let getUuid = (localStorage.getItem('uuid')) ? true : false;

if(!getUuid) {
    localStorage.setItem('uuid', uuid())
}

$(function() {
    $('a.search').click((e) => {
        e.preventDefault()
        $('.input-search').toggleClass('active')
        $('.input-search').focus()
    })
    $(document).click((e) => {
        let isSearch = $(e.target).hasClass('search'),
            isInputSearch = $(e.target).hasClass('input-search');
        if(!isSearch && !isInputSearch) {
            $('.input-search').removeClass('active')
            $('.input-search').blur()
        }
    })

    $('.list').mouseenter(function(e) {
        $('.list').removeClass('active')
        $(this).addClass('active')
    })

    $('.more').click(e => {
        e.preventDefault()
        $(e.target).toggleClass('show')
        $(e.target).parent().find('.text').toggleClass('show')
    })

    function limitStr(str, n, symb) {
        if (!n && !symb) return str;
        symb = symb || '...';
        return str.substr(0, n - symb.length) + symb;
    }

    let desc = $('.news-desc');
    let title = $('.news-title');

    for (let i = 0; i < desc.length; i++) {
        const e = desc[i];
        let len = $(e).text().length;
        if(len >= 62) {
            $(e).text(limitStr($(e).text(), 62))
        }
    }


    $('.slider-year .next').click(function(e) {
        e.preventDefault()
        
        $('.sliders-years-box').animate({
			scrollLeft: '+=100',
		}, 500, function() {
			//код по завершении анимации
		});
    })

    $('.slider-year .prev').click(function(e) {
        e.preventDefault()
        
        $('.sliders-years-box').animate({
			scrollLeft: '-=100',
		}, 500, function() {
			//код по завершении анимации
		});
    })

    $('.sliders_jornals-box .next').click(function(e) {
        e.preventDefault()
        
        $('.slider-jornals').animate({
			scrollLeft: '+=300',
		}, 500, function() {
			//код по завершении анимации
		});
    })

    $('.sliders_jornals-box .prev').click(function(e) {
        e.preventDefault()
        
        $('.slider-jornals').animate({
			scrollLeft: '-=300',
		}, 500, function() {
			//код по завершении анимации
		});
    })

    $('#mylist a').on('click', function (e) {
        e.preventDefault()
        $(this).tab('show')
    })

    $('.like').click(function(e) {

        e.preventDefault();

        let token = $('[name="csrf-token"]').attr('content'),
            id = $(this).data('id'),
            type = $(this).data('type'),
            _this = this,
            count = $(this).text();
        $.ajax({
            type: 'post',
            url: `/${type}/likes`,
            headers: {'X-CSRF-TOKEN': token},
            data: {
                uid: localStorage.getItem('uuid'),
                id: id,
            },
            success: function(data) {
                console.log(data)
                if(data.data === 'like') {
                    $(_this).children('span').text(parseInt(++count))
                } else if(data.data === 'unlike') {
                    $(_this).children('span').text(parseInt(--count))
                }
            },
            error: function(err) {
                console.log(err)
            }
        })
    })

    $('.dislike').click(function(e) {

        e.preventDefault();

        let token = $('[name="csrf-token"]').attr('content'),
            id = $(this).data('id'),
            type = $(this).data('type'),
            _this = this,
            count = $(this).text();
        $.ajax({
            type: 'post',
            url: `/${type}/dislikes`,
            headers: {'X-CSRF-TOKEN': token},
            data: {
                uid: localStorage.getItem('uuid'),
                id: id,
            },
            success: function(data) {
                console.log(data)
                if(data.data === 'dislike') {
                    $(_this).children('span').text(parseInt(++count))
                } else if(data.data === 'undislike') {
                    $(_this).children('span').text(parseInt(--count))
                }
            },
            error: function(err) {
                console.log(err)
            }
        })
    })

    
    // $('#ok').click(function(e) {

    //     e.preventDefault();

    //     let url = $(this).data('url'),
    //         title = $(this).data('title'),
    //         img = $(this).data('img'),
    //         link = `//connect.ok.ru/offer?url=${url}&title=${title}&imageUrl=${img}`,
    //         left = (screen.width/2)-(600/2),
    //         top = (screen.height/2)-(400/2);

    //     window.open(link, title, `width=600,height=400,left=${left},top=${top}`);
    // })

    $('#fb').click(function(e) {
        e.preventDefault();
        let url = $(this).data('url'),
            title = $(this).data('title'),
            img = $(this).data('img'),
            desc = $(this).data('desc'),
            link = `http://www.facebook.com/sharer.php`,
            left = (screen.width/2)-(600/2),
            top = (screen.height/2)-(400/2);

        window.open(link, title, `width=600,height=400,left=${left},top=${top}`);
    })

    $('.box-control .date_picker').datepicker({
        format: "yyyy-mm-dd",
        language: "ru",
        multidate: true,
        // multidateSeparator: ',',
    }).on('changeDate', function(e) {
        let date = $('.date_picker').val(),
            url = $(e.target).data('url')+`/${date}`;
            if(e.dates.length >= 2) {
                location.href = url
            }
    });

    // $('.box-control .date_pickers').datepicker({
    //     format: "dd-mm-yyyy",
    //     language: "ru",
    // }).on('changeDate', function(e) {
    //     // let date = $('.date_picker').val(),
    //     //     url = $(e.target).data('url')+`/${date}`;
    //     //     location.href = url
    //     console.log(e)
    // });

    $('.box-control a.date').click(function(e) {
        e.preventDefault();

        // $(this).find('input').trigger('click');
    })

    $('.burger').click(function() {
        $('.header-mob').toggleClass('open')
        $('.menu').toggleClass('open')
        $('body').toggleClass('open-menu')
    })
    $('.search-mob').click(function() {
        $('.mob-search').toggleClass('open')
        $('body').toggleClass('open-menu')
    })

    $(document).scroll(function() {
        if($(this).scrollTop() >= 150) {
            $('.header-mob').addClass('fixed');
        } else {
            $('.header-mob').removeClass('fixed');
        }
    })

    let big = $('.big'),
        small = $('.small'),
        width = $(document).innerWidth();

    if(width >= 768) {
        big.each(function(i) {
            if($(big[i]).data('bigstyle')) {
                $(big[i]).find('img').css($(big[i]).data('bigstyle'))
            }
        })
        small.each(function(i) {
            if($(small[i]).data('smallstyle')) {
                $(small[i]).find('img').css($(small[i]).data('smallstyle'))
            }
        })
    } else {
        // big.each(function(i) {
        //     if($(big[i]).data('smallstyle')) {
        //         $(big[i]).find('img').css($(big[i]).data('smallstyle'))
        //     }
        // })
    }

})

// $(window).on('load', function () {
    setTimeout(function() {
        $('.loader').fadeOut('slow');
    }, 1000)
// });

    // $("img.news_image").lazyload({
    //     effect : "fadeIn"
    // });

$(function() {

    $('form.toSearch').submit(function(e) {
        e.preventDefault();
        let str = $(this).find('input[name="search"]').val();
        location.href = `/search/${str}`;
    })

    // $('.sendme-button').click(function(e) {
    //     e.preventDefault();
    //     $('#sendme-mail').trigger('submit')
        
        
    // })

        $('#sendme-mail').submit(function(e) {
            e.preventDefault();
            let data = new FormData($(this)[0]);
            
            $.ajax({
                type: 'post',
                url: '/send-mail',
                data: data,
                dataType:'JSON',
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function() {
                    $('#sendMe').find('.loader').addClass('show');
                },
                success: function(data) {
                    console.log(data)
                    $('#sendMe').find('.loader').removeClass('show');
                    if(data) {
                        $('.success').show().css('display', 'flex');
                    } else {
                        $('#sendMe').find('.loader').removeClass('show');
                        $('.error').show().css('display', 'flex');
                    }

                    setTimeout(function() {
                        $('.success').hide();
                        $('.error').hide();
                        $('#sendMe').modal('hide')
                        $('#sendme-mail')[0].reset()
                    }, 3000)
                    
                },
                error: function(err) {
                    console.log(err)
                }
            })
        })



        $('.scrollToTop').click(function() {
            $("html, body").animate({ scrollTop: 0 }, "slow");
            return false;
        });

        
        $(document).scroll(function() {
            let top = $(document).scrollTop();
            if(top >= 500) {
                $('.scrollToTop').fadeIn('slow').css('display', 'flex');
            } else {
                $('.scrollToTop').fadeOut('slow');
            }
        })

        $('.tag').click(function(e) {
            e.preventDefault()
            let tag = $(this).text().trim('#').slice(1)
            location.href = `/search-tag/${tag}`;
            // console.log(tag)
        })
        

        
    

});



