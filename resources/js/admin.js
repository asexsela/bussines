import Cropper from 'cropperjs';

const image = document.getElementById('image');

if(image) {
    let cropper = new Cropper(image, {
        zoomable: false,
        autoCrop: false,
        preview: '.big_img',
        ready(e) {
            let crop = $(e.target).data('cropper');
            if(crop) {
                this.cropper.setCropBoxData({left: crop.left, top : crop.top, width : crop.width, height:crop.height});
            }
    
    
            let big_style = $('.big.img').find('img').css([
                'display',
                'width',
                'height',
                'min-width',
                'min-height',
                'max-width',
                'max-height',
                'transform'
            ]),
            small_style = $('.small.img').find('img').css([
                'display',
                'width',
                'height',
                'min-width',
                'min-height',
                'max-width',
                'max-height',
                'transform'
            ]);
            $('input[name="big_style"]').val(JSON.stringify(big_style))
            $('input[name="small_style"]').val(JSON.stringify(small_style))
        },
        crop(event) {
            let big_style = $('.big.big_img').find('img').css([
                    'display',
                    'width',
                    'height',
                    'min-width',
                    'min-height',
                    'max-width',
                    'max-height',
                    'transform'
                ]),
                small_style = $('.small.big_img').find('img').css([
                    'display',
                    'width',
                    'height',
                    'min-width',
                    'min-height',
                    'max-width',
                    'max-height',
                    'transform'
                ]),
                cropper = this.cropper.getCropBoxData(),
                cr = {
                    left: event.detail.x, 
                    top : event.detail.y, 
                    width : cropper.width, 
                    height: cropper.height
                };
                
            $('input[name="big_style"]').val(JSON.stringify(big_style))
            $('input[name="small_style"]').val(JSON.stringify(small_style))
            $('input[name="cropper"]').val(JSON.stringify(cr))
            $('.big > img').css(big_style)
            $('.small > img').css(small_style)
        },
    });
}




